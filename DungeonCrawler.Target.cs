// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class DungeonCrawlerTarget : TargetRules
{
	public DungeonCrawlerTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.Add( "DungeonCrawler");
	}
}
