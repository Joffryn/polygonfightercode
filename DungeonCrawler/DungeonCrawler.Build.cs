// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class DungeonCrawler : ModuleRules
{
	public DungeonCrawler(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.Add("GameplayAbilities");
		PublicDependencyModuleNames.Add("GameplayTasks");
		PublicDependencyModuleNames.Add("GameplayTags");
		PublicDependencyModuleNames.Add("InputCore");
		PublicDependencyModuleNames.Add("Engine");
		PublicDependencyModuleNames.Add("Core");
		bEnableUndefinedIdentifierWarnings = false;
		
		if (Target.Configuration != UnrealTargetConfiguration.Shipping)
		{
			PrivateDependencyModuleNames.Add("ImGui");
		}
	}
}
