// Fill out your copyright notice in the Description page of Project Settings.

#include "MyAIController.h"

#include "AIManager.h"
#include "BaseCharacter.h"
#include "Kismet/GameplayStatics.h"

void AMyAIController::SetNewTarget(ACharacter* Target)
{
	if(Target != UGameplayStatics::GetPlayerCharacter(this, 0))
		if(LastAwareTarget == UGameplayStatics::GetPlayerCharacter(this, 0))
			if(const auto AIManager = UAIManager::GetAIManager(this))
				AIManager->AIStopBeingAwareOfThePlayer(this);

	LastAwareTarget = Target;
	if(Target == UGameplayStatics::GetPlayerCharacter(this, 0))
		if(const auto AIManager = UAIManager::GetAIManager(this))
			AIManager->AIStartBeingAwareOfThePlayer(this);
}

AMyAIController::AMyAIController()
{
}

void AMyAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	if(const auto AIManager = UAIManager::GetAIManager(this))
		AIManager->RegisterAI(this);

	if(const auto BaseCharacter = Cast<ABaseCharacter>(GetPawn()))
		BaseCharacter->OnDeath.AddDynamic(this, &AMyAIController::OnPossessedPawnDeath);
}

void AMyAIController::OnUnPossess()
{
	Super::OnUnPossess();
	if(const auto AIManager = UAIManager::GetAIManager(this))
		AIManager->UnregisterAI(this);
}

void AMyAIController::OnPossessedPawnDeath()
{
	if(LastAwareTarget == UGameplayStatics::GetPlayerCharacter(this, 0))
		if(const auto AIManager = UAIManager::GetAIManager(this))
			AIManager->AIStopBeingAwareOfThePlayer(this);
}
