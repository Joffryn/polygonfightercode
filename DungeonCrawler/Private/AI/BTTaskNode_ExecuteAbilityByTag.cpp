// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/BTTaskNode_ExecuteAbilityByTag.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"
#include "MyAbilitySystemComponent.h"

UBTTaskNode_ExecuteAbilityByTag::UBTTaskNode_ExecuteAbilityByTag()
{
	bCreateNodeInstance = true;
	bCancelAbilityOnAbort = true;
	bIgnoreTargetCheck = false;
}

EBTNodeResult::Type UBTTaskNode_ExecuteAbilityByTag::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const AAIController* AIController = Cast<AAIController>(OwnerComp.GetOwner());
	APawn* OwnerPawn = AIController->GetPawn();
	if (!OwnerPawn)
		return EBTNodeResult::Failed;

	UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
	if (!BlackboardComp)
		return EBTNodeResult::Failed;

	AActor* TargetCharacter = Cast<AActor>(BlackboardComp->GetValueAsObject(FName(TEXT("Target"))));
	if (!TargetCharacter && !bIgnoreTargetCheck)
		return EBTNodeResult::Failed;

	IAbilitySystemInterface* Asi = Cast<IAbilitySystemInterface>(OwnerPawn);
	if (Asi)
		MyAbilitySystemComponent = Cast<UMyAbilitySystemComponent>(Asi->GetAbilitySystemComponent());
	else
		return EBTNodeResult::Failed;

	const FGameplayTagContainer TagContainer = FGameplayTagContainer(GameplayTag);
	FGameplayEventData EventData;
	EventData.Instigator = OwnerPawn;
	EventData.Target = TargetCharacter;
	OnAbilityFailedHandle = MyAbilitySystemComponent->AbilityFailedCallbacks.AddUObject(this, &UBTTaskNode_ExecuteAbilityByTag::OnAbilityFailed, &OwnerComp);
	OnAbilityEndedHandle = MyAbilitySystemComponent->AbilityEndedCallbacks.AddUObject(this, &UBTTaskNode_ExecuteAbilityByTag::OnAbilityEnded, &OwnerComp);
	if (MyAbilitySystemComponent->TryActivateAbilitiesByTagWithEventData(TagContainer, EventData))
		return EBTNodeResult::InProgress;
	
	return EBTNodeResult::Failed;
}

FString UBTTaskNode_ExecuteAbilityByTag::GetStaticDescription() const
{
	return FString::Printf(TEXT("Execute ability: %s"), *GameplayTag.ToString());
}

EBTNodeResult::Type UBTTaskNode_ExecuteAbilityByTag::AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (bCancelAbilityOnAbort)
	{
		const FGameplayTagContainer TagContainer = FGameplayTagContainer(GameplayTag);
		MyAbilitySystemComponent->CancelAbilities(&TagContainer);
	}

	return Super::AbortTask(OwnerComp, NodeMemory);
}

void UBTTaskNode_ExecuteAbilityByTag::OnAbilityEnded(UGameplayAbility* GameplayAbility, UBehaviorTreeComponent* OwnerComp)
{
	if (GameplayAbility->AbilityTags.HasTag(GameplayTag))
	{
		MyAbilitySystemComponent->AbilityFailedCallbacks.Remove(OnAbilityFailedHandle);
		MyAbilitySystemComponent->AbilityEndedCallbacks.Remove(OnAbilityEndedHandle);
		FinishLatentTask(*OwnerComp, EBTNodeResult::Succeeded);
	}
}

void UBTTaskNode_ExecuteAbilityByTag::OnAbilityFailed(const UGameplayAbility* GameplayAbility, const FGameplayTagContainer& GameplayTags, UBehaviorTreeComponent* OwnerComp)
{
	if (GameplayAbility->AbilityTags.HasTag(GameplayTag))
	{
		MyAbilitySystemComponent->AbilityFailedCallbacks.Remove(OnAbilityFailedHandle);
		MyAbilitySystemComponent->AbilityEndedCallbacks.Remove(OnAbilityEndedHandle);
		FinishLatentTask(*OwnerComp, EBTNodeResult::Failed);
	}
}
