// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/BTDecorator_ActivationChance.h"

UBTDecorator_ActivationChance::UBTDecorator_ActivationChance()
{
	NodeName = "ActivationChance";
}

bool UBTDecorator_ActivationChance::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const float RandomFloat = FMath::FRand();

	if (ChanceToActivate == 0.0)
		return false;

	if (RandomFloat <= ChanceToActivate)
		return true;

	return false;
}
