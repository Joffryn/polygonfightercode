// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/Tasks/AbilityTask_InterpRotation.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"

UAbilityTask_InterpRotation* UAbilityTask_InterpRotation::InterpolateControlRotationToActor(UGameplayAbility* OwningAbility, AActor* TargetActor, const FVector& LocationOffset, const float Speed, const bool OnlyYaw, const float Tolerance)
{
	UAbilityTask_InterpRotation* Task = NewAbilityTask< UAbilityTask_InterpRotation >(OwningAbility);
	Task->TargetActor = TargetActor;
	Task->TargetLocation = LocationOffset;
	Task->RotateOnlyYaw = OnlyYaw;
	Task->InterpolationSpeed = Speed;
	Task->Tolerance = Tolerance;
	return Task;
}

UAbilityTask_InterpRotation::UAbilityTask_InterpRotation()
{
	bTickingTask = true;
}

void UAbilityTask_InterpRotation::TickTask(const float DeltaTime)
{
	ACharacter* AvatarCharacter = Cast< ACharacter >(GetAvatarActor());
	if (IsValid(AvatarCharacter) == false)
	{
		if (ShouldBroadcastAbilityTaskDelegates())
		{
			OnError.Broadcast();
		}
		EndTask();
		return;
	}
	AController* AvatarController = AvatarCharacter->GetController();

	const FVector DirectionToTarget = GetDirectionToTarget(AvatarCharacter);

	FRotator TargetRotator = FRotationMatrix::MakeFromXZ(DirectionToTarget, FVector::UpVector).Rotator();
	TargetRotator.Roll = 0.0f;

	const FRotator CurrentRotator = IsValid(AvatarController) ? AvatarController->GetControlRotation() : AvatarCharacter->GetActorRotation();

	FRotator InterpolatedRotator = FMath::RInterpTo(CurrentRotator, TargetRotator, DeltaTime, InterpolationSpeed);
	InterpolatedRotator.Roll = 0.0f;

	if (IsValid(AvatarController))
		AvatarController->SetControlRotation(InterpolatedRotator);
	else
		AvatarCharacter->SetActorRotation(InterpolatedRotator);

	if (InterpolatedRotator.Equals(TargetRotator, Tolerance))
	{
		if (ShouldBroadcastAbilityTaskDelegates())
		{
			OnFinish.Broadcast();
		}
		EndTask();
	}
}

FVector UAbilityTask_InterpRotation::GetTargetLocation() const
{
	if (IsValid(TargetComponent))
	{
		return TargetComponent->GetComponentTransform().TransformPosition(TargetLocation);
	}
	if (IsValid(TargetActor))
	{
		return TargetActor->GetActorTransform().TransformPosition(TargetLocation);
	}
	return TargetLocation;
}

FVector UAbilityTask_InterpRotation::GetDirectionToTarget(AActor* AvatarActor) const
{
	FVector DirectionToTarget;
	ensure(AvatarActor);

	if (RotateOnlyYaw)
	{
		DirectionToTarget = (GetTargetLocation() - AvatarActor->GetActorLocation()).GetSafeNormal2D();
	}
	else
	{
		DirectionToTarget = (GetTargetLocation() - AvatarActor->GetActorLocation()).GetSafeNormal();
	}

	return DirectionToTarget;
}

