// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/Calculations/MaxStatChange.h"

float UMaxStatChange::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
	const float StatChange = Spec.GetLevel();

	return StatChange;
}
