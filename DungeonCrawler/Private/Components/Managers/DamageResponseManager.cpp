// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/Managers/DamageResponseManager.h"

// Sets default values for this component's properties
UDamageResponseManager::UDamageResponseManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


float UDamageResponseManager::SendDamageInfo(FDamageInfo DamageInfo)
{
	return 0.0f;
}

// Called when the game starts
void UDamageResponseManager::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UDamageResponseManager::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

