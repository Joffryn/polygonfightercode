// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/Managers/AIManager.h"

#include "AIController.h"
#include "BossComponent.h"
#include "MyGameMode.h"

UAIManager* UAIManager::GetAIManager(const UObject* WorldContextObject)
{
	return AMyGameMode::GetMyGameMode(WorldContextObject)->AIManager;
}

bool UAIManager::AreThereAnyEnemies() const
{
	return RegisteredAIs.Num() > 0;
}

bool UAIManager::IsThereAIAwareOfThePlayer() const
{
	return AIsAwareOfThePlayer.Num() > 0;
}

void UAIManager::AIStartBeingAwareOfThePlayer(AAIController* AIController)
{
	if(AIsAwareOfThePlayer.Num() == 0)
		OnFirstEnemyStartBeingAwareOfThePlayer.Broadcast();
		
	AIsAwareOfThePlayer.AddUnique(AIController);
}

void UAIManager::AIStopBeingAwareOfThePlayer(AAIController* AIController)
{
	AIsAwareOfThePlayer.Remove(AIController);
	
	if(AIsAwareOfThePlayer.Num() == 0)
		OnLastEnemyStopBeingAwareOfThePlayer.Broadcast();
}

void UAIManager::RegisterAI(AAIController* AIController)
{
	RegisteredAIs.AddUnique(AIController);
	if(const auto Pawn = AIController->GetPawn())
		if(Pawn->GetComponentByClass(UBossComponent::StaticClass()))
			RegisteredBosses.AddUnique(AIController);
}

void UAIManager::UnregisterAI(AAIController* AIController)
{
	RegisteredAIs.Remove(AIController);
	AIStopBeingAwareOfThePlayer(AIController);
	if (RegisteredAIs.Num() == 0)
		OnLastAIDeath.Broadcast();
	
	if(const auto Pawn = AIController->GetPawn())
		if(Pawn->GetComponentByClass(UBossComponent::StaticClass()))
		{
			RegisteredBosses.Remove(AIController);
			OnLastBossDeath.Broadcast();
		}
}
