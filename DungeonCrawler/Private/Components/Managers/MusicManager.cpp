// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/Managers/MusicManager.h"

#include "MyGameMode.h"
#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"

UMusicManager* UMusicManager::GetMusicManager(const UObject* WorldContextObject)
{
	return AMyGameMode::GetMyGameMode(WorldContextObject)->MusicManager;
}

void UMusicManager::PlayLocationMusic(USoundBase* Music, const float FadeInTime)
{
	if(!Music)
	  return;
	
	if(CurrentBossMusic)
		return;
	
	if(CurrentCombatMusic)
		return;
	
	CurrentLocationMusic = UGameplayStatics::SpawnSound2D(this, Music);
	if(CurrentLocationMusic)
		CurrentLocationMusic->FadeIn(FadeInTime, 1.0f);
	
}

void UMusicManager::StopLocationMusic(const float FadeOutTime)
{
	if(CurrentLocationMusic)
		CurrentLocationMusic->FadeOut(FadeOutTime, 0.0f);

	CurrentLocationMusic = nullptr;
}

void UMusicManager::PlayBossMusic(USoundBase* Music, const float FadeInTime)
{
	if(!Music)
		return;

	if(CurrentCombatMusic)
		StopCombatMusic(0.0f);

	if(CurrentLocationMusic)
		StopLocationMusic(0.0f);
	
	CurrentBossMusic = UGameplayStatics::SpawnSound2D(this, Music);
	if(CurrentBossMusic)
		CurrentBossMusic->FadeIn(FadeInTime, 1.0f);

}

void UMusicManager::StopBossMusic(const float FadeOutTime)
{
	if(CurrentBossMusic)
		CurrentBossMusic->FadeOut(FadeOutTime, 0.0f);

	CurrentBossMusic = nullptr;
}

void UMusicManager::PlayCombatMusic(const float FadeInTime)
{
	if(CurrentBossMusic)
		return;

	if(CurrentLocationMusic)
		StopLocationMusic(0.0f);

	USoundBase* RandomMusicToPlay = DefaultCombatMusic[FMath::RandRange(0,DefaultCombatMusic.Num() -1)];
	CurrentCombatMusic = UGameplayStatics::SpawnSound2D(this, RandomMusicToPlay);
	if(CurrentCombatMusic)
		CurrentCombatMusic->FadeIn(FadeInTime, 1.0f);
	
}

void UMusicManager::StopCombatMusic(const float FadeOutTime)
{
	if(CurrentCombatMusic)
		CurrentCombatMusic->FadeOut(FadeOutTime, 0.0f);

	CurrentCombatMusic = nullptr;
}
