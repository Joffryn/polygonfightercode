// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerCameraManager.h"
#include "Kismet/GameplayStatics.h"

AMyPlayerCameraManager* AMyPlayerCameraManager::GetMyPlayerCameraManager(const UObject* WorldContextObject)
{
	return Cast<AMyPlayerCameraManager>(UGameplayStatics::GetPlayerCameraManager(WorldContextObject, 0));
}
