// Fill out your copyright notice in the Description page of Project Settings.

#include "GameConfigComponent.h"
#include "MyGameMode.h"

UGameConfigComponent* UGameConfigComponent::GetGameConfigComponent(const UObject* WorldContextObject)
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(WorldContextObject))
		return MyGameMode->GameConfigComponent;

	return nullptr;
}
