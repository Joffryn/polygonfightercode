// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProjectileMovementComponent.h"

void UMyProjectileMovementComponent::StartMoving()
{
	Velocity = Velocity.GetSafeNormal() * InitialSpeed;
}
