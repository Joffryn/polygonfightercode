// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerInteractionComponent.h"

#include <d3d10.h>

#include "InteractionComponent.h"
#include "HudWidget.h"
#include "InteractableInterface.h"
#include "MyHUD.h"
#include "MyPlayerController.h"

void UPlayerInteractionComponent::BeginPlay()
{
	Super::BeginPlay();

	OnComponentBeginOverlap.AddDynamic(this, &UPlayerInteractionComponent::OnBeginOverlap);
	OnComponentEndOverlap.AddDynamic(this, &UPlayerInteractionComponent::OnEndOverlap);
	
}

void UPlayerInteractionComponent::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{		
	if(const auto InteractionComponent = Cast<UInteractionComponent>(OtherComp))
		PossibleInteractionCandidates.AddUnique(InteractionComponent);
}

void UPlayerInteractionComponent::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (const auto OtherCompInteraction = Cast<UInteractionComponent>(OtherComp))
	{
		if(OtherCompInteraction == InteractionCandidate)
			SetInteractionCandidate(nullptr);
		
		PossibleInteractionCandidates.Remove( Cast<UInteractionComponent>(OtherCompInteraction));
	}
}

void UPlayerInteractionComponent::InteractAction()
{
	if (InteractionCandidate)
		InteractionCandidate->Interact(this);
}

void UPlayerInteractionComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//Only care about this logic when possessed by the player
	if(const auto OwnerPawn = Cast<APawn>(GetOwner()))
		if(OwnerPawn->GetController() != AMyPlayerController::GetMyPlayerController(this))
		{
			if(InteractionCandidate)
				InteractionCandidate->bIsFocused = false;

			InteractionCandidate = nullptr;
			return;
		}
			
	UInteractionComponent* BestCandidate = nullptr;
	for(const auto Candidate : PossibleInteractionCandidates)
	{
		if(!Candidate)
		{
			InteractionCandidate = nullptr;
			return;
		}

		//Only consider candidates that can be interacted with
		const auto CandidateOwnerActor = Candidate->GetOwner();
		if(const auto InteractableInterface = Cast<IInteractableInterface>(CandidateOwnerActor))
			if(!InteractableInterface->Execute_CanInteract(CandidateOwnerActor))
				return;
			
		Candidate->bIsFocused = false;
		float DistanceToTheClosestCandidate = 9999999.0f;
		const float DistanceToTheCurrentCandidate = (GetComponentLocation() - Candidate->GetComponentLocation()).Size();
		if(DistanceToTheCurrentCandidate < DistanceToTheClosestCandidate)
		{
			DistanceToTheClosestCandidate = DistanceToTheCurrentCandidate;
			BestCandidate = Candidate;
		}
	}
	
	if(BestCandidate != InteractionCandidate)
		SetInteractionCandidate(BestCandidate);
	
	if(InteractionCandidate)
		InteractionCandidate->bIsFocused = true;
}

void UPlayerInteractionComponent::SetInteractionCandidate(UInteractionComponent* NewInteractionCandidate)
{
	InteractionCandidate = NewInteractionCandidate;
	OnInteractionDelegateSet.Broadcast(InteractionCandidate);
}
