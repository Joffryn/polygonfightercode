// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/UpgradeHandlingComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"
#include "BaseCharacter.h"
#include "DamageResponseComponent.h"
#include "MyAttributeSet.h"
#include "StatsComponent.h"
#include "Kismet/GameplayStatics.h"

void UUpgradeHandlingComponent::BeginPlay()
{
	Super::BeginPlay();

	BaseCharacterOwner = Cast<ABaseCharacter>(GetOwner());
	OwnerAbilitySystemComponent = Cast<UAbilitySystemComponent>(GetOwner()->GetComponentByClass(UAbilitySystemComponent::StaticClass()));

	BindDelegates();
}

void UUpgradeHandlingComponent::BindDelegates()
{
	const auto StatsComponent = BaseCharacterOwner->StatsComponent;
	StatsComponent->OnStatChanged.AddDynamic(this, &UUpgradeHandlingComponent::OnStatChanged);

	const auto DamageResponseComponent = BaseCharacterOwner->DamageResponseComponent;
	DamageResponseComponent->OnDealedDamage.AddDynamic(this, &UUpgradeHandlingComponent::OnAttackDamageDealed);
	DamageResponseComponent->OnCritDamageDealed.AddDynamic(this, &UUpgradeHandlingComponent::OnAttackCrit);
	DamageResponseComponent->OnParry.AddDynamic(this, &UUpgradeHandlingComponent::OnAttackParried);
	DamageResponseComponent->OnKill.AddDynamic(this, &UUpgradeHandlingComponent::OnKill);
}

void UUpgradeHandlingComponent::OnStatChanged(EStat Stat, float Amount)
{
	const FGameplayEffectContextHandle EffectContext;
	switch (Stat)
	{
	//Mana restore on damage taken upgrade
	case EStat::Health:
		if(Amount < 0.0f)
			if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(DamageTakeManaGainTag))
				OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreManaEffect, -1.0f * DamageTakenManaGainMultiplier * Amount, EffectContext);
	
	default: ;
	}
}

void UUpgradeHandlingComponent::OnAttackCrit(TSubclassOf<UDamageType> DamageType, float Amount)
{
	//Stamina restore on crit
	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(CritStaminaGainTag))
	{
		const FGameplayEffectContextHandle EffectContext;
		OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreStaminaEffect, CritDamageStaminaGainMultiplier * Amount, EffectContext);
	}
}

void UUpgradeHandlingComponent::OnAttackParried(AActor* Actor, TSubclassOf<UDamageType> DamageType, float Amount)
{
	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(ParryStaminaRestorationTag))
	{
		bool bSuccess;
		const float Value = UAbilitySystemBlueprintLibrary::GetFloatAttribute(GetOwner(), UMyAttributeSet::GetCurrentActionStaminaCostAttribute(), bSuccess);
		const FGameplayEffectContextHandle EffectContext;
		OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreStaminaEffect, Value, EffectContext);
	}
	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(ParryDamageDealTag))
		UGameplayStatics::ApplyDamage(Actor, Amount, BaseCharacterOwner->GetController(), BaseCharacterOwner, DamageType);
}

void UUpgradeHandlingComponent::OnAttackDamageDealed(TSubclassOf<UDamageType> DamageType, float Amount)
{
	//Mana restore on attack, enabled by default

	if(!OwnerAbilitySystemComponent->HasMatchingGameplayTag(ManaGenerationBlockedTag))
		if(const auto MyDamageTypeClass = Cast<UMyDamageType>(DamageType->GetDefaultObject())->GetClass())
			if(Cast<UMyDamageType>(MyDamageTypeClass->GetDefaultObject())->DamageTags.HasTag(DamageMeleeTag))
			{
				const FGameplayEffectContextHandle EffectContext;
				OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreManaEffect, ManaPerMeleeDamageGainMultiplier * Amount, EffectContext);
			}
	}

void UUpgradeHandlingComponent::OnKill(AActor* Target)
{
	//Heal on kill upgrade
	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(HealOnKillTag))
	{
		const FGameplayEffectContextHandle EffectContext;
		OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreHealthEffect, HealthGainPerKill, EffectContext);
	}
	//Mana restore on kill upgrade
	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(ManaRestoreOnKillTag))
	{
		const FGameplayEffectContextHandle EffectContext;
		OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreManaEffect, ManaRestorePerKill, EffectContext);
	}
}
