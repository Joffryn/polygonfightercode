// Fill out your copyright notice in the Description page of Project Settings.

#include "SoundsComponent.h"

#include "GameConfigComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameplayTagContainer.h"

void USoundsComponent::SpawnSound(const FGameplayTag SoundTag, const float VolumeMultiplier, const float PitchMultiplier) const
{
	USoundAttenuation* BaseAttenuation = nullptr;
	if(const auto GameConfigComponent = UGameConfigComponent::GetGameConfigComponent(this))
		BaseAttenuation= GameConfigComponent->BaseAttenuation;
	
	if (const auto Sound = SoundsToTagsMap.Find(SoundTag))
		UGameplayStatics::SpawnSoundAtLocation(this, *Sound, GetOwner()->GetActorLocation(), FRotator(),
			CharacterVolumeMultiplier * VolumeMultiplier, CharacterPitchMultiplier * PitchMultiplier, 0.0f, BaseAttenuation);
}

UAudioComponent* USoundsComponent::SpawnSoundAttached(const FGameplayTag SoundTag, const FName AttachPointName, const float VolumeMultiplier,const float PitchMultiplier) const
{
	USoundAttenuation* BaseAttenuation = nullptr;
	if(const auto GameConfigComponent = UGameConfigComponent::GetGameConfigComponent(this))
		BaseAttenuation= GameConfigComponent->BaseAttenuation;
	
	if (const auto Sound = SoundsToTagsMap.Find(SoundTag))
		return UGameplayStatics::SpawnSoundAttached(*Sound, GetOwner()->GetRootComponent(), AttachPointName, FVector::ZeroVector,
			EAttachLocation::Type ::KeepRelativeOffset, true,CharacterVolumeMultiplier * VolumeMultiplier, CharacterPitchMultiplier * PitchMultiplier, 0.0f, BaseAttenuation);
	
	return nullptr;
}
