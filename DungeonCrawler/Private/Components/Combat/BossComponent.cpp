// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/Combat/BossComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "Components/AudioComponent.h"
#include "MyAbilitySystemComponent.h"
#include "DamageResponseComponent.h"
#include "MyAttributeSet.h"
#include "BaseCharacter.h"
#include "MusicManager.h"
#include "MyHUD.h"
#include "Kismet/GameplayStatics.h"

UBossComponent::UBossComponent()
{
}

void UBossComponent::SetPhase(const int32 NewPhase)
{
	if(NewPhase > Phase)
	{
		const FGameplayEventData Payload;
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(GetOwner(), NextBossPhaseTag, Payload);
	}
	Phase = NewPhase;
	OnBossPhaseEnter.Broadcast(Phase);
 }

void UBossComponent::BeginPlay()
{
	Super::BeginPlay();

	if(const auto BaseCharacterOwner = Cast<ABaseCharacter>(GetOwner()))
	{
		if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
			Player->DamageResponseComponent->OnDeath.AddDynamic(this, &UBossComponent::OnPlayerDeath);
		
		BaseCharacterOwner->OnTargetSet.AddDynamic(this, &UBossComponent::OnTargetSet);
		BaseCharacterOwner->DamageResponseComponent->OnDeath.AddDynamic(this, &UBossComponent::OnOwnerDeath);

		//Ignore if boss has no phases
		if(PhasesSetup.Num() > 0)
			if (const auto Abs = BaseCharacterOwner->AbilitySystemComponent)
				Abs->OnAttributeValueChange.AddDynamic(this, &UBossComponent::OnOwnerAttributeChange);
	}
}

void UBossComponent::OnTargetSet(AActor* Target)
{
	if(bIsSetuped)
		return;
	
	if(Target == UGameplayStatics::GetPlayerCharacter(this, 0))
	{
		AMyHUD::GetMyHud(this)->ShowBossHealthBar(Cast<ABaseCharacter>(GetOwner()));
		if(const auto MusicManager = UMusicManager::GetMusicManager(this))
			MusicManager->PlayBossMusic(Music, MusicFadeInTime);

		bIsSetuped = true;
	}
}

void UBossComponent::OnOwnerAttributeChange(const FGameplayAttribute Attribute, float NewValue, float OldValue)
{
	if (Attribute == UMyAttributeSet::GetHealthAttribute())
	{
		bool bFound;
		const auto HealthPercentage = UAbilitySystemBlueprintLibrary::GetFloatAttribute(GetOwner(), UMyAttributeSet::GetHealthAttribute(), bFound) 
		/ UAbilitySystemBlueprintLibrary::GetFloatAttribute(GetOwner(), UMyAttributeSet::GetMaxHealthAttribute(), bFound);
		int32 BestMatch = -1;
		float BestMatchHealthPercentage = FLT_MAX;
		for (const auto BossPhase : PhasesSetup)
		{
			if(BossPhase.PhaseID > Phase)
			{
				if (HealthPercentage < BossPhase.HealthPercentage && HealthPercentage < BestMatchHealthPercentage)
				{
					BestMatchHealthPercentage = HealthPercentage;
					BestMatch = BossPhase.PhaseID;
				}
			}
		}
		if(BestMatch > 0)
			if(BestMatch != Phase)
				SetPhase(BestMatch);
	}
}

void UBossComponent::OnOwnerDeath()
{
	StopMusic();
	AMyHUD::GetMyHud(this)->ShowVictoryMessage();
}

void UBossComponent::OnPlayerDeath()
{
	StopMusic();
}

void UBossComponent::StopMusic() const
{
	if(const auto MusicManager = UMusicManager::GetMusicManager(this))
		MusicManager->StopBossMusic(MusicFadeOutTime);
}
