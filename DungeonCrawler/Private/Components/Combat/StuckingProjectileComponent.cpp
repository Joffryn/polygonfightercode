// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/Combat/StuckingProjectileComponent.h"
#include "Actors/Characters/TargetDummy.h"
#include "MyProjectileMovementComponent.h"
#include "Components/SceneComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameConfigComponent.h"
#include "BaseCharacter.h"
#include "Actors/Projectile.h"

UStuckingProjectileComponent::UStuckingProjectileComponent()
{
}

void UStuckingProjectileComponent::BeginPlay()
{
	Super::BeginPlay();

	ProjectileOwner = Cast<AProjectile>(GetOwner());
	ensure(ProjectileOwner);
	if (ProjectileOwner)
		ProjectileOwner->OnProjectileHit.AddDynamic(this, &UStuckingProjectileComponent::OnOwnerProjectileHit);
}

void UStuckingProjectileComponent::OnOwnerProjectileHit(const FHitResult HitResult, const float Damage)
{
	//Very naive solution, it will probably be refactored in the future
	//For now just use base attenuation
	USoundAttenuation* BaseAttenuation = nullptr;
	if(const auto GameConfigComponent = UGameConfigComponent::GetGameConfigComponent(this))
		BaseAttenuation= GameConfigComponent->BaseAttenuation;

	//Think of a cleaner solution
	if (Cast<ABaseCharacter>(HitResult.Actor) || Cast<ATargetDummy>(HitResult.Actor))
	{
		//If no damage done doesn't stuck, start physic simulation instead
		if (Damage > 0)
		{
			UGameplayStatics::PlaySoundAtLocation(this, CharacterStuckSound, GetOwner()->GetActorLocation(), FRotator(), 1.0f, 1.0f, 0.0f, BaseAttenuation);
		}
		else
		{
			UGameplayStatics::PlaySoundAtLocation(this, DeflectSound, GetOwner()->GetActorLocation(), FRotator(), 1.0f, 1.0f, 0.0f, BaseAttenuation);
			ProjectileOwner->SimulatePhysics();
			return;
		}
	}
	else
	{
		UGameplayStatics::PlaySoundAtLocation(this, EnvironmentStuckSound, GetOwner()->GetActorLocation(), FRotator(), 1.0f, 1.0f, 0.0f, BaseAttenuation);
	}

	const FAttachmentTransformRules AttachmentTransformRules{ EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, true };
	ProjectileOwner->AttachToComponent(HitResult.GetComponent(), AttachmentTransformRules, HitResult.BoneName);
	ProjectileOwner->ProjectileMovementComponent->DestroyComponent();
	ProjectileOwner->ClearTrails();

	if (const auto RootAsPrimitive = Cast<UPrimitiveComponent>(ProjectileOwner->GetRootComponent()))
		RootAsPrimitive->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	OnStuck.Broadcast();
}

