
// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/Combat/AICombatComponent.h"
#include "ImGuiCommon.h"

UAICombatComponent::UAICombatComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	
	AiActionsSetup.Add(EAIActionType::FuckAround, FAiActionSetup(0.0f, 0.0f, 0.0f));
	AiActionsSetup.Add(EAIActionType::Strafe, FAiActionSetup(0.0f, 1000.0f, 1.5f));
	AiActionsSetup.Add(EAIActionType::Block, FAiActionSetup(0.0f, 9999.0f, 0.0f, false));
	AiActionsSetup.Add(EAIActionType::MeleeAttack, FAiActionSetup(0.0f, 300.0f, 2.0f));
	AiActionsSetup.Add(EAIActionType::CloseDistanceAttack, FAiActionSetup(300.0f, 600.0f, 5.0f));
	AiActionsSetup.Add(EAIActionType::RangedAttack,  FAiActionSetup(600.0f, 1200.0f, 2.0f, false));
	AiActionsSetup.Add(EAIActionType::Taunt,  FAiActionSetup(500.0f, 1000.0f, 10.0f));
	AiActionsSetup.Add(EAIActionType::Flex,  FAiActionSetup(0.0f, 3000.0f, 10.0f, true, 0.5f));
	
	AIActionToCooldownsSetup.Add(EAIActionType::Strafe, 0.0f);
	AIActionToCooldownsSetup.Add(EAIActionType::Block, 0.0f);
	AIActionToCooldownsSetup.Add(EAIActionType::MeleeAttack, 0.0f);
	AIActionToCooldownsSetup.Add(EAIActionType::CloseDistanceAttack, 0.0f);
	AIActionToCooldownsSetup.Add(EAIActionType::RangedAttack, 0.0f);
	AIActionToCooldownsSetup.Add(EAIActionType::Taunt, 0.0f);
	
}

void UAICombatComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(GlobalActionsCooldown > 0.0f)
	{
		GlobalActionsCooldown -= DeltaTime;
		if(GlobalActionsCooldown < 0.0f)
			GlobalActionsCooldown = 0.0f;
	}
	for (TPair<EAIActionType, float>& Pair : AIActionToCooldownsSetup)
		if(Pair.Value > 0.0f)
		{
			Pair.Value -= DeltaTime;
			if(Pair.Value < 0.0f)
				Pair.Value = 0.0f;
		}
}

void UAICombatComponent::OnActionExecuted(const EAIActionType Action)
{
	const auto ActionSetup = AiActionsSetup.Find(Action);
	ApplyActionCooldown(Action);
	GlobalActionsCooldown = ActionSetup->GlobalActionCooldown + FMath::FRandRange(0.0f, ActionSetup->GlobalActionCooldownRandomInterval);
}

bool UAICombatComponent::PerformActivationChanceCheck(const EAIActionType Action)
{
	const float ActivationChance = AiActionsSetup.Find(Action)->ActivationChance;
	//ActivationChance >= 1.0f -> always activate
	if(ActivationChance  >= 1.0f)
		return true;
	
	//ActivationChance < 1.0f -> Do check if should activate
	if(ActivationChance > FMath::FRandRange(0.0f, 1.0f))
		return true;

	//Apply action cooldown
	ApplyActionCooldown(Action);
	return false;
}

void UAICombatComponent::ApplyActionCooldown(const EAIActionType Action)
{
	const auto ActionSetup = AiActionsSetup.Find(Action);
	SetActionCooldown(Action, ActionSetup->Cooldown + FMath::FRandRange(0.0f, ActionSetup->CooldownRandomInterval));
}

void UAICombatComponent::SetActionCooldown(EAIActionType Action, float Cooldown)
{
	if(AIActionToCooldownsSetup.Find(Action))
		AIActionToCooldownsSetup.Emplace(Action, Cooldown);
}

bool UAICombatComponent::CanExecuteAnyMeaningfulAction(const float DistanceToTarget, const int32 Priority)
{
	for(const auto MeaningfulActionSetup : MeaningfulActionsSetup)
		if(const auto ActionSetup = AiActionsSetup.Find(MeaningfulActionSetup.Action))
			if(ActionSetup->bCanExecuteAction)
				if(MeaningfulActionSetup.Priority >= Priority)
					if(DistanceToTarget > ActionSetup->MinRange && DistanceToTarget < ActionSetup->MaxRange)
						if(!IsActionOnCooldown(MeaningfulActionSetup.Action))
							return true;
	
	return false;
}

float UAICombatComponent::GetDesiredRangeValue()
{
	float HighestRange = -1.0f;
	//First check if can do any meaningful action
	for(const auto MeaningfulActionSetup : MeaningfulActionsSetup)
		if(const auto ActionSetup = AiActionsSetup.Find(MeaningfulActionSetup.Action))
			//Priority zero means basically none, it shouldn't affect movement
			if(MeaningfulActionSetup.Priority > 0)
				if(ActionSetup->bCanExecuteAction)
					if(!IsActionOnCooldown(MeaningfulActionSetup.Action) && !IsOnGlobalActionCooldown())
						if(ActionSetup->MaxRange > HighestRange)
							HighestRange = ActionSetup->MaxRange;

	if(HighestRange > 0.0f)
		return HighestRange;
	
	//Otherwise go into desired range
	switch (DesiredRange)
	{
	case EAIDesiredRange::Melee:
		if(const auto MeleeAttackSetup = AiActionsSetup.Find(EAIActionType::MeleeAttack))
			return MeleeAttackSetup->MaxRange;

	case EAIDesiredRange::CloseDistanceAttack:
		if(const auto CloseDistanceAttackAttackSetup = AiActionsSetup.Find(EAIActionType::CloseDistanceAttack))
			return CloseDistanceAttackAttackSetup->MaxRange;
		
	case EAIDesiredRange::Ranged:
		if(const auto RangedAttackSetup = AiActionsSetup.Find(EAIActionType::RangedAttack))
			return RangedAttackSetup->MaxRange;
		
	default:;
	}
	
	return 2137.0f;
}

bool UAICombatComponent::IsActionOnCooldown(const EAIActionType Action)
{
	if(AIActionToCooldownsSetup.Find(Action))
		return *AIActionToCooldownsSetup.Find(Action) > 0.0f;
		
	return false;
}

bool UAICombatComponent::IsOnGlobalActionCooldown() const
{
	return GlobalActionsCooldown > 0.0f;
}

float UAICombatComponent::GetActionCooldown(const EAIActionType Action)
{
	if(AIActionToCooldownsSetup.Find(Action))
		return *AIActionToCooldownsSetup.Find(Action);
	
	return 0.0f;
}
