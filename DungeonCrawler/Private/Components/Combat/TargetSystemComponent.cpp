// Copyright 2018-2019 Mickael Daniel. All Rights Reserved.

#include "TargetSystemComponent.h"
#include "TargetSystemLockOnWidget.h"
#include "TargetSystemTargetableInterface.h"
#include "MyPlayerCameraManager.h"
#include "Engine/World.h"
#include "Engine/Public/TimerManager.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"
#include "BaseCharacter.h"
#include "DamageResponseComponent.h"
#include "EngineUtils.h"

class ULockOnTargetPositionComponent;
// Sets default values for this component's properties
UTargetSystemComponent::UTargetSystemComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	MinimumDistanceToEnable = 1200.0f;
	MaxCameraLookAtPitch = 20.0f;
	ClosestTargetDistance = 0.0f;
	TargetLocked = false;
	TargetLockedOnWidgetDrawSize = 32.0f;
	TargetLockedOnWidgetRelativeLocation = FVector(0.0f, 0.0f, 20.0f);
	BreakLineOfSightDelay = 2.0f;
	bIsBreakingLineOfSight = false;
	ShouldControlRotationWhenLockedOn = true;
	StartRotatingThreshold = 1.5f;
	bIsSwitchingTarget = false;
	ShouldDrawTargetLockedOnWidget = true;

	TargetableActors = APawn::StaticClass();
}

// Called when the game starts
void UTargetSystemComponent::BeginPlay()
{
	Super::BeginPlay();
	CharacterReference = GetOwner();
	if (!CharacterReference)
	{
		UE_LOG(LogTemp, Error, TEXT("[%s] TargetSystemComponent: Cannot get Owner reference ..."), *this->GetName());
		return;
	}

	PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (!PlayerController)
		UE_LOG(LogTemp, Error, TEXT("[%s] TargetSystemComponent: Cannot get PlayerController reference ..."), *CharacterReference->GetName());
}

void UTargetSystemComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (TargetLocked && NearestTarget)
	{
		if (!TargetIsTargetable(NearestTarget))
		{
			TargetLockOff();
			return;
		}

		if(ShouldControlRotationWhenLockedOn)
			SetControlRotationOnTarget();
		
		//UpdateCameraPitchToLookAtTarget();
		// Target Locked Off based on Distance
		if (GetDistanceFromCharacter(NearestTarget) > MinimumDistanceToEnable)
			TargetLockOff();

		if (ShouldBreakLineOfSight() && !bIsBreakingLineOfSight)
		{
			if (BreakLineOfSightDelay <= 0)
				TargetLockOff();
			else
			{
				bIsBreakingLineOfSight = true;
				GetWorld()->GetTimerManager().SetTimer(
					LineOfSightBreakTimerHandle,
					this,
					&UTargetSystemComponent::BreakLineOfSight,
					BreakLineOfSightDelay
				);
			}
		}
	}
}

void UTargetSystemComponent::TargetActor()
{
	ClosestTargetDistance = MinimumDistanceToEnable;

	if (TargetLocked)
		TargetLockOff();
	else
	{
		const TArray<AActor*> Actors = GetAllActorsOfClass(TargetableActors);
		NearestTarget = FindNearestTarget(Actors);

		//If there is no target zero camera control rotation
		if(NearestTarget)
			TargetLockOn(NearestTarget);
		else
			if(const auto OwnerPawn = Cast<APawn>(GetOwner()))
				if(const auto Controller = OwnerPawn->GetController())
					Controller->SetControlRotation(FRotator(0.0f, OwnerPawn->GetActorRotation().Yaw, 0.0f));
	}
}

void UTargetSystemComponent::TargetActorWithAxisInput(const float AxisValue)
{
	// If we're not locked on, do nothing
	if (!TargetLocked)
		return;

	// If we're switching target, do nothing for a set amount of time
	if (bIsSwitchingTarget)
		return;

	// Lock off target
	AActor* CurrentTarget = NearestTarget;

	// Depending on Axis Value negative / positive, set Direction to Look for (negative: left, positive: right)
	const float RangeMin = AxisValue < 0.0f ? 0.0f : 180.0f;
	const float RangeMax = AxisValue < 0.0f ? 180.0f : 360.0f;

	// Reset Closest Target Distance to Minimum Distance to Enable
	ClosestTargetDistance = MinimumDistanceToEnable;

	// Get All Actors of Class
	TArray<AActor*> Actors = GetAllActorsOfClass(TargetableActors);

	// For each of these actors, check line trace and ignore Current Target and build the list of actors to look from
	TArray<AActor*> ActorsToLook;

	TArray<AActor*> ActorsToIgnore;
	ActorsToIgnore.Add(CurrentTarget);
	for (AActor* Actor : Actors)
	{
		const bool bHit = LineTraceForActor(Actor, ActorsToIgnore);
		if (bHit && IsInViewport(Actor))
			ActorsToLook.Add(Actor);
	}

	// Find Targets in Range (left or right, based on Character and CurrentTarget)
	TArray<AActor*> TargetsInRange = FindTargetsInRange(ActorsToLook, RangeMin, RangeMax);

	// For each of these targets in range, get the closest one to current target
	AActor* ActorToTarget = nullptr;
	for (AActor* Actor : TargetsInRange)
	{
		if(!IsInViewport(Actor))
			continue;

		// and filter out any character too distant from minimum distance to enable
		const float Distance = GetDistanceFromCharacter(Actor);
		if (Distance < MinimumDistanceToEnable)
		{
			const float RelativeActorsDistance = CurrentTarget->GetDistanceTo(Actor);
			if (RelativeActorsDistance < ClosestTargetDistance)
			{
				ClosestTargetDistance = RelativeActorsDistance;
				ActorToTarget = Actor;
			}
		}
	}

	if (ActorToTarget)
	{
		bIsSwitchingTarget = true;
		TargetLockOff();
		NearestTarget = ActorToTarget;
		TargetLockOn(ActorToTarget);
		GetWorld()->GetTimerManager().SetTimer(
			SwitchingTargetTimerHandle,
			this,
			&UTargetSystemComponent::ResetIsSwitchingTarget,
			0.5f
		);
	}
}

TArray<AActor*> UTargetSystemComponent::FindTargetsInRange(TArray<AActor*> ActorsToLook, const float RangeMin, const float RangeMax) const
{
	TArray<AActor*> ActorsInRange;

	for (AActor* Actor : ActorsToLook)
	{
		const float Angle = GetAngleUsingCameraRotation(Actor);
		if (Angle >= RangeMin && Angle <= RangeMax)
			ActorsInRange.Add(Actor);
	}

	return ActorsInRange;
}

float UTargetSystemComponent::GetAngleUsingCameraRotation(const AActor* ActorToLook) const
{
	USkeletalMeshComponent* CameraComponent = nullptr;
	//Only for player, if it will ever be needed make generic logic for every character
	if(const auto CameraManager = AMyPlayerCameraManager::GetMyPlayerCameraManager(this))
		CameraComponent = CameraManager->GetCameraComponent();

	if (!CameraComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("TargetSystem::GetAngleUsingCameraRotation() Cannot get reference to camera component"));
		return 0.0f;
	}

	const FRotator CameraWorldRotation = CameraComponent->GetComponentRotation();
	const FRotator LookAtRotation = FindLookAtRotation(CameraComponent->GetComponentLocation(), ActorToLook->GetActorLocation());

	float YawAngle = CameraWorldRotation.Yaw - LookAtRotation.Yaw;
	if (YawAngle < 0)
		YawAngle = YawAngle + 360;

	return YawAngle;
}

FRotator UTargetSystemComponent::FindLookAtRotation(const FVector Start, const FVector Target)
{
	return FRotationMatrix::MakeFromX(Target - Start).Rotator();
}

void UTargetSystemComponent::ResetIsSwitchingTarget()
{
	bIsSwitchingTarget = false;
}

void UTargetSystemComponent::TargetLockOn(AActor* TargetToLockOn)
{
	TargetetActor = TargetToLockOn;
	if (TargetToLockOn)
	{
		TargetLocked = true;
		if (ShouldDrawTargetLockedOnWidget)
			CreateAndAttachTargetLockedOnWidgetComponent(TargetToLockOn);
		
		PlayerController->SetIgnoreLookInput(true);
		if (OnTargetLockedOn.IsBound())
			OnTargetLockedOn.Broadcast(TargetToLockOn);
		
		if(TargetToLockOn->GetClass()->ImplementsInterface(UTargetSystemTargetableInterface::StaticClass()))
			ITargetSystemTargetableInterface::Execute_OnBeingLockedOn(TargetToLockOn);

		//If target is base character and died then lock off
		if(const auto TargetDamageResponseComponent = Cast<UDamageResponseComponent>(TargetToLockOn->GetComponentByClass(UDamageResponseComponent::StaticClass())))
			TargetDamageResponseComponent->OnDeath.AddDynamic(this, &UTargetSystemComponent::TargetLockOff);
		
	}
}

void UTargetSystemComponent::TargetLockOff()
{
	if (TargetetActor && TargetetActor->GetClass()->ImplementsInterface(UTargetSystemTargetableInterface::StaticClass()))
		ITargetSystemTargetableInterface::Execute_OnBeingLockedOff(TargetetActor);

	if(TargetetActor)
		if(const auto TargetDamageResponseComponent = Cast<UDamageResponseComponent>(TargetetActor->GetComponentByClass(UDamageResponseComponent::StaticClass())))
			TargetDamageResponseComponent->OnDeath.RemoveDynamic(this, &UTargetSystemComponent::TargetLockOff);

	TargetLocked = false;
	TargetetActor = nullptr;
	if (TargetLockedOnWidgetComponent->IsValidLowLevel())
		TargetLockedOnWidgetComponent->DestroyComponent();

	if (NearestTarget)
	{
		PlayerController->ResetIgnoreLookInput();
		if (OnTargetLockedOff.IsBound())
			OnTargetLockedOff.Broadcast(NearestTarget);
		
	}
	NearestTarget = nullptr;
}

void UTargetSystemComponent::CreateAndAttachTargetLockedOnWidgetComponent(AActor* TargetActor)
{
	TargetLockedOnWidgetComponent = NewObject<UWidgetComponent>(TargetActor, FName("TargetLockOn"));
	if (TargetLockedOnWidgetClass)
		TargetLockedOnWidgetComponent->SetWidgetClass(TargetLockedOnWidgetClass);
	else
		TargetLockedOnWidgetComponent->SetWidgetClass(UTargetSystemLockOnWidget::StaticClass());

	TargetLockedOnWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
	if (TargetActor->GetClass()->ImplementsInterface(UTargetSystemTargetableInterface::StaticClass()))
		TargetLockedOnWidgetComponent->SetupAttachment(ITargetSystemTargetableInterface::Execute_GetAttachComponent(TargetActor));
	else
		TargetLockedOnWidgetComponent->SetupAttachment(TargetActor->GetRootComponent());

	TargetLockedOnWidgetComponent->SetRelativeLocation(TargetLockedOnWidgetRelativeLocation);
	TargetLockedOnWidgetComponent->SetDrawSize(FVector2D(TargetLockedOnWidgetDrawSize, TargetLockedOnWidgetDrawSize));
	TargetLockedOnWidgetComponent->SetVisibility(true);
	TargetLockedOnWidgetComponent->RegisterComponent();
}

TArray<AActor*> UTargetSystemComponent::GetAllActorsOfClass(const TSubclassOf<AActor> ActorClass) const
{
	TArray<AActor*> Actors;
		
	for (TActorIterator<AActor> ActorIterator(GetWorld(), ActorClass); ActorIterator; ++ActorIterator)
	{
		AActor* Actor = *ActorIterator;
		const bool IsTargetable = TargetIsTargetable(Actor);
		if (IsTargetable)
			Actors.Add(Actor);
	}

	return Actors;
}

bool UTargetSystemComponent::TargetIsTargetable(const AActor* Actor) const
{
	bool bIsTargetable = false;
	if (Actor->GetClass()->ImplementsInterface(UTargetSystemTargetableInterface::StaticClass()))
		bIsTargetable = ITargetSystemTargetableInterface::Execute_IsTargetable(Actor);

	//Do not target friends
	bool bIsEnemy = true;
	if(const IGenericTeamAgentInterface* TeamInterface = Cast<IGenericTeamAgentInterface>(GetOwner()))
		bIsEnemy =  TeamInterface->GetTeamAttitudeTowards(*Actor) != ETeamAttitude::Friendly;
	
	return bIsTargetable && bIsEnemy;
}

void UTargetSystemComponent::UpdateCameraPitchToLookAtTarget() const
{
	const auto CameraComponent = AMyPlayerCameraManager::GetMyPlayerCameraManager(this)->GetCameraComponent();
	if (!CameraComponent)
		UE_LOG(LogTemp, Error, TEXT("TargetSystem::GetAngleUsingCameraRotation() Cannot get reference to camera component"));

	FRotator CameraWorldRotation = CameraComponent->GetComponentRotation();
	const FRotator LookAtRotation = FindLookAtRotation(CameraComponent->GetComponentLocation(), TargetLockedOnWidgetComponent->GetComponentLocation());
		
	CameraWorldRotation.Pitch = LookAtRotation.Pitch;
	CameraComponent->SetWorldRotation(CameraWorldRotation);
}

AActor* UTargetSystemComponent::FindNearestTarget(TArray<AActor*> Actors) const
{
	TArray<AActor*> ActorsHit;

	// Find all actors we can line trace to
	for (AActor* Actor : Actors)
	{
		const bool bHit = LineTraceForActor(Actor);
		if (bHit && IsInViewport(Actor))
			ActorsHit.Add(Actor);
	}

	// From the hit actors, check distance and return the nearest
	if (ActorsHit.Num() == 0)
		return nullptr;

	float ClosestDistance = ClosestTargetDistance;
	AActor* Target = nullptr;
	for (AActor* Actor : ActorsHit)
	{
		const float Distance = GetDistanceFromCharacter(Actor);
		if (Distance < ClosestDistance)
		{
			ClosestDistance = Distance;
			Target = Actor;
		}
	}

	return Target;
}


bool UTargetSystemComponent::LineTraceForActor(AActor* OtherActor, const TArray<AActor*> ActorsToIgnore) const
{
	FHitResult HitResult;
	const bool bHit = LineTrace(HitResult, OtherActor, ActorsToIgnore);
	if (bHit)
	{
		AActor* HitActor = HitResult.GetActor();
		if (HitActor == OtherActor)
			return true;
	}
	return false;
}

bool UTargetSystemComponent::LineTrace(FHitResult& HitResult, const AActor* OtherActor, const TArray<AActor*> ActorsToIgnore) const
{
	FCollisionQueryParams Params = FCollisionQueryParams(FName("LineTraceSingle"));

	TArray<AActor*> IgnoredActors;
	IgnoredActors.Init(CharacterReference, 1);
	IgnoredActors += ActorsToIgnore;

	Params.AddIgnoredActors(IgnoredActors);

	return GetWorld()->LineTraceSingleByChannel(
		HitResult,
		CharacterReference->GetActorLocation(),
		OtherActor->GetActorLocation(),
		ECC_Pawn,
		Params
	);
}

FRotator UTargetSystemComponent::GetControlRotationOnTarget() const
{
	const FRotator ControlRotation = PlayerController->GetControlRotation();
	const FVector CharacterLocation = CharacterReference->GetActorLocation();

	FVector LocationAdjustment;
	if(TargetetActor)
		if (TargetetActor->GetClass()->ImplementsInterface(UTargetSystemTargetableInterface::StaticClass()))
			LocationAdjustment = ITargetSystemTargetableInterface::Execute_GetLookAtPositionAdjustment(TargetetActor);
		
	const FVector OtherActorLocation = TargetLockedOnWidgetComponent->GetComponentLocation() + LocationAdjustment;

	// Find look at rotation
	FRotator LookRotation = FRotationMatrix::MakeFromX(OtherActorLocation - CharacterLocation).Rotator();

	if(LookRotation.Pitch > MaxCameraLookAtPitch)
		LookRotation.Pitch = MaxCameraLookAtPitch;
	
	const FRotator TargetRotation = FRotator(LookRotation.Pitch, LookRotation.Yaw, ControlRotation.Roll);

	return FMath::RInterpTo(ControlRotation, TargetRotation, GetWorld()->GetDeltaSeconds(), 5.0f);
}

void UTargetSystemComponent::SetControlRotationOnTarget() const
{
	if (!PlayerController)
		return;

	PlayerController->SetControlRotation(GetControlRotationOnTarget());
}

float UTargetSystemComponent::GetDistanceFromCharacter(const AActor* OtherActor) const
{
	return CharacterReference->GetDistanceTo(OtherActor);
}

bool UTargetSystemComponent::ShouldBreakLineOfSight() const
{
	if (!NearestTarget)
		return true;

	TArray<AActor*> ActorsToIgnore = GetAllActorsOfClass(TargetableActors);
	ActorsToIgnore.Remove(NearestTarget);

	FHitResult HitResult;
	const bool bHit = LineTrace(HitResult, NearestTarget, ActorsToIgnore);
	if (bHit && HitResult.GetActor() != NearestTarget)
		return true;

	return false;
}

void UTargetSystemComponent::BreakLineOfSight()
{
	bIsBreakingLineOfSight = false;
	if (ShouldBreakLineOfSight())
		TargetLockOff();
}

bool UTargetSystemComponent::IsInViewport(const AActor* TargetActor) const
{
	if (!PlayerController)
		return true;

	FVector2D ScreenLocation;
	PlayerController->ProjectWorldLocationToScreen(TargetActor->GetActorLocation(), ScreenLocation);

	FVector2D ViewportSize;
	GetWorld()->GetGameViewport()->GetViewportSize(ViewportSize);

	return ScreenLocation.X > 0 && ScreenLocation.Y > 0 && ScreenLocation.X < ViewportSize.X && ScreenLocation.Y < ViewportSize.Y;
}
