// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/Character.h"
#include "AbilitySystemComponent.h"
#include "GameplayTagContainer.h"
#include "AbilitySystemGlobals.h"
#include "GameConfigComponent.h"
#include "GameplayEffectTypes.h"
#include "Engine/EngineTypes.h"
#include "Engine/World.h"
#include "MeleeWeapon.h"
#include "Weapon.h"
#include "Enums.h"

UCombatComponent::UCombatComponent()
{
	RightHandAttachmentSocket = FName("RightHand");
	LeftHandAttachmentSocket = FName("LeftHand");
	RightLegAttachmentSocket = FName("RightLeg");
	LeftLegAttachmentSocket = FName("LeftLeg");
}

void UCombatComponent::Init()
{
	EquipWeaponByClass(WeaponClass);
	
	if(!WeaponClass)
		WeaponClass = UGameConfigComponent::GetGameConfigComponent(this)->RightHandWeaponClass;

	SpawnAndAttachSecondaryWeapon();
	SpawnAndAttachLegWeapons();
}

void UCombatComponent::BeginPlay()
{
	Super::BeginPlay();
	Init();
}

void UCombatComponent::EquipWeaponByClass(const TSubclassOf<AWeapon> WeaponClassToEquip)
{
	if(RightHandWeapon)
	{ 
		RightHandWeapon->K2_DestroyActor();
		RightHandWeapon = nullptr;
		UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->RemoveActiveGameplayEffect(EquippedRightWeaponEffectHandle);
	}
	
	if (LeftHandWeapon)
	{
		LeftHandWeapon->K2_DestroyActor();
		LeftHandWeapon = nullptr;
		UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->RemoveActiveGameplayEffect(EquippedLeftWeaponEffectHandle);
		OnWeaponEquipped.Broadcast(LeftHandWeapon);
	}
	
	RightHandWeapon = SpawnAndAttachWeapon(WeaponClassToEquip, EWeaponSocket::RightHand);

	if(!RightHandWeapon)
		RightHandWeapon = SpawnAndAttachWeapon(UGameConfigComponent::GetGameConfigComponent(this)->RightHandWeaponClass, EWeaponSocket::RightHand);

	if(RightHandWeapon)
	{
		WeaponClass = WeaponClassToEquip;
		EquippedRightWeaponEffectHandle = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->BP_ApplyGameplayEffectToSelf(RightHandWeapon->EquippedGameplayEffectClass, 0.0f, FGameplayEffectContextHandle());
		OnWeaponEquipped.Broadcast(RightHandWeapon);
	}
	
	PostEquipActions();
}

void UCombatComponent::SwitchWeapon()
{
	UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->RemoveActiveGameplayEffect(EquippedRightWeaponEffectHandle);
	const auto SecondaryRightWeaponCache = SecondaryRightHandWeapon;
	SecondaryRightHandWeapon = RightHandWeapon;
	RightHandWeapon = SecondaryRightWeaponCache;

	if(RightHandWeapon)
		EquippedRightWeaponEffectHandle = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->BP_ApplyGameplayEffectToSelf(RightHandWeapon->EquippedGameplayEffectClass, 0.0f, FGameplayEffectContextHandle());

	UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->RemoveActiveGameplayEffect(EquippedLeftWeaponEffectHandle);
	const auto SecondaryLeftWeaponCache = SecondaryLeftHandWeapon;
	SecondaryLeftHandWeapon = LeftHandWeapon;
	LeftHandWeapon = SecondaryLeftWeaponCache;

	if(LeftHandWeapon)
		EquippedLeftWeaponEffectHandle = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->BP_ApplyGameplayEffectToSelf(LeftHandWeapon->EquippedGameplayEffectClass, 0.0f, FGameplayEffectContextHandle());

	if(RightHandWeapon)
		WeaponClass = RightHandWeapon->GetClass();
	
	if(SecondaryRightHandWeapon)
		SecondaryWeaponClass = SecondaryRightHandWeapon->GetClass();
		
	if(SecondaryLeftHandWeapon)
		SecondaryLeftWeaponClass = SecondaryLeftHandWeapon->GetClass();
}

void UCombatComponent::InstantlySwitchWeapon()
{
	SheathWeapon(GetWeaponInSocket(EWeaponSocket::RightHand));
	SheathWeapon(GetWeaponInSocket(EWeaponSocket::LeftHand));
	SwitchWeapon();
	DrawWeapon(GetWeaponInSocket(EWeaponSocket::RightHand));
	DrawWeapon(GetWeaponInSocket(EWeaponSocket::LeftHand));
}

bool UCombatComponent::HasWeapon() const
{
	return IsValid(RightHandWeapon);
}

AWeapon* UCombatComponent::GetWeaponInSocket(const EWeaponSocket Socket) const
{
	switch (Socket)
	{
		case EWeaponSocket::RightHand:
			return RightHandWeapon;
		case EWeaponSocket::LeftHand:
			return LeftHandWeapon;		
		case EWeaponSocket::RightLeg:
			return RightLeg;			
		case EWeaponSocket::LeftLeg:
			return LeftLeg;
		default:
			return nullptr;
	}
}

AWeapon* UCombatComponent::GetSecondaryWeaponInSocket(const EWeaponSocket Socket) const
{
	switch (Socket)
	{
	case EWeaponSocket::RightHand:
		return SecondaryRightHandWeapon;
	case EWeaponSocket::LeftHand:
		return SecondaryLeftHandWeapon;		
	case EWeaponSocket::RightLeg:
		return RightLeg;			
	case EWeaponSocket::LeftLeg:
		return LeftLeg;
	default:
		return nullptr;
	}
}

void UCombatComponent::CalculateWeaponAttack(const EWeaponSocket WeaponSocket, const float DamageMultiplier, const int32 ExtraAttackStaggerStrength) const
{
	AWeapon* Weapon;
	switch (WeaponSocket)
	{
		case EWeaponSocket::RightHand:
			Weapon = RightHandWeapon;
			break;
		case EWeaponSocket::LeftHand:
			Weapon = LeftHandWeapon;
			break;
		case EWeaponSocket::RightLeg:
			Weapon = RightLeg;
			break;
		case EWeaponSocket::LeftLeg:
			Weapon = LeftLeg;
			break;
		default:
			Weapon = nullptr;
		}

	 if (const auto MeleeWeapon = Cast<AMeleeWeapon>(Weapon))
		 MeleeWeapon->CalculateWeaponDamage(DamageMultiplier, ExtraAttackStaggerStrength);
}

TArray<AWeapon*> UCombatComponent::GetWeapons() const 
{
	TArray<AWeapon*> Weapons;
	if (RightHandWeapon)
		Weapons.Add(RightHandWeapon);
	if (LeftHandWeapon)
		Weapons.Add(LeftHandWeapon);
	if (RightLeg)
		Weapons.Add(RightLeg);
	if (LeftLeg)
		Weapons.Add(LeftLeg);

	return Weapons;
}

TArray<AWeapon*> UCombatComponent::GetSecondaryWeapons() const
{
	TArray<AWeapon*> Weapons;
	if(SecondaryRightHandWeapon)
		Weapons.Add(SecondaryRightHandWeapon);
	if(SecondaryLeftHandWeapon)
		Weapons.Add(SecondaryLeftHandWeapon);
	
	return Weapons;
}

void UCombatComponent::SheathWeapon(AWeapon* Weapon) const
{
	const auto CharacterOwner = Cast<ACharacter>(GetOwner());
	if(!CharacterOwner)
		return;
	
	if(Weapon)
	{
		Weapon->AttachToComponent(CharacterOwner->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, Weapon->SheathSocket);
		Weapon->SetActorRelativeLocation(FVector::ZeroVector);
		Weapon->SetActorRelativeRotation(FRotator::ZeroRotator);
	}
}

void UCombatComponent::DrawWeapon(AWeapon* Weapon) const
{
	const auto CharacterOwner = Cast<ACharacter>(GetOwner());
	if(!CharacterOwner)
		return;
	
	if(Weapon)
	{
		Weapon->AttachToComponent(CharacterOwner->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, Weapon->EquipSocket);
		Weapon->SetActorRelativeLocation(Weapon->LocationCorrection);
		Weapon->SetActorRelativeRotation(Weapon->RotationCorrection);
	}
}

AWeapon* UCombatComponent::SpawnAndAttachWeapon(const TSubclassOf< AWeapon> WeaponClassToSpawn, const EWeaponSocket WeaponSocket) const
{
	if (!WeaponClassToSpawn->IsValidLowLevel())
		return nullptr;

	const auto CharacterOwner = Cast<ACharacter>(GetOwner());
	if(!CharacterOwner)
		return nullptr;

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Owner = CharacterOwner;
	SpawnInfo.Instigator = CharacterOwner;
	const auto Weapon = GetWorld()->SpawnActor<AWeapon>(WeaponClassToSpawn, FVector::ZeroVector, FRotator::ZeroRotator, SpawnInfo);
	FName AttachmentSocket;
	switch (WeaponSocket)
	{
		case EWeaponSocket::RightHand:
			AttachmentSocket = RightHandAttachmentSocket;
			break;
		case EWeaponSocket::LeftHand:
			AttachmentSocket = LeftHandAttachmentSocket;
			break;
		case EWeaponSocket::RightLeg:
			AttachmentSocket = RightLegAttachmentSocket;
			break;
		case EWeaponSocket::LeftLeg:
			AttachmentSocket = LeftLegAttachmentSocket;
			break;
		case EWeaponSocket::Secondary:
			AttachmentSocket = Weapon->SheathSocket;
			break;
	}

	if(Weapon)
	{
		if(WeaponSocket != EWeaponSocket::Secondary)
		{
			Weapon->SetActorRelativeLocation(Weapon->LocationCorrection);
			Weapon->SetActorRelativeRotation(Weapon->RotationCorrection);
		}
		Weapon->AttachToComponent(CharacterOwner->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, AttachmentSocket);
	}
	
	return Weapon;
}

void UCombatComponent::SpawnAndAttachLegWeapons()
{
	RightLeg = SpawnAndAttachWeapon(UGameConfigComponent::GetGameConfigComponent(this)->RightLegWeaponClass, EWeaponSocket::RightLeg);
	OnWeaponEquipped.Broadcast(RightLeg);
	LeftLeg = SpawnAndAttachWeapon(UGameConfigComponent::GetGameConfigComponent(this)->LeftLegWeaponClass, EWeaponSocket::LeftLeg);
	OnWeaponEquipped.Broadcast(LeftLeg);

}

void UCombatComponent::SpawnAndAttachSecondaryWeapon()
{
	if(!SecondaryWeaponClass)
		SecondaryWeaponClass = UGameConfigComponent::GetGameConfigComponent(this)->RightHandWeaponClass;
	
	if(SecondaryWeaponClass)
		SecondaryRightHandWeapon = SpawnAndAttachWeapon(SecondaryWeaponClass, EWeaponSocket::Secondary);

	if(!SecondaryLeftWeaponClass)
		SecondaryLeftWeaponClass = UGameConfigComponent::GetGameConfigComponent(this)->LeftHandWeaponClass;
	
	if(SecondaryLeftWeaponClass)
		SecondaryLeftHandWeapon = SpawnAndAttachWeapon(SecondaryLeftWeaponClass, EWeaponSocket::Secondary);
	
}

void UCombatComponent::PostEquipActions() 
{
	if (!RightHandWeapon)
		return;

	RightHandWeapon->SetOwner(GetOwner());
	switch (RightHandWeapon->WeaponType)
	{
		case EWeaponType::TwoHanded:
			break;
		case EWeaponType::Katana:
			break;
		case EWeaponType::OneHandedAndShield:
			LeftHandWeapon = SpawnAndAttachWeapon(RightHandWeapon->SecondWeaponClass, EWeaponSocket::LeftHand);
			EquippedLeftWeaponEffectHandle = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->BP_ApplyGameplayEffectToSelf(LeftHandWeapon->EquippedGameplayEffectClass, 0.0f, FGameplayEffectContextHandle());
			OnWeaponEquipped.Broadcast(LeftHandWeapon);
			break;
		case EWeaponType::Dual:
			LeftHandWeapon = SpawnAndAttachWeapon(RightHandWeapon->SecondWeaponClass, EWeaponSocket::LeftHand);
			EquippedLeftWeaponEffectHandle = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->BP_ApplyGameplayEffectToSelf(LeftHandWeapon->EquippedGameplayEffectClass, 0.0f, FGameplayEffectContextHandle());
			OnWeaponEquipped.Broadcast(LeftHandWeapon);
			break;
		case EWeaponType::Assassin:
			break;
		case EWeaponType::Shield:
			break;
		case EWeaponType::Greatsword:
			break;
		case EWeaponType::BodyPart:
			break;
		case EWeaponType::OneHanded:
			break;
		case EWeaponType::Giant:
			if(RightHandWeapon->SecondWeaponClass)
			{
				LeftHandWeapon = SpawnAndAttachWeapon(RightHandWeapon->SecondWeaponClass, EWeaponSocket::LeftHand);
				EquippedLeftWeaponEffectHandle = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->BP_ApplyGameplayEffectToSelf(LeftHandWeapon->EquippedGameplayEffectClass, 0.0f, FGameplayEffectContextHandle());
				OnWeaponEquipped.Broadcast(LeftHandWeapon);
			}
			break;
		case EWeaponType::OrientalSpear:
			break;
		default:
			break;
	}
	if(!LeftHandWeapon)
	{
		LeftHandWeapon = SpawnAndAttachWeapon(UGameConfigComponent::GetGameConfigComponent(this)->LeftHandWeaponClass, EWeaponSocket::LeftHand);
		OnWeaponEquipped.Broadcast(LeftHandWeapon);
	}
}
