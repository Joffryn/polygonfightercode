// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/Combat/RotationComponent.h"

#include "AIController.h"
#include "GameFramework/PawnMovementComponent.h"
#include "MyCharacterMovementComponent.h"
#include "GameplayTagAssetInterface.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "BaseCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"

URotationComponent::URotationComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void URotationComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bShouldRotate)
		RotateTowardTarget(DeltaTime);

	if(bWantsToStrafe)
		Strafe();
}

void URotationComponent::SetCharacterRotation(const FRotator NewRequestedRotation, const bool bInterp /*= false*/, const float InterpSpeed /*= 0.0f*/, float AcceptableYawDiff /*= 5.0f*/)
{
	RequestedRotation = NewRequestedRotation;
	if (!bInterp)
		CharacterRotation = RequestedRotation;
	else
		CharacterRotation = UKismetMathLibrary::RInterpTo(CharacterRotation, RequestedRotation, UGameplayStatics::GetWorldDeltaSeconds(this), InterpSpeed);

	CharacterRotation.Pitch = 0.0f;
	GetOwner()->SetActorRotation(CharacterRotation);
}

void URotationComponent::SetTarget(AActor* NewTarget)
{
	Target = NewTarget;
}

void URotationComponent::RotateTowardTarget(const float DeltaTime)
{
	if (!Target)
		return;

	//Never rotate when having a move request, it should handle rotation
	if(const auto PawnOwner = Cast<APawn>(GetOwner()))
		if(const auto AIController = Cast<AAIController>(PawnOwner->GetController()))
			if(AIController->GetMoveStatus() == EPathFollowingStatus::Moving)
				return;
	
	if(bWithDebug)
	{
		GEngine->AddOnScreenDebugMessage(-1, 0.0f, FColor::Emerald, TEXT("Current Rotation = ") + GetOwner()->GetActorRotation().ToString());
		GEngine->AddOnScreenDebugMessage(-1, 0.0f, FColor::Magenta, TEXT("Desired Rotation = ") + GetLookAtRotation().ToString());
	}
		
	if (!CanTurn())
		return;
	
	const FRotator CalculatedRotation = UKismetMathLibrary::RInterpTo_Constant(GetOwner()->GetActorRotation(),GetLookAtRotation(), DeltaTime, AIRotationSpeed * RotationSpeedMultiplier);
	
	SetCharacterRotation(CalculatedRotation);
}

void URotationComponent::Strafe() const
{
	if(!Target)
		return;

	if (const auto OwnerBaseCharacter = Cast<ABaseCharacter>(GetOwner()))
	{
		const FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(Target->GetActorLocation(), GetOwner()->GetActorLocation());
		FVector InputVector = UKismetMathLibrary::GetRightVector(LookAtRotation);
		if(!bStrafeRight)
			InputVector *= -1;
		OwnerBaseCharacter->GetMyCharacterMovementComponent()->AddInputVector(InputVector);
	}
}

FRotator URotationComponent::GetLookAtRotation() const
{
	FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(Target->GetActorLocation(), GetOwner()->GetActorLocation());
	LookAtRotation.Yaw += 180.0f;
	LookAtRotation.Pitch = 0.0f;
	
	return LookAtRotation;
}

bool URotationComponent::CanTurn() const
{
	if (const auto GameplayTagInterface = Cast<IGameplayTagAssetInterface>(GetOwner()))
		if(GameplayTagInterface->HasAnyMatchingGameplayTags(BlockingGameplayTags))
			return false;

	if(const auto Blackboard = UAIBlueprintHelperLibrary::GetBlackboard(GetOwner()))
		if(!Blackboard->GetValueAsBool(IsInRangeBlackboardKeyName))
			return false;
	
	return true;
}

void URotationComponent::StartStrafing()
{
	if(!bWantsToStrafe)
	{ 
		bWantsToStrafe = true;
		bStrafeRight = FMath::RandBool();
	}
}

void URotationComponent::StopStrafing()
{
	bWantsToStrafe = false;
	OnStrafingEnded.Broadcast();
}

void URotationComponent::Reset()
{
	StopStrafing();
	Target = nullptr;
}
