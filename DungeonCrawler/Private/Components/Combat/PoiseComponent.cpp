// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/Combat/PoiseComponent.h"

#include "BaseCharacter.h"
#include "MyAbilitySystemComponent.h"

UPoiseComponent::UPoiseComponent()
{
}

void UPoiseComponent::SetPoise(const EPoiseLevel NewPoiseLevel)
{
	PoiseLevel = NewPoiseLevel;
	HandlePoiseChange();
}

void UPoiseComponent::ChangePoiseLevel(const int32 Amount)
{
	ActualPoiseLevel += Amount;
	int32 CurrentPoiseValue = FMath::Clamp(ActualPoiseLevel, static_cast<int32>(EPoiseLevel::None), static_cast<int32>(EPoiseLevel::Unstaggerable));
	SetPoise(static_cast<EPoiseLevel>(CurrentPoiseValue));
}

void UPoiseComponent::BeginPlay()
{
	Super::BeginPlay();
	HandlePoiseChange();
	ActualPoiseLevel = static_cast<int32>(PoiseLevel);
}

void UPoiseComponent::HandlePoiseChange()
{
	//Only works on MyAbilitySystemComponent, consider refactoring
	if(const auto  Abs = Cast<UMyAbilitySystemComponent>(GetOwner()->GetComponentByClass(UMyAbilitySystemComponent::StaticClass())))
	{
		auto DefaultPoiseTags = PoiseTags;
		const auto PoiseTag = *PoiseLevelToTagsMap.Find(PoiseLevel);
		DefaultPoiseTags.RemoveTag(PoiseTag);
		Abs->HandleTagChange(PoiseTag, DefaultPoiseTags,FGameplayTagContainer());
	}
}

