
#include "Components/Combat/CritSolver.h"

#include "AbilitySystemComponent.h"
#include "CombatComponent.h"
#include "MyGameMode.h"

UCritSolver* UCritSolver::GetCritSolver(const UObject* WorldContextObject)
{
	return AMyGameMode::GetMyGameMode(WorldContextObject)->CritSolver;
}

bool UCritSolver::IsAttackCrit(AActor* Attacker, AActor* Attacked) const
{
	//Should be always critted
	if(Attacked)
		if(const auto AttackedAbilitySystemComponent = Cast<UAbilitySystemComponent>(Attacked->GetComponentByClass(UAbilitySystemComponent::StaticClass())))
			if(AttackedAbilitySystemComponent->HasMatchingGameplayTag(AlwaysCrittedTag))
				return true;
	
	if(Attacker)
	{
		const auto AttackerAbilitySystemComponent = Cast<UAbilitySystemComponent>(Attacker->GetComponentByClass(UAbilitySystemComponent::StaticClass()));
		const auto AttackerCombatComponent = Cast<UCombatComponent>(Attacker->GetComponentByClass(UCombatComponent::StaticClass()));

		//Sprint crit attack
		if(AttackerCombatComponent && AttackerAbilitySystemComponent)
			if(AttackerAbilitySystemComponent->HasMatchingGameplayTag(SprintAttackCritUpgradeTag))
				if(AttackerCombatComponent->CurrentAttackExtraTags.HasTag(SprintAttackTag))
					return true;
		
	}		
	return false;
}
