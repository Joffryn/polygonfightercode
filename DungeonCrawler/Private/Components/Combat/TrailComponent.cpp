// Fill out your copyright notice in the Description page of Project Settings.

#include "TrailComponent.h"
#include "Particles/TypeData/ParticleModuleTypeDataAnimTrail.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/MeshComponent.h"
#include "ParticleEmitterInstances.h"
#include "GameFramework/Character.h"
#include "GameFramework/Actor.h"
#include "GameConfigComponent.h"
#include "TrailInterface.h"
#include "UObject/UObjectHash.h"

typedef TInlineComponentArray<UParticleSystemComponent*, 8> FParticleSystemComponentArray;

static void GetCandidateSystems(const UMeshComponent& MeshComp, FParticleSystemComponentArray& Components)
{
	if (const AActor* Owner = MeshComp.GetOwner())
	{
		Owner->GetComponents(Components);
	}
	else
	{
		// No actor owner in some editor windows. Get PSCs spawned by the MeshComp.
		ForEachObjectWithOuter(&MeshComp, [&Components](UObject* Child)
			{
				if (UParticleSystemComponent* ChildPSC = Cast<UParticleSystemComponent>(Child))
					Components.Add(ChildPSC);
			
			}, false, RF_NoFlags, EInternalObjectFlags::PendingKill);
	}
}

// Sets default values for this component's properties
UTrailComponent::UTrailComponent()
{
	FirstSocketName = FName("TrailStart");
	SecondSocketName = FName("TrailEnd");
	WidthScaleMode = ETrailWidthMode_FromCentre;
	bRecycleSpawnedSystems = true;
}

void UTrailComponent::StartTrail()
{
	if(const auto StaticMeshComponent = ITrailInterface::Execute_GetStaticMesh(GetOwner()))
		TrailBegin(StaticMeshComponent);
}

void UTrailComponent::EndTrail() const 
{
	if(const auto StaticMeshComponent = ITrailInterface::Execute_GetStaticMesh(GetOwner()))
		TrailEnd(StaticMeshComponent);
}

UParticleSystemComponent* UTrailComponent::GetParticleSystemComponent(UMeshComponent* MeshComp) const
{
	if (MeshComp == nullptr)
		return nullptr;

	FParticleSystemComponentArray Children;
	GetCandidateSystems(*MeshComp, Children);
	for (UParticleSystemComponent* ParticleComp : Children)
	{
		if (ParticleComp->IsActive())
		{
			UParticleSystemComponent::TrailEmitterArray TrailEmitters;
			ParticleComp->GetOwnedTrailEmitters(TrailEmitters, this, false);
			if (TrailEmitters.Num() > 0)
				// We have a trail emitter, so return this one
				return ParticleComp;
		}
	}
	return nullptr;
}

void UTrailComponent::BeginPlay()
{
	Super::BeginPlay();
	
	if (!BaseTrail)
		BaseTrail = UGameConfigComponent::GetGameConfigComponent(this)->BaseTrail;
}

void UTrailComponent::TrailBegin(UMeshComponent* MeshComp)
{
	if (TrailOverride)
		PSTemplate = TrailOverride;
	else
		PSTemplate = BaseTrail;

	if (MeshComp->GetWorld()->GetNetMode() == NM_DedicatedServer)
		return;

	if (PSTemplate == nullptr)
		return;

	FParticleSystemComponentArray Children;
	GetCandidateSystems(*MeshComp, Children);

	constexpr float Width = 1.0f;

	UParticleSystemComponent* RecycleCandidates[3] = { nullptr, nullptr, nullptr }; // in order of priority
	bool bFoundExistingTrail = false;
	for (UParticleSystemComponent* ParticleComp : Children)
	{
		if (ParticleComp->IsActive())
		{
			UParticleSystemComponent::TrailEmitterArray TrailEmitters;
			ParticleComp->GetOwnedTrailEmitters(TrailEmitters, this, false);

			if (TrailEmitters.Num() > 0)
			{
				// This has active emitters, we'll just restart this one.
				bFoundExistingTrail = true;

				//If there are any trails, ensure the template hasn't been changed. Also destroy the component if there are errors.
				if (PSTemplate != ParticleComp->Template && ParticleComp->GetOuter() == MeshComp)
				{
					//The PSTemplate was changed so we need to destroy this system and create it again with the new template. May be able to just change the template?
					ParticleComp->DestroyComponent();
				}
				else
				{
					for (FParticleAnimTrailEmitterInstance* Trail : TrailEmitters)
					{
						Trail->BeginTrail();
						Trail->SetTrailSourceData(FirstSocketName, SecondSocketName, WidthScaleMode, Width);
					}
				}

				break;
			}
		}
		else if (ParticleComp->bAllowRecycling && !ParticleComp->IsActive())
		{
			// We prefer to recycle one with a matching template, and prefer one created by us.
			// 0: matching template, owned by mesh
			// 1: matching template, owned by actor
			// 2: non-matching template, owned by actor or mesh
			int32 RecycleIndex = 2;
			if (ParticleComp->Template == PSTemplate)
				RecycleIndex = ParticleComp->GetOuter() == MeshComp ? 0 : 1;
			
			RecycleCandidates[RecycleIndex] = ParticleComp;
		}
	}

	if (!bFoundExistingTrail)
	{
		// Spawn a new component from PSTemplate, or recycle an old one.
		UParticleSystemComponent* RecycleComponent = RecycleCandidates[0] ? RecycleCandidates[0] : RecycleCandidates[1] ? RecycleCandidates[1] : RecycleCandidates[2];
		UParticleSystemComponent* NewParticleComp = RecycleComponent ? RecycleComponent : NewObject<UParticleSystemComponent>(MeshComp);
		NewParticleComp->bAutoDestroy = RecycleComponent ? false : !bRecycleSpawnedSystems;
		NewParticleComp->bAllowRecycling = true;
		NewParticleComp->SecondsBeforeInactive = 0.0f;
		NewParticleComp->bAutoActivate = false;
		NewParticleComp->bOverrideLODMethod = false;
		NewParticleComp->GetRelativeScale3D() = FVector(1.f);
		NewParticleComp->bAutoManageAttachment = true; // Let it detach when finished (only happens if not auto-destroying)
		NewParticleComp->SetAutoAttachParams(MeshComp, NAME_None);

		// When recycling we can avoid setting the template if set already.
		if (NewParticleComp->Template != PSTemplate)
			NewParticleComp->SetTemplate(PSTemplate);

		// Recycled components are usually already registered
		if (!NewParticleComp->IsRegistered())
			NewParticleComp->RegisterComponentWithWorld(MeshComp->GetWorld());

		NewParticleComp->AttachToComponent(MeshComp, FAttachmentTransformRules::KeepRelativeTransform);
		NewParticleComp->ActivateSystem(true);

		UParticleSystemComponent::TrailEmitterArray TrailEmitters;
		NewParticleComp->GetOwnedTrailEmitters(TrailEmitters, this, true);

		for (FParticleAnimTrailEmitterInstance* Trail : TrailEmitters)
		{
			Trail->BeginTrail();
			Trail->SetTrailSourceData(FirstSocketName, SecondSocketName, WidthScaleMode, Width);
		}
	}
}

void UTrailComponent::TrailEnd(const UMeshComponent* MeshComp) const 
{
	if (MeshComp->GetWorld()->GetNetMode() == NM_DedicatedServer)
		return;

	FParticleSystemComponentArray Children;
	GetCandidateSystems(*MeshComp, Children);

	for (UParticleSystemComponent* ParticleComp : Children)
	{
		if (ParticleComp->IsActive())
		{
			UParticleSystemComponent::TrailEmitterArray TrailEmitters;
			ParticleComp->GetOwnedTrailEmitters(TrailEmitters, this, false);
			for (FParticleAnimTrailEmitterInstance* Trail : TrailEmitters)
				Trail->EndTrail();
		}
	}
}

