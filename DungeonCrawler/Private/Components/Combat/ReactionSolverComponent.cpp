// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/Combat/ReactionSolverComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "GameplayTagContainer.h"
#include "MyGameMode.h"
#include "Structs.h"
#include "Enums.h"
#include "PoiseComponent.h"

UReactionSolverComponent* UReactionSolverComponent::GetReactionSolver(const UObject* WorldContextObject)
{
	return AMyGameMode::GetMyGameMode(WorldContextObject)->ReactionSolver;
}

FGameplayTag UReactionSolverComponent::GetGameplayTagForAttackStaggerStrength(const EAttackStaggerStrength AttackStaggerStrength)
{
	if (const auto Tag = AttackStrengthToTagsMap.Find(AttackStaggerStrength))
			return *Tag;
	
	return FGameplayTag();
}

void UReactionSolverComponent::SendStaggerGameplayEvent(AActor* Target, const EAttackStaggerStrength AttackStaggerStrength)
{
	if(const auto PoiseComponent = Cast<UPoiseComponent>(Target->GetComponentByClass(UPoiseComponent::StaticClass())))
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(Target, GetReactionTag(AttackStaggerStrength, PoiseComponent->PoiseLevel), FGameplayEventData());
}

EAttackStaggerStrength UReactionSolverComponent::GetAttackStaggerStrengthForGameplayTag(const FGameplayTag Tag) const
{
	if (const auto AttackStaggerStrength = AttackStrengthToTagsMap.FindKey(Tag))
		return *AttackStaggerStrength;
	
	return EAttackStaggerStrength::None;
}

FGameplayTag UReactionSolverComponent::GetReactionTag(const EAttackStaggerStrength AttackStrength, const EPoiseLevel PoiseLevel)
{
	const EStaggerReaction StaggerReaction = DecideStaggerReaction(AttackStrength, PoiseLevel);
	switch (StaggerReaction)
	{
		case EStaggerReaction::None:
			return NoReaction;

		case EStaggerReaction::Hit:
			return HitReaction;

		case EStaggerReaction::HeavyHit:
			return StrongHitReaction;

		case EStaggerReaction::Stun:
			return StunReaction;

		case EStaggerReaction::Knockdown:
			return KnockDownReaction;

		default:
			return NoReaction;
	}
}

EStaggerReaction UReactionSolverComponent::DecideStaggerReaction(const EAttackStaggerStrength AttackStrength, const EPoiseLevel PoiseLevel)
{
	if (const auto Setup = ReactionsSetup.PoiseLevelToStaggerSetupMap.Find(PoiseLevel))
		if (const auto Reaction = Setup->AttackToReactionMap.Find(AttackStrength))
			return *Reaction;
	
	return EStaggerReaction::None;
}
