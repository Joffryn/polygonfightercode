// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/Combat/DamageResponseComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"
#include "BaseCharacter.h"
#include "CombatComponent.h"
#include "CritSolver.h"
#include "GameConfigComponent.h"
#include "GameplayAbilityTypes.h"
#include "MyAttributeSet.h"
#include "MyDamageType.h"
#include "MyGameplayStatics.h"
#include "MyHUD.h"
#include "MyPlayerController.h"
#include "ReactionSolverComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Perception/AISense_Damage.h"

UDamageResponseComponent::UDamageResponseComponent()
{
}

bool UDamageResponseComponent::CanTakeDamage() const
{
	if(OwnerAbilitySystemComponent)
		return !OwnerAbilitySystemComponent->HasMatchingGameplayTag(InvincibilityTag);
	
	return true;
}

bool UDamageResponseComponent::ShouldParry(const TSubclassOf<UMyDamageType> DamageType, const FVector AttackLocation) const
{
	//Isn't in parrying state
	if(!OwnerAbilitySystemComponent->HasMatchingGameplayTag(ParryingTag))
		return false;

	//Unparryable attack
	if(Cast<UMyDamageType>(DamageType->GetDefaultObject())->DamageTags.HasTag(UnblockableTag))
		return false;

	//Angle to great
	if(GetYawAngleBetweenOwnerAndLocation(AttackLocation) > PreventingAngle)
		return false;

	return true;
}

bool UDamageResponseComponent::ShouldBlock(const TSubclassOf<UMyDamageType> DamageType, const FVector AttackLocation) const
{
	//Isn't in blocking state
	if(!OwnerAbilitySystemComponent->HasMatchingGameplayTag(BlockingTag))
		return false;

	//Unparryable attack
	if(Cast<UMyDamageType>(DamageType->GetDefaultObject())->DamageTags.HasTag(UnblockableTag))
		return false;

	//Angle to great
	if(GetYawAngleBetweenOwnerAndLocation(AttackLocation) > PreventingAngle)
		return false;

	//Ranged attack can only be blocked if specific tag is applied
	if(Cast<UMyDamageType>(DamageType->GetDefaultObject())->DamageTags.HasTag(RangedDamageTag))
	{
		if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(CanBlockProjectilesTag))
			return true;
		return false;
	}

	return true;
}

void UDamageResponseComponent::OnOwnerAnyDamageTaken(AActor* Actor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if(CanTakeDamage())
	{
		if(const auto MyDamageType = Cast<UMyDamageType>(DamageType))
		{
			if(MyDamageType->HitStaggerStrength != EAttackStaggerStrength::None)
			{
				const FGameplayEventData Payload;
				UReactionSolverComponent::GetReactionSolver(this)->SendStaggerGameplayEvent(GetOwner(), MyDamageType->HitStaggerStrength);
				UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(GetOwner(), MyDamageType->HitTag, Payload);
			}
		}

		bool bIsCrit = UCritSolver::GetCritSolver(this)->IsAttackCrit(DamageCauser, Actor);
		
		if(bIsCrit)
			if(DamageCauser)
				if(const auto CauserAbilitySystemComponent = Cast<UAbilitySystemComponent>(DamageCauser->GetComponentByClass(UAbilitySystemComponent::StaticClass())))
					Damage *= CauserAbilitySystemComponent->GetNumericAttribute(UMyAttributeSet::GetCritDamageMultiplierAttribute());

		if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(HealthyTag))
		{
			if(DamageCauser)
				if(const auto CauserAbilitySystemComponent = Cast<UAbilitySystemComponent>(DamageCauser->GetComponentByClass(UAbilitySystemComponent::StaticClass())))
					if(CauserAbilitySystemComponent->HasMatchingGameplayTag(HealthyMeleeAttackExtraDamageTag))
						//Consider using an attribute
						Damage *= 2.0f;

			if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(HealthyDamageReductionTag))
				Damage /= 2.0f;
		}

		//Global damage multipliers
		if(auto GameConfigComponent = UGameConfigComponent::GetGameConfigComponent(this))
		{
			if(BaseCharacterOwner)
			{
				if(BaseCharacterOwner->GetController() == AMyPlayerController::GetMyPlayerController(this))
					Damage *= GameConfigComponent->DamageMultiplierPlayer;
				else
					Damage *= GameConfigComponent->DamageMultiplierAI;
			}
			else
				Damage *= GameConfigComponent->DamageMultiplierAI;
		}
		
		const FGameplayEffectContextHandle EffectContext;
		if(BaseCharacterOwner)
		{
			if(BaseCharacterOwner->GetTeamAttitudeTowardsActor(DamageCauser) == ETeamAttitude::Type::Hostile)
			{
				 UAISense_Damage::ReportDamageEvent(this, BaseCharacterOwner, DamageCauser,  Damage, DamageCauser->GetActorLocation(), BaseCharacterOwner->GetActorLocation());
			}
			if(BaseCharacterOwner->IsAlive())
				OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(LoseHealthEffect,  Damage, EffectContext);
		}
		else
			OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(LoseHealthEffect,  Damage, EffectContext);

		const bool bBaseCharacterDead = BaseCharacterOwner && !BaseCharacterOwner->IsAlive();
		if(DamageCauser)
		{
			if(const auto CauserDamageResponseComponent = Cast<UDamageResponseComponent>(DamageCauser->GetComponentByClass(UDamageResponseComponent::StaticClass())))
			{
				if(const auto MyDamageType = Cast<UMyDamageType>(DamageType))
					if(MyDamageType->DamageTags.HasTag(MeleeDamageTag))
						if(bIsCrit)
						{
							CauserDamageResponseComponent->OnCritDamageDealed.Broadcast(DamageType->GetClass(), Damage);
							OnBeingCrit.Broadcast();
						}
						else
						{
							CauserDamageResponseComponent->OnHit.Broadcast();
							OnBeingHit.Broadcast();
						}
					
				CauserDamageResponseComponent->OnDealedDamage.Broadcast(DamageType->GetClass() ,Damage);
				if(bBaseCharacterDead & !bHasBroadcastedOwnerDeath)
					CauserDamageResponseComponent->OnKill.Broadcast(BaseCharacterOwner);
			}
		}
		
		if(bBaseCharacterDead & !bHasBroadcastedOwnerDeath)
		{
			const FGameplayEventData Payload;
			UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(GetOwner(), DeathReactionTag, Payload);
			OnDeath.Broadcast();
			bHasBroadcastedOwnerDeath = true;
			ShowDamageInfo(Actor, Damage, DamageType, bIsCrit);
		}

		//Don't show damage numbers on when owner is dead
		if(BaseCharacterOwner)
			if(!BaseCharacterOwner->IsAlive())
				return;

		ShowDamageInfo(Actor, Damage, DamageType, bIsCrit);
	}
}

float UDamageResponseComponent::OnOwnerPointDamageTaken(const float Damage, const FVector HitLocation, const FVector HitFromDirection, const TSubclassOf<UDamageType> DamageTypeClass, AActor* DamageCauser)
{
	const auto MyDamageTypeClass = Cast<UMyDamageType>(DamageTypeClass->GetDefaultObject())->GetClass();

	//In the case of melee attacks use causer location instead of hit one, using hit one sometimes pierce blocks when attacked by bigger enemies which weapons can hit entire character and not just front
	FVector AttackLocation = HitLocation;
	if (Cast<UMyDamageType>(MyDamageTypeClass->GetDefaultObject())->DamageTags.HasTag(MeleeDamageTag))
		AttackLocation = DamageCauser->GetActorLocation();

	if(ShouldParry(MyDamageTypeClass, AttackLocation))
	{
		Parry(MyDamageTypeClass, DamageCauser ,HitLocation, Damage);
		return 0.0f;
	}

	if(ShouldBlock(MyDamageTypeClass, AttackLocation))
	{
		Block(Damage, MyDamageTypeClass, DamageCauser ,HitLocation);
		return 0.0f;
	}

	if(CanTakeDamage())
	{
		FGameplayCueParameters GameplayCueParameters;
		GameplayCueParameters.Location = HitLocation;
		GameplayCueParameters. Normal = HitFromDirection;
		GameplayCueParameters.EffectCauser = GetOwner();
		GameplayCueParameters.Instigator = GetOwner();
		GameplayCueParameters.SourceObject = GetOwner();
	
		UMyGameplayStatics::ExecuteGameplayCue(Cast<UAbilitySystemComponent>(GetOwner()->GetComponentByClass(UAbilitySystemComponent::StaticClass())), PointHitGameplayCueTag, GameplayCueParameters);
		return Damage;
	}
	
	InvincibleHit(MyDamageTypeClass, DamageCauser, HitLocation);
	return 0.0f;
}

float UDamageResponseComponent::OnOwnerRadialDamageTaken(const float Damage, const FVector Origin, const TSubclassOf<UDamageType> DamageTypeClass, AActor* DamageCauser)
{
	const auto MyDamageTypeClass = Cast<UMyDamageType>(DamageTypeClass->GetDefaultObject())->GetClass();
	if(ShouldBlock(MyDamageTypeClass, Origin))
	{
		Block(Damage, MyDamageTypeClass, DamageCauser ,Origin);
		return 0.0f;
	}

	if(CanTakeDamage())
	{
		FGameplayCueParameters GameplayCueParameters;
		GameplayCueParameters.Location = GetOwner()->GetActorLocation();
		GameplayCueParameters.EffectCauser = GetOwner();
		GameplayCueParameters.Instigator = GetOwner();
		GameplayCueParameters.SourceObject = GetOwner();
	
		UMyGameplayStatics::ExecuteGameplayCue(Cast<UAbilitySystemComponent>(GetOwner()->GetComponentByClass(UAbilitySystemComponent::StaticClass())), PointHitGameplayCueTag, GameplayCueParameters);
		return Damage;
	}
	
	InvincibleHit(MyDamageTypeClass, DamageCauser, GetOwner()->GetActorLocation());
	return 0.0f;
}

void UDamageResponseComponent::BeginPlay()
{
	Super::BeginPlay();

	BaseCharacterOwner = Cast<ABaseCharacter>(GetOwner());
	OwnerAbilitySystemComponent = Cast<UAbilitySystemComponent>(GetOwner()->GetComponentByClass(UAbilitySystemComponent::StaticClass()));
	
	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UDamageResponseComponent::OnOwnerAnyDamageTaken);
}

void UDamageResponseComponent::ShowDamageInfo(const AActor* Actor, const float Damage, const UDamageType* DamageType, const bool bIsCrit)
{
	if(const auto  GameConfig = UGameConfigComponent::GetGameConfigComponent(this))
		if(!GameConfig->bShowDamageNumbers)
			return;
	
	if(const auto MyHud = AMyHUD::GetMyHud(this))
	{
		FLinearColor Color = FLinearColor::White;
		if(const auto MyDamageType = Cast<UMyDamageType>(DamageType))
			if(MyDamageType->bCanShowNumbers)
				Color = MyDamageType->UIColor;
			else
				return;
		
		MyHud->ShowDamageInfo(Actor->GetActorLocation(), FText::FromString(FString::FromInt(static_cast<int32>(Damage))), Color, bIsCrit);
	}
}

void UDamageResponseComponent::InvincibleHit(const TSubclassOf<UMyDamageType> DamageType, AActor* DamageCauser, const FVector HitLocation) const
{
	FGameplayCueParameters GameplayCueParameters;
	GameplayCueParameters.Location = HitLocation;
	GameplayCueParameters.EffectCauser = DamageCauser;
	GameplayCueParameters.Instigator = DamageCauser;
	GameplayCueParameters.SourceObject = DamageCauser;
	
	UMyGameplayStatics::ExecuteGameplayCue(Cast<UAbilitySystemComponent>(GetOwner()->GetComponentByClass(UAbilitySystemComponent::StaticClass())), InvincibleHitGameplayCueTag, GameplayCueParameters);
	
	if(Cast<UMyDamageType>(DamageType->GetDefaultObject())->DamageTags.HasTag(MeleeDamageTag))
		if(DamageCauser)
			if(const auto CauserDamageResponseComponent = Cast<UDamageResponseComponent>(DamageCauser->GetComponentByClass(UDamageResponseComponent::StaticClass())))
				CauserDamageResponseComponent->OnInvincibleHit.Broadcast();
	
}

void UDamageResponseComponent::Parry(const TSubclassOf<UMyDamageType> DamageType, AActor* DamageCauser, const FVector HitLocation, const float Damage) const
{
	if(Cast<UMyDamageType>(DamageType->GetDefaultObject())->DamageTags.HasTag(MeleeDamageTag))
	{
		const FGameplayEventData Payload;
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(DamageCauser, ParriedReactionTag, Payload);
		if(DamageCauser)
			if(const auto CauserDamageResponseComponent = Cast<UDamageResponseComponent>(DamageCauser->GetComponentByClass(UDamageResponseComponent::StaticClass())))
				CauserDamageResponseComponent->OnBeingParried.Broadcast();
	}
		
	FGameplayCueParameters GameplayCueParameters;
	GameplayCueParameters.Location = HitLocation;
	GameplayCueParameters.EffectCauser = DamageCauser;
	GameplayCueParameters.Instigator = DamageCauser;
	GameplayCueParameters.SourceObject = DamageCauser;
	
	UMyGameplayStatics::ExecuteGameplayCue(Cast<UAbilitySystemComponent>(GetOwner()->GetComponentByClass(UAbilitySystemComponent::StaticClass())), ParryGameplayCueTag, GameplayCueParameters);
	OnParry.Broadcast(DamageCauser, DamageType, Damage);
}

void UDamageResponseComponent::Block(float Damage, TSubclassOf<UMyDamageType> DamageType, AActor* DamageCauser, FVector HitLocation) const
{
	if(Cast<UMyDamageType>(DamageType->GetDefaultObject())->DamageTags.HasTag(MeleeDamageTag))
	{
		const FGameplayEventData Payload;
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(DamageCauser, BlockedReactionTag, Payload);
		if(DamageCauser)
			if(const auto CauserDamageResponseComponent = Cast<UDamageResponseComponent>(DamageCauser->GetComponentByClass(UDamageResponseComponent::StaticClass())))
				CauserDamageResponseComponent->OnBeingBlocked.Broadcast();
	}
	
	FGameplayCueParameters GameplayCueParameters;
	GameplayCueParameters.Location = HitLocation;
	GameplayCueParameters.EffectCauser = DamageCauser;
	GameplayCueParameters.Instigator = DamageCauser;
	GameplayCueParameters.SourceObject = DamageCauser;

	const FGameplayEventData Payload;
	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(GetOwner(), DamageHitTag, Payload);

	FGameplayEffectContextHandle EffectContext;
	OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(LoseStaminaEffect, OwnerAbilitySystemComponent->GetNumericAttribute(BlockingStaminaCostAttribute) * Damage, EffectContext);
	
	UMyGameplayStatics::ExecuteGameplayCue(Cast<UAbilitySystemComponent>(GetOwner()->GetComponentByClass(UAbilitySystemComponent::StaticClass())), BlockGameplayCueTag, GameplayCueParameters);
	OnBlock.Broadcast();
}

float UDamageResponseComponent::GetYawAngleBetweenOwnerAndLocation(const FVector Location) const
{
	const auto Owner = GetOwner();
	const auto LookAtRotation = UKismetMathLibrary::FindLookAtRotation(Owner->GetActorLocation(), Location);
	const auto OwnerRotation = Owner->GetActorRotation();
	return (FVector(OwnerRotation.Roll, OwnerRotation.Pitch, OwnerRotation.Yaw) - FVector(LookAtRotation.Roll, LookAtRotation.Pitch, LookAtRotation.Yaw)).Size();
}
