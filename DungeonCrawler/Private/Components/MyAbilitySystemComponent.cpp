// Fill out your copyright notice in the Description page of Project Settings.

#include "MyAbilitySystemComponent.h"

#include "BaseCharacter.h"
#include "BlueprintGameplayTagLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Abilities/GameplayAbility.h"
#include "GameFramework/Character.h"
#include "MyGameplayAbility.h"
#include "Kismet/GameplayStatics.h"

UMyAbilitySystemComponent* UMyAbilitySystemComponent::GetPlayerAbilitySystemComponent(const UObject* WorldContextObject)
{
	if(const auto MyPlayerCharacter = ABaseCharacter::GetMyPlayerCharacter(WorldContextObject))
		return MyPlayerCharacter->AbilitySystemComponent;

	return nullptr;
}

bool UMyAbilitySystemComponent::HandleTagChange(const FGameplayTag TagToPotentiallyAdd, const FGameplayTagContainer TagsToRemove, const FGameplayTagContainer BlockingTags)
{
	if(HasAnyMatchingGameplayTags(BlockingTags))
	{
		RemoveGameplayTag(UBlueprintGameplayTagLibrary::MakeGameplayTagContainerFromTag(TagToPotentiallyAdd));
		RemoveGameplayTag(TagsToRemove);
		return true;
	}
	if(!HasMatchingGameplayTag(TagToPotentiallyAdd))
	{
		AddGameplayTag(UBlueprintGameplayTagLibrary::MakeGameplayTagContainerFromTag(TagToPotentiallyAdd));
		RemoveGameplayTag(TagsToRemove);
		return true;
	}
	return false;
}

bool UMyAbilitySystemComponent::TryActivateAbilitiesByTagWithEventData(const FGameplayTagContainer& GameplayTagContainer, FGameplayEventData& EventData, bool bAllowRemoteActivation )
{
	TArray<FGameplayAbilitySpec*> AbilitiesToActivate;
	GetActivatableGameplayAbilitySpecsByAllMatchingTags(GameplayTagContainer, AbilitiesToActivate);

	bool bSuccess = false;

	for (const auto GameplayAbilitySpec : AbilitiesToActivate)
		bSuccess |= TryActivateAbilityEventData(GameplayAbilitySpec->Handle, &EventData, true, bAllowRemoteActivation);

	return bSuccess;
}

bool UMyAbilitySystemComponent::BP_TryActivateAbilitiesByTagWithEventData(const FGameplayTagContainer& GameplayTagContainer, FGameplayEventData EventData, const bool bAllowRemoteActivation )
{
	return TryActivateAbilitiesByTagWithEventData(GameplayTagContainer, EventData, bAllowRemoteActivation);
}

bool UMyAbilitySystemComponent::TryActivateAbilityEventData(const FGameplayAbilitySpecHandle& AbilityToActivate, const FGameplayEventData* EventData, const bool bPressed, const bool bAllowRemoteActivation )
{
	FGameplayTagContainer FailureTags;
	FGameplayAbilitySpec* Spec = FindAbilitySpecFromHandle(AbilityToActivate);
	if (!Spec)
	{
		ABILITY_LOG(Warning, TEXT("TryActivateAbility called with invalid Handle"));
		return false;
	}

	UGameplayAbility* Ability = Spec->Ability;

	if (!Ability)
	{
		ABILITY_LOG(Warning, TEXT("TryActivateAbility called with invalid Ability"));
		return false;
	}

	const FGameplayAbilityActorInfo* ActorInfo = AbilityActorInfo.Get();
	
	if (ActorInfo == nullptr || !ActorInfo->OwnerActor.IsValid() || !ActorInfo->AvatarActor.IsValid())
		return false;

	const ENetRole NetMode = ActorInfo->AvatarActor->GetLocalRole();
	if (Spec->IsActive()) 
	{
		if (bPressed)
		{
			if (GenericLocalConfirmCallbacks.IsBound())
			{
				LocalInputConfirm();
				return false;
			}
			if (Spec->Ability->bReplicateInputDirectly && IsOwnerActorAuthoritative() == false)
				ServerSetInputPressed(Spec->Handle);

			AbilitySpecInputPressed(*Spec);

			InvokeReplicatedEvent(EAbilityGenericReplicatedEvent::InputPressed, Spec->Handle, Spec->ActivationInfo.GetActivationPredictionKey());
		}
		else
		{
			if (Spec->Ability->bReplicateInputDirectly && IsOwnerActorAuthoritative() == false)
				ServerSetInputReleased(Spec->Handle);

			AbilitySpecInputReleased(*Spec);

			InvokeReplicatedEvent(EAbilityGenericReplicatedEvent::InputReleased, Spec->Handle, Spec->ActivationInfo.GetActivationPredictionKey());
		}
	}

	const bool bIsLocal = AbilityActorInfo->IsLocallyControlled();
	
	if (!bIsLocal && (Ability->GetNetExecutionPolicy() == EGameplayAbilityNetExecutionPolicy::LocalOnly || Ability->GetNetExecutionPolicy() == EGameplayAbilityNetExecutionPolicy::LocalPredicted))
	{
		if (bAllowRemoteActivation)
		{
			ClientTryActivateAbility(AbilityToActivate);
			return true;
		}

		ABILITY_LOG(Log, TEXT("Can't activate LocalOnly or LocalPredicted ability %s when not local."), *Ability->GetName());
		return false;
	}

	//Flush any remaining server moves before activating the ability.
	//	Flushing the server moves prevents situations where previously pending move's DeltaTimes are figured into montages that are about to play and update.
	//	When this happened, clients would have a smaller delta time than the server which meant the server would get ahead and receive their notifies before the client, etc.
	//	The system depends on the server not getting ahead, so it's important to send along any previously pending server moves here.
	if (ActorInfo && ActorInfo->AvatarActor.Get() && !ActorInfo->IsNetAuthority())
	{
		AActor* MyActor = ActorInfo->AvatarActor.Get();

		if (MyActor)
		{
			ACharacter* MyCharacter = Cast<ACharacter>(MyActor);
			if (MyCharacter)
			{
				UCharacterMovementComponent* CharMoveComp = Cast<UCharacterMovementComponent>(MyCharacter->GetMovementComponent());

				if (CharMoveComp)
					CharMoveComp->FlushServerMoves();
			}
		}
	}

	if (NetMode != ROLE_Authority && (Ability->GetNetExecutionPolicy() == EGameplayAbilityNetExecutionPolicy::ServerOnly || Ability->GetNetExecutionPolicy() == EGameplayAbilityNetExecutionPolicy::ServerInitiated))
	{
		if (bAllowRemoteActivation)
		{
			if (Ability->CanActivateAbility(AbilityToActivate, ActorInfo, nullptr, nullptr, &FailureTags))
			{
				// No prediction key, server will assign a server-generated key
				CallServerTryActivateAbility(AbilityToActivate, Spec->InputPressed, FPredictionKey());
				return true;
			}
			NotifyAbilityFailed(AbilityToActivate, Ability, FailureTags);
			return false;
		}

		ABILITY_LOG(Log, TEXT("Can't activate ServerOnly or ServerInitiated ability %s when not the server."), *Ability->GetName());
		return false;
	}

	bool bSuccess = false;

	//Do not try activate ability on released event.
	if (bPressed)
		bSuccess = InternalTryActivateAbility(AbilityToActivate, FPredictionKey(), nullptr, nullptr, EventData);

	if (bSuccess)
	{
		if (Spec->Ability->bReplicateInputDirectly && IsOwnerActorAuthoritative() == false)
			ServerSetInputPressed(Spec->Handle);

		AbilitySpecInputPressed(*Spec);

		// Invoke the InputPressed event. This is not replicated here. If someone is listening, they may replicate the InputPressed event to the server.
		InvokeReplicatedEvent(EAbilityGenericReplicatedEvent::InputPressed, Spec->Handle, Spec->ActivationInfo.GetActivationPredictionKey());
	}
	return bSuccess;
}

int32 UMyAbilitySystemComponent::HandleGameplayEvent(const FGameplayTag EventTag, const FGameplayEventData* Payload)
{
	const int32 Result = Super::HandleGameplayEvent(EventTag, Payload);
	OnGameplayEvent.Broadcast(EventTag, Payload != nullptr ? *Payload : FGameplayEventData());
	return Result;
}

void UMyAbilitySystemComponent::SetUserAbilityActivationInhibited(const bool NewInhibit)
{
	Super::SetUserAbilityActivationInhibited(NewInhibit);
	if (!NewInhibit)
		HandleAbilityQueue();
}

void UMyAbilitySystemComponent::BeginPlay()
{
	Super::BeginPlay();
	AddGameplayTag(TagsToApplyAtBeginPlay);
	ApplyStartingGameplayEffects();
	BindDelegates();
}

void UMyAbilitySystemComponent::AddGameplayTag(const FGameplayTagContainer TagsToAdd)
{
	AddLooseGameplayTags(TagsToAdd);
}

void UMyAbilitySystemComponent::RemoveGameplayTag(const FGameplayTagContainer TagsToRemove)
{
	RemoveLooseGameplayTags(TagsToRemove);
}

bool UMyAbilitySystemComponent::QueueAbility(const FGameplayAbilitySpecHandle AbilityToQueue)
{
	const bool ActivatedImmediately = TryActivateAbility(AbilityToQueue);
	if (!ActivatedImmediately)
		return InternalTryQueueAbility(AbilityToQueue);

	return ActivatedImmediately;
}

bool UMyAbilitySystemComponent::QueueAbilityWithEventData(const FGameplayAbilitySpecHandle AbilityToQueue, FGameplayEventData EventData)
{
	const bool ActivatedImmediately = TriggerAbilityFromGameplayEvent(AbilityToQueue, AbilityActorInfo.Get(), EventData.EventTag, &EventData, *this);
	if (!ActivatedImmediately)
		return InternalTryQueueAbility(AbilityToQueue, &EventData);

	return ActivatedImmediately;
}

bool UMyAbilitySystemComponent::QueueAbilityByClass(const TSubclassOf<UGameplayAbility> AbilityClass)
{
	bool bSuccess = false;
	const UGameplayAbility* const InAbilityCDO = AbilityClass.GetDefaultObject();
	for (const FGameplayAbilitySpec& Spec : ActivatableAbilities.Items)
	{
		if (Spec.Ability == InAbilityCDO)
		{
			bSuccess |= QueueAbility(Spec.Handle);
			break;
		}
	}
	return bSuccess;
}

bool UMyAbilitySystemComponent::QueueAbilityByTag(const FGameplayTagContainer AbilityTags)
{
	TArray<FGameplayAbilitySpec*> AbilitiesToActivate;
	GetActivatableGameplayAbilitySpecsByAllMatchingTags(AbilityTags, AbilitiesToActivate, false);
	bool bSuccess = false;

	for (const auto GameplayAbilitySpec : AbilitiesToActivate)
		bSuccess |= QueueAbility(GameplayAbilitySpec->Handle);
	
	return bSuccess;
}

bool UMyAbilitySystemComponent::QueueAbilityByEvent(const FGameplayTag EventTag, FGameplayEventData EventData)
{
	EventData.EventTag = EventTag;

	bool bSuccess = false;

	FGameplayTag CurrentTag = EventTag;
	while (CurrentTag.IsValid())
	{
		if (GameplayEventTriggeredAbilities.Contains(CurrentTag))
			for (const FGameplayAbilitySpecHandle& AbilityHandle : GameplayEventTriggeredAbilities[CurrentTag])
				bSuccess |= QueueAbilityWithEventData(AbilityHandle, EventData);

		CurrentTag = CurrentTag.RequestDirectParent();
	}

	return bSuccess;
}

void UMyAbilitySystemComponent::ClearQueue()
{
	QueuedAbilityHandle = FGameplayAbilitySpecHandle();
	QueuedAbilitiesEventData = FGameplayEventData();
	OnAbilityRemovedFromQueue.Broadcast();
}

bool UMyAbilitySystemComponent::HasQueuedAbility() const
{
	return QueuedAbilityHandle.IsValid();
}

UGameplayAbility* UMyAbilitySystemComponent::GetAbilityInstance(const FGameplayAbilitySpecHandle Handle)
{
	if (const FGameplayAbilitySpec* AbilitySpec = FindAbilitySpecFromHandle(Handle))
		return AbilitySpec->Ability;
	
	return nullptr;
}

void UMyAbilitySystemComponent::BroadcastAttributeChange(const FOnAttributeChangeData& AttributeChangeData)
{
	OnAttributeValueChange.Broadcast(AttributeChangeData.Attribute, AttributeChangeData.NewValue, AttributeChangeData.OldValue);
}

void UMyAbilitySystemComponent::BindDelegates()
{
	TArray<FGameplayAttribute> Attributes;
	GetAllAttributes(Attributes);
	for (const auto Attribute : Attributes)
		GetGameplayAttributeValueChangeDelegate(Attribute).AddUObject(this, &UMyAbilitySystemComponent::BroadcastAttributeChange);
}

 UGameplayAbility* UMyAbilitySystemComponent::GetQueuedAbility()
{
	if (const FGameplayAbilitySpec* AbilitySpec = FindAbilitySpecFromHandle(QueuedAbilityHandle))
		return AbilitySpec->Ability;
	
	return nullptr;
}

bool UMyAbilitySystemComponent::InternalTryQueueAbility(const FGameplayAbilitySpecHandle AbilityToQueue, FGameplayEventData* EventData /*= nullptr*/)
{
	bool bCanBeQueued = false;
	FGameplayAbilitySpec* AbilityToQueueSpec = FindAbilitySpecFromHandle(AbilityToQueue);
	if (AbilityToQueueSpec)
	{
		UGameplayAbility* AbilityInstance = AbilityToQueueSpec->GetPrimaryInstance();
		if (AbilityInstance)
			AbilityInstance = AbilityToQueueSpec->Ability;

		if (const UMyGameplayAbility* QueueableAbilityInstance = Cast< UMyGameplayAbility >(AbilityInstance))
			bCanBeQueued = QueueableAbilityInstance->CanQueueAbility(AbilityToQueue, AbilityActorInfo.Get(), this);
		else
			bCanBeQueued = true;
	}

	if (bCanBeQueued)
	{
		QueuedAbilityHandle = AbilityToQueue;
		if (EventData)
			QueuedAbilitiesEventData  = *EventData;
		
		OnAbilityQueued.Broadcast();
		return true;
	}
	return false;
}

void UMyAbilitySystemComponent::HandleAbilityQueue()
{
	static bool bIsHandlingQueue = false;
	//Cannot be called recursively
	if (!bIsHandlingQueue && HasQueuedAbility())
	{			
		bIsHandlingQueue = true;
		bool bHasActivated;
		if (QueuedAbilitiesEventData.EventTag.IsValid())
			bHasActivated = TriggerAbilityFromGameplayEvent(QueuedAbilityHandle, AbilityActorInfo.Get(), QueuedAbilitiesEventData.EventTag, &QueuedAbilitiesEventData, *this);
		else
			bHasActivated = TryActivateAbility(QueuedAbilityHandle);
		
		if (bHasActivated)
		{
			OnAbilityActivatedFromQueue.Broadcast();
			ClearQueue();
		}
		bIsHandlingQueue = false;
	}
}

void UMyAbilitySystemComponent::NotifyAbilityActivated(const FGameplayAbilitySpecHandle Handle, UGameplayAbility* Ability)
{
	Super::NotifyAbilityActivated(Handle, Ability);
	OnAbilityActivated.Broadcast(Ability);
}

void UMyAbilitySystemComponent::NotifyAbilityFailed(const FGameplayAbilitySpecHandle Handle, UGameplayAbility* Ability, const FGameplayTagContainer& FailureReason)
{
	Super::NotifyAbilityFailed(Handle, Ability, FailureReason);
	OnAbilityFailed.Broadcast(Ability, FailureReason);
}

void UMyAbilitySystemComponent::NotifyAbilityEnded(const FGameplayAbilitySpecHandle Handle, UGameplayAbility* Ability, const bool bWasCancelled)
{
	Super::NotifyAbilityEnded(Handle, Ability, bWasCancelled);
	OnAbilityEnded.Broadcast(Ability, bWasCancelled);
	HandleAbilityQueue();
}

void UMyAbilitySystemComponent::NotifyAbilityCommit(UGameplayAbility* Ability)
{
	Super::NotifyAbilityCommit(Ability);
	OnAbilityCommited.Broadcast(Ability);
}

void UMyAbilitySystemComponent::OnTagUpdated(const FGameplayTag& Tag, const bool TagExists)
{
	Super::OnTagUpdated(Tag, TagExists);
	if (!TagExists)
	{
		OnGameplayTagRemoved.Broadcast(Tag);
		HandleAbilityQueue();
	}
	else if (GetTagCount(Tag) == 1)
	{
		OnGameplayTagAdded.Broadcast(Tag);
		HandleAbilityQueue();
	}
}

void UMyAbilitySystemComponent::ApplyStartingGameplayEffects()
{
	const FGameplayEffectContextHandle EffectContext;
	for(const auto GameplayEffectClass : EffectsToApplyAtBeginPlay)
		BP_ApplyGameplayEffectToSelf(GameplayEffectClass, 0, EffectContext);
}
