// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/SaveComponent.h"
#include "Kismet/GameplayStatics.h"
#include "AttributeSet.h"
#include "BaseCharacter.h"
#include "CombatComponent.h"
#include "MyAbilitySystemComponent.h"
#include "MyGameState.h"
#include "MySaveGame.h"
#include "PlayerCastingComponent.h"
#include "SoundsComponent.h"

USaveComponent::USaveComponent()
{
	
}

USaveComponent* USaveComponent::GetSaveComponent(const UObject* WorldContextObject)
{
	return AMyGameState::GetMyGameState(WorldContextObject)->SaveComponent;
}

void USaveComponent::SaveGame(const FName NextLevelName)
{
	Save = Cast<UMySaveGame>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));
	if(Save)
	{
		PlayerCharacter = ABaseCharacter::GetMyPlayerCharacter(this);
		Save->LevelName = NextLevelName;
		SaveLook();
		SaveWeapons();
		SaveAttributes();
		SaveUpgradeEffects();
		SaveCasting();
		SaveSounds();
		
		UGameplayStatics::SaveGameToSlot(Save, TEXT("Save"), 0);
	}
}

void USaveComponent::LoadGame()
{
	if(!UGameplayStatics::DoesSaveGameExist(TEXT("Save"), 0))
		return;
	
	Save = Cast<UMySaveGame>(UGameplayStatics::LoadGameFromSlot(TEXT("Save"), 0));
	if(Save)
	{
		PlayerCharacter = ABaseCharacter::GetMyPlayerCharacter(this);
		LoadLook();
		LoadWeapons();
		LoadAttributes();
		LoadUpgradeEffects();
		LoadCasting();
		LoadSounds();
		
		UGameplayStatics::SaveGameToSlot(Save, TEXT("Save"), 0);
	}
}

bool USaveComponent::DoesSaveExist()
{
	return UGameplayStatics::DoesSaveGameExist(TEXT("Save"), 0);
}

FName USaveComponent::GetSavedLevelName()
{
	if(!UGameplayStatics::DoesSaveGameExist(TEXT("Save"), 0))
		return FName();
	
	Save = Cast<UMySaveGame>(UGameplayStatics::LoadGameFromSlot(TEXT("Save"), 0));
	if(Save)
		return Save->LevelName;

	return FName();
}

void USaveComponent::SaveLook() const
{
	const auto MeshComponent = PlayerCharacter->GetMesh();
	Save->Mesh = MeshComponent->SkeletalMesh;
	Save->Material = MeshComponent->GetMaterial(0)->GetBaseMaterial();
	Save->HiddenBones = PlayerCharacter->HiddenBones;
}

void USaveComponent::SaveWeapons() const
{
	const auto CombatComponent = PlayerCharacter->CombatComponent;
	Save->PrimaryWeaponClass = CombatComponent->WeaponClass;
	Save->SecondaryWeaponClass = CombatComponent->SecondaryWeaponClass;
	Save->SecondaryLeftWeaponClass = CombatComponent->SecondaryLeftWeaponClass;
}

void USaveComponent::SaveAttributes()
{
	const auto AbilitySystem = PlayerCharacter->AbilitySystemComponent;
	for(auto Attribute : AttributesToSave)
	{
		if(Save->Attributes.Contains(Attribute))
		{
			const auto Key = Save->Attributes.Find(Attribute);
				*Key = AbilitySystem->GetNumericAttribute(Attribute);
		}
		else
			Save->Attributes.Add(Attribute, AbilitySystem->GetNumericAttribute(Attribute));
	}
}

void USaveComponent::SaveCasting() const
{
	if(const auto PlayerCastingComponent = Cast<UPlayerCastingComponent>(PlayerCharacter->GetComponentByClass(UPlayerCastingComponent::StaticClass())))
		Save->CastingAbilitiesClasses = PlayerCastingComponent->AbilitiesClasses;
}

void USaveComponent::SaveUpgradeEffects() const
{
	const auto AbilitySystem = PlayerCharacter->AbilitySystemComponent;
	Save->UpgradeEffects.Empty();
	// Save active gameplay effects that match query
	TArray<FActiveGameplayEffectHandle> ActiveEffectsHandles = AbilitySystem->GetActiveEffects(SaveGameplayEffectsQuery);
	for (const FActiveGameplayEffectHandle& ActiveEffectHandle : ActiveEffectsHandles)
	{
		const FActiveGameplayEffect* ActiveGameplayEffect = AbilitySystem->GetActiveGameplayEffect(ActiveEffectHandle);
		if (ActiveGameplayEffect != nullptr)
		{
			FGameplayEffectSaveData EffectSaveData;
			EffectSaveData.EffectSpec = ActiveGameplayEffect->Spec;
			EffectSaveData.EffectElapsedTime = ActiveGameplayEffect->GetDuration() - ActiveGameplayEffect->GetTimeRemaining(AbilitySystem->GetWorld()->GetTimeSeconds());

			Save->UpgradeEffects.Add(EffectSaveData);
		}
	}
}

void USaveComponent::SaveSounds() const
{
	if(const auto SoundsComponent = Cast<USoundsComponent>(PlayerCharacter->GetComponentByClass(USoundsComponent::StaticClass())))
		Save->SoundsToTagsMap = SoundsComponent->SoundsToTagsMap;
}

void USaveComponent::LoadLook() const
{
	const auto MeshComponent = PlayerCharacter->GetMesh();
	MeshComponent->SetSkeletalMesh(Save->Mesh);
	MeshComponent->SetMaterial(0, Save->Material);
	PlayerCharacter->HideBones(Save->HiddenBones);
}

void USaveComponent::LoadWeapons() const
{
	const auto CombatComponent = PlayerCharacter->CombatComponent;
	CombatComponent->WeaponClass = Save->PrimaryWeaponClass;
	CombatComponent->SecondaryWeaponClass = Save->SecondaryWeaponClass;
	CombatComponent->SecondaryLeftWeaponClass = Save->SecondaryLeftWeaponClass;
	//ToDo refactor to make it more clever that just init for a second time.
	CombatComponent->Init();
}

void USaveComponent::LoadAttributes()
{
	const auto AbilitySystem = PlayerCharacter->AbilitySystemComponent;
	for(auto Attribute : AttributesToSave)
		for (auto& Elem : Save->Attributes)
			AbilitySystem->SetNumericAttributeBase(Elem.Key, Elem.Value);
}

void USaveComponent::LoadCasting() const
{
	if(const auto PlayerCastingComponent = Cast<UPlayerCastingComponent>(PlayerCharacter->GetComponentByClass(UPlayerCastingComponent::StaticClass())))
	{
		PlayerCastingComponent->AbilitiesClasses = Save->CastingAbilitiesClasses;
		PlayerCastingComponent->SetupAbilities();
	}
}

void USaveComponent::LoadUpgradeEffects() const
{
	const auto AbilitySystem = PlayerCharacter->AbilitySystemComponent;
	for (const FGameplayEffectSaveData& EffectSaveData : Save->UpgradeEffects)
	{
		const FActiveGameplayEffectHandle RestoredEffectHandle = AbilitySystem->ApplyGameplayEffectSpecToSelf(EffectSaveData.EffectSpec);
		//Granted abilities with handles are not saved, make sure that they are given
		for (FGameplayAbilitySpecDef AbilitySpecDef : EffectSaveData.EffectSpec.GrantedAbilitySpecs)
			AbilitySystem->GiveAbility(FGameplayAbilitySpec(AbilitySpecDef, EffectSaveData.EffectSpec.GetLevel(), INDEX_NONE));

		if (EffectSaveData.EffectSpec.Duration > 0.0f)
			AbilitySystem->ModifyActiveEffectStartTime(RestoredEffectHandle, -EffectSaveData.EffectElapsedTime);
	}
}

void USaveComponent::LoadSounds() const
{
	if(const auto SoundsComponent = Cast<USoundsComponent>(PlayerCharacter->GetComponentByClass(USoundsComponent::StaticClass())))
		SoundsComponent->SoundsToTagsMap = Save->SoundsToTagsMap;
}
