// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/LootComponent.h"
#include "Globals/MyBlueprintFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "AbilitySystemComponent.h"
#include "MyHUD.h"

void ULootComponent::BeginPlay()
{
	Super::BeginPlay();

	ActualGoldAmount = FMath::RandRange(MinGoldAmount, MaxGoldAmount);
}

void ULootComponent::Loot(AActor* Looter)
{
	if(ActualGoldAmount > 0.0f)
	{ 
		if (const auto AbilitySystem = Cast<UAbilitySystemComponent>(Looter->GetComponentByClass(UAbilitySystemComponent::StaticClass())))
			UMyBlueprintFunctionLibrary::ApplyEffectWithMagnitudeByCaller(AbilitySystem, AddGoldEffect, SetByCallerGoldTag, ActualGoldAmount);

		AMyHUD::GetMyHud(this)->ShowGoldCollectedInfo(GetOwner()->GetActorLocation(), ActualGoldAmount);
		UGameplayStatics::PlaySound2D(this, LootingSound);
		ActualGoldAmount = 0.0f;
	}
}
