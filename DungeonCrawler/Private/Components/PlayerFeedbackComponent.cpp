// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerFeedbackComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MyPlayerCameraManager.h"
#include "BaseCharacter.h"
#include "DamageResponseComponent.h"


void UPlayerFeedbackComponent::BeginPlay()
{
	Super::BeginPlay();
	CharacterOwner = Cast<ABaseCharacter>(GetOwner());
	BindDelegates();
}

void UPlayerFeedbackComponent::BindDelegates()
{
	if(!CharacterOwner)
		return;

	if(const auto DamageResponse = CharacterOwner->DamageResponseComponent)
	{
		DamageResponse->OnInvincibleHit.AddDynamic(this, &UPlayerFeedbackComponent::OnAttackHitInvincible);
		DamageResponse->OnBeingBlocked.AddDynamic(this, &UPlayerFeedbackComponent::OnBeingBlocked);
		DamageResponse->OnBeingParried.AddDynamic(this, &UPlayerFeedbackComponent::OnBeingParried);
		DamageResponse->OnBeingCrit.AddDynamic(this, &UPlayerFeedbackComponent::OnBeingCrit);
		DamageResponse->OnBeingHit.AddDynamic(this, &UPlayerFeedbackComponent::OnBeingHit);
		DamageResponse->OnDeath.AddDynamic(this, &UPlayerFeedbackComponent::OnDeath);

		DamageResponse->OnCritDamageDealed.AddDynamic(this, &UPlayerFeedbackComponent::OnAttackCrit);
		DamageResponse->OnBlock.AddDynamic(this, &UPlayerFeedbackComponent::OnAttackBlocked);
		DamageResponse->OnParry.AddDynamic(this, &UPlayerFeedbackComponent::OnAttackParried);
		DamageResponse->OnHit.AddDynamic(this, &UPlayerFeedbackComponent::OnAttackHit);
		DamageResponse->OnKill.AddDynamic(this, &UPlayerFeedbackComponent::OnKill);

	}
}

void UPlayerFeedbackComponent::OnDeath()
{
	PlayActionFeedback(OnDeathFeedback);
}

void UPlayerFeedbackComponent::OnKill(AActor* Target)
{
	PlayActionFeedback(OnKillFeedback);
}

void UPlayerFeedbackComponent::OnBeingHit()
{
	PlayActionFeedback(OnBeingHitFeedback);
}

void UPlayerFeedbackComponent::OnBeingCrit()
{
	PlayActionFeedback(OnBeingCritFeedback);
}

void UPlayerFeedbackComponent::OnBeingBlocked()
{
	PlayActionFeedback(OnBeingBlockedFeedback);
}

void UPlayerFeedbackComponent::OnBeingParried()
{
	PlayActionFeedback(OnBeingParriedFeedback);
}

void UPlayerFeedbackComponent::OnAttackHit()
{
	PlayActionFeedback(OnHitFeedback);
}

void UPlayerFeedbackComponent::OnAttackBlocked()
{
	PlayActionFeedback(OnBlockedFeedback);
}

void UPlayerFeedbackComponent::OnAttackParried(AActor* Actor, TSubclassOf<UDamageType> DamageType, float Amount)
{
	PlayActionFeedback(OnParriedFeedback);
}

void UPlayerFeedbackComponent::OnAttackHitInvincible()
{
	PlayActionFeedback(OnHitInvincibleFeedback);
}

void UPlayerFeedbackComponent::OnCastFailed()
{
	//UGameplayStatics::PlaySound2D(this, CastFailedSound);
}

void UPlayerFeedbackComponent::OnAttackCrit(TSubclassOf<UDamageType> DamageType, float Amount)
{
	PlayActionFeedback(OnCritFeedback);
}

void UPlayerFeedbackComponent::PlayActionFeedback(const FActionFeedbackSetup& FeedbackSetup)
{
	if(FeedbackSetup.Shake)
		AMyPlayerCameraManager::GetMyPlayerCameraManager(this)->StartCameraShake(FeedbackSetup.Shake);

	if(FeedbackSetup.Sound)
		UGameplayStatics::PlaySound2D(this, FeedbackSetup.Sound);
	
	if(FeedbackSetup.ForceFeedback)
		if(auto* PlayerController = UGameplayStatics::GetPlayerController(this, 0))
			PlayerController->ClientPlayForceFeedback (FeedbackSetup.ForceFeedback);

	//Maybe curve would be better?
	if(FeedbackSetup.SlomoTime > 0.0f)
		TryToApplySlomo(FeedbackSetup.SlomoTime, FeedbackSetup.CustomTimeDilation);
}

void UPlayerFeedbackComponent::TryToApplySlomo(const float Time, const float Value)
{
	//Override bigger slomo with smaller one, never the other way around
	if(Value < CurrentCustomTimeDilation)
	{
		CurrentCustomTimeDilation = Value;
		UGameplayStatics::SetGlobalTimeDilation(this, Value);
		SlomoTimer.Invalidate();
		GetOwner()->GetWorldTimerManager().SetTimer(SlomoTimer, this, &UPlayerFeedbackComponent::OnSlomoEnded, 1.0f, false, Time);
	}
}

void UPlayerFeedbackComponent::OnSlomoEnded()
{
	CurrentCustomTimeDilation = 1.0f;
	UGameplayStatics::SetGlobalTimeDilation(this, 1.0f);
}
