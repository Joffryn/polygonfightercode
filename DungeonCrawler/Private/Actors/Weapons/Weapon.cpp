// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon.h"

#include "SoundsComponent.h"
#include "Kismet/GameplayStatics.h"

AWeapon::AWeapon()
{
	SoundsComponent = CreateDefaultSubobject<USoundsComponent>(TEXT("SoundsComponent"));
	PrimaryActorTick.bCanEverTick = true;
	SheathSocket = TEXT("WeaponBack");
	EquipSocket = TEXT("RightHand");
}

void AWeapon::SetCurrentAttackParams(const FAttackParams NewAttackParams)
{
	CurrentAttackParams = NewAttackParams;
}

void AWeapon::AttackStarted()
{
	OnAttackStarted.Broadcast();
	OnAttackStarted.Clear();
}

void AWeapon::HideWeapon()
{
}

void AWeapon::ShowWeapon()
{
}

void AWeapon::AttackFinished()
{
	OnAttackFinished.Broadcast();
	OnAttackFinished.Clear();
}
