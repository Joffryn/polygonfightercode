// Fill out your copyright notice in the Description page of Project Settings.

#include "MeleeWeapon.h"
#include "Components/Combat/ReactionSolverComponent.h"
#include "Components/StaticMeshComponent.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "Components/SceneComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/EngineTypes.h"
#include "WeaponCollision.h"
#include "MyAttributeSet.h"
#include "TrailComponent.h"
#include "BaseCharacter.h"
#include "DataTablesManager.h"
#include "Enums.h"
#include "GameConfigComponent.h"
#include "PoiseComponent.h"
#include "SoundsComponent.h"

AMeleeWeapon::AMeleeWeapon()
{
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponMesh"));
	RootComponent = StaticMeshComponent;
	WeaponCollision = CreateDefaultSubobject<UWeaponCollision>(TEXT("WeaponCollision"));
	WeaponCollision->AttachToComponent(StaticMeshComponent, FAttachmentTransformRules::KeepRelativeTransform);

	TrailComponent = CreateDefaultSubobject<UTrailComponent>(TEXT("TrailComponent"));
	CollisionStartSocketName = FName(TEXT("TrailStart"));
	CollisionEndSocketName = FName(TEXT("TrailEnd"));
	SecondaryCollisionStartSocketName = FName(TEXT("HiltStart"));
	SecondaryCollisionEndSocketName = FName(TEXT("HiltEnd"));
}

void AMeleeWeapon::OnActorOverlapped(AActor* Actor, const FHitResult& HitResult)
{
	//Simple null check 
	if(!Actor)
		return;

	//Weapon is overlapping with owner all the time, let's just ignore it
	if (Actor == GetOwner())
		return;

	//Ignore everything that is owned by owner of this
	if (Actor->GetOwner() == GetOwner())
		return;

	//Don't do anything if attack animation is not played
	if (!bIsDealingDamage)
		return;

	//Do not damage same actor twice in 1 attack
	if (ActorsHitInCurrentAttack.Contains(Actor))
		return;

	//Do not damage friends
	if (FGenericTeamId::GetAttitude(Actor, GetOwner()) == ETeamAttitude::Friendly && !UGameConfigComponent::GetGameConfigComponent(this)->bFriendlyFireEnabled)
		return;
	
	bool bFoundAttribute = false; 

	const float OwnerDamageMultiplier = UAbilitySystemBlueprintLibrary::GetFloatAttribute(GetOwner(), UMyAttributeSet::GetDamageMultiplierAttribute(), bFoundAttribute);
	ActorsHitInCurrentAttack.Add(Actor);
	float DamageDealt = 0.0f;
	
	for(auto DamageParams : CurrentDamageParams)
	{
		//Hack, ToDo: refactor
		auto DamageClass = DamageParams.DamageClass;
		if(DamageClass == DamageType && bIsUnblockable)
			DamageClass = UnblockableDamageType;

		if(bFoundAttribute)
			DamageParams.Amount *= OwnerDamageMultiplier;

		DamageDealt += UGameplayStatics::ApplyPointDamage(Actor, DamageParams.Amount, GetActorLocation(), HitResult, GetInstigator()->GetController(), GetOwner(), DamageClass);
	}
	
	if(DamageDealt > 0.0f)
	{
		PlayHitSound();
		UReactionSolverComponent::GetReactionSolver(this)->SendStaggerGameplayEvent(Actor, CurrentAttackStaggerStrength);
	}
}

void AMeleeWeapon::ClearHitActors()
{
	ActorsHitInCurrentAttack.Empty();
}

void AMeleeWeapon::PlayHitSound() const
{
	SoundsComponent->SpawnSound(WeaponHitTag);
}

void AMeleeWeapon::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsDealingDamage)
		TraceWeaponCollision();
}

void AMeleeWeapon::BeginPlay()
{
	Super::BeginPlay();
	LoadConfigFromDataTable();
}

UStaticMeshComponent* AMeleeWeapon::GetStaticMesh_Implementation()
{
	return StaticMeshComponent;
}

void AMeleeWeapon::CalculateWeaponDamage(const float DamageMultiplier, const int32 ExtraAttackStaggerStrength)
{
	CurrentDamageParams.Empty();
	for(auto DamageParams : BaseDamageParams )
	{
		DamageParams.Amount *= DamageMultiplier;
		CurrentDamageParams.Add(DamageParams);
	}
	
	int32 CurrentAttackStaggerValue = static_cast<int32>(BaseAttackStaggerStrength) + ExtraAttackStaggerStrength;
	CurrentAttackStaggerValue = FMath::Clamp(CurrentAttackStaggerValue, static_cast<int32>(EAttackStaggerStrength::None), static_cast<int32>(EAttackStaggerStrength::UltraHeavy));
	CurrentAttackStaggerStrength = static_cast<EAttackStaggerStrength>(CurrentAttackStaggerValue);
}

void AMeleeWeapon::HideWeapon()
{
	Super::HideWeapon();
	CollisionEnabledCache = StaticMeshComponent->GetCollisionEnabled();

	TArray<UActorComponent*> Components;
	GetComponents(UMeshComponent::StaticClass(), Components);
	for(const auto Component : Components)
		if(const auto MeshComponent = Cast<UMeshComponent>(Component))
			{
				MeshComponent->SetCollisionEnabled(ECollisionEnabled::Type::NoCollision);
				MeshComponent->SetVisibility(false);
			}
	
}

void AMeleeWeapon::ShowWeapon()
{
	Super::ShowWeapon();
	TArray<UActorComponent*> Components;
	GetComponents(UMeshComponent::StaticClass(), Components);
	for(const auto Component : Components)
		if(const auto MeshComponent = Cast<UMeshComponent>(Component))
		{
			MeshComponent->SetCollisionEnabled(CollisionEnabledCache);
			MeshComponent->SetVisibility(true);
		}
	
}

void AMeleeWeapon::OnCollisionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (bIsDealingDamage)
		OnActorOverlapped(OtherActor, SweepResult);
}

void AMeleeWeapon::TraceWeaponCollision()
{
	auto CollisionProfileName = WeaponCollision->GetCollisionProfileName();
	FVector WeaponCollisionStartLocation = StaticMeshComponent->GetSocketLocation(CollisionStartSocketName);
	FVector WeaponCollisionEndLocation = StaticMeshComponent->GetSocketLocation(CollisionEndSocketName);
	TArray<AActor*> ActorsToIgnore;
	ActorsToIgnore.Add(GetOwner());
	TArray<FHitResult> OutHits;

	UKismetSystemLibrary::BoxTraceMultiByProfile(this, WeaponCollisionStartLocation, WeaponCollisionEndLocation, WeaponCollision->GetScaledBoxExtent(), WeaponCollision->GetComponentRotation(),
	CollisionProfileName, true, ActorsToIgnore, bShowDebug ? EDrawDebugTrace::Type::ForDuration : EDrawDebugTrace::Type::None, OutHits, true);

	for (auto Hit : OutHits)
		OnActorOverlapped(Hit.GetActor(), Hit);

	if (SecondaryWeaponCollision)
	{
		FVector SecondaryWeaponCollisionStartLocation = StaticMeshComponent->GetSocketLocation(SecondaryCollisionStartSocketName);
		FVector SecondaryWeaponCollisionEndLocation = StaticMeshComponent->GetSocketLocation(SecondaryCollisionEndSocketName);

		UKismetSystemLibrary::BoxTraceMultiByProfile(this, SecondaryWeaponCollisionStartLocation, SecondaryWeaponCollisionEndLocation, WeaponCollision->GetScaledBoxExtent(), WeaponCollision->GetComponentRotation(),
			CollisionProfileName, true, ActorsToIgnore, bShowDebug ? EDrawDebugTrace::Type::ForDuration : EDrawDebugTrace::Type::None, OutHits, true);

		for (auto Hit : OutHits)
			OnActorOverlapped(Hit.GetActor(), Hit);
	}
}

void AMeleeWeapon::LoadConfigFromDataTable()
{
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto WeaponsDataTable = DataTablesManager->Weapons)
			for(const auto RowName : WeaponsDataTable->GetRowNames())
				if(const auto Row = WeaponsDataTable->FindRow<FWeaponsSetup>(RowName, TEXT("")))
					if(Row->WeaponClass == GetClass())
					{
						BaseAttackStaggerStrength = Row->BaseAttackStaggerStrength;
						BaseDamageParams = Row->BaseDamageParams;
						BaseStaminaCost = Row->BaseStaminaCost;
						WeaponType = Row->WeaponType;
						return;
					}
}
