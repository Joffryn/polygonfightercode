// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseCharacter.h"

#include "AIController.h"
#include "BlueprintGameplayTagLibrary.h"
#include "Components/Combat/RotationComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AIPerceptionSystem.h"
#include "MyCharacterMovementComponent.h"
#include "MyAbilitySystemComponent.h"
#include "DamageResponseComponent.h"
#include "Perception/AISense_Touch.h"
#include "Perception/AISense_Team.h"
#include "Kismet/GameplayStatics.h"
#include "TargetSystemComponent.h"
#include "NavigationSystem.h"
#include "SoundsComponent.h"
#include "CombatComponent.h"
#include "DataTablesManager.h"
#include "StatsComponent.h"
#include "PoiseComponent.h"
#include "ImGuiCommon.h"
#include "ImGuiModule.h"
#include "Enums.h"

ABaseCharacter::ABaseCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UMyCharacterMovementComponent>(CharacterMovementComponentName))
{
	AbilitySystemComponent = CreateDefaultSubobject<UMyAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	DamageResponseComponent = CreateDefaultSubobject<UDamageResponseComponent>(TEXT("DamageResponseComponent"));
	TargetSystemComponent = CreateDefaultSubobject<UTargetSystemComponent>(TEXT("TargetSystemComponent"));
	PerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("MyPerceptionComponent"));
	RotationComponent = CreateDefaultSubobject<URotationComponent>(TEXT("RotationComponent"));
	CombatComponent = CreateDefaultSubobject<UCombatComponent>(TEXT("CombatComponent"));
	SoundsComponent = CreateDefaultSubobject<USoundsComponent>(TEXT("SoundsComponent"));
	StatsComponent = CreateDefaultSubobject<UStatsComponent>(TEXT("StatsComponent"));
	PoiseComponent = CreateDefaultSubobject<UPoiseComponent>(TEXT("PoiseComponent"));
}

ABaseCharacter* ABaseCharacter::GetMyPlayerCharacter(const UObject* WorldContextObject)
{
	return Cast<ABaseCharacter>(UGameplayStatics::GetPlayerPawn(WorldContextObject, 0));
}

void ABaseCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	if (AbilitySystemComponent)
		for (TSubclassOf<UGameplayEffect> EffectClass : DefaultInfiniteEffects)
			AbilitySystemComponent->ApplyGameplayEffectToTarget(EffectClass.GetDefaultObject(), AbilitySystemComponent, 1);
}

 UMyCharacterMovementComponent* ABaseCharacter::GetMyCharacterMovementComponent() const 
{
	return Cast<UMyCharacterMovementComponent>(GetCharacterMovement());
}

void ABaseCharacter::HideBones(TArray<FName> BonesToHide)
{
	for(auto Bone : BonesToHide)
	{
		HiddenBones.Add(Bone);
		GetMesh()->HideBoneByName(Bone, EPhysBodyOp::PBO_None);
	}
}

bool ABaseCharacter::IsAlive() const
{
	return StatsComponent->GetStatValue(EStat::Health) > 0.0f;
}

ETeamAttitude::Type ABaseCharacter::GetTeamAttitudeTowardsActor(const AActor* Target) const
{
	return GetTeamAttitudeTowards(*Target);
}

void ABaseCharacter::SendTeamEvent(AActor* Target, const float EventRange /*= 1000.0f*/)
{
	//Only send event if target is hostile  
	if (GetTeamAttitudeTowards(*Target) == ETeamAttitude::Hostile)
		UAIPerceptionSystem::OnEvent<FAITeamStimulusEvent, FAITeamStimulusEvent::FSenseClass>(GetWorld(),
			FAITeamStimulusEvent(this, Target, Target->GetActorLocation(), EventRange));
}

void ABaseCharacter::SendTouchEvent(AActor* TouchReceiver)
{
	//Only send event to the hostile
	if(GetTeamAttitudeTowards(*TouchReceiver) == ETeamAttitude::Hostile)
		UAIPerceptionSystem::OnEvent<FAITouchEvent, FAITouchEvent::FSenseClass>(GetWorld(), FAITouchEvent(TouchReceiver, this, GetActorLocation()));
}

void ABaseCharacter::BeginPlay() 
{
	Super::BeginPlay();
	LoadAtrributesFromDataTable();
}

float ABaseCharacter::InternalTakePointDamage(const float Damage, FPointDamageEvent const& PointDamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	 return DamageResponseComponent->OnOwnerPointDamageTaken(Damage, PointDamageEvent.HitInfo.Location, PointDamageEvent.ShotDirection, PointDamageEvent.DamageTypeClass, DamageCauser);
}

float ABaseCharacter::InternalTakeRadialDamage(float Damage, FRadialDamageEvent const& RadialDamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::InternalTakeRadialDamage(Damage, RadialDamageEvent, EventInstigator, DamageCauser);
	return DamageResponseComponent->OnOwnerRadialDamageTaken(Damage, RadialDamageEvent.Origin, RadialDamageEvent.DamageTypeClass, DamageCauser);
}

void ABaseCharacter::GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const
{
	TagContainer.Reset();
	if (IsValid(AbilitySystemComponent))
		AbilitySystemComponent->GetOwnedGameplayTags(TagContainer);
}

void ABaseCharacter::SetGenericTeamId(const FGenericTeamId& NewTeamID)
{
	TeamID = NewTeamID; 
}

FGenericTeamId ABaseCharacter::GetGenericTeamId() const
{
	return TeamID;
}

UAIPerceptionComponent* ABaseCharacter::GetPerceptionComponent()
{
	return PerceptionComponent;
}

UAbilitySystemComponent* ABaseCharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

void ABaseCharacter::SetAttributesFromSetup(TMap<FGameplayAttribute, float> AttributesToValuesSetup)
{
	for(auto& Elem : AttributesToValuesSetup)
		AbilitySystemComponent->SetNumericAttributeBase(Elem.Key, Elem.Value);
}

void ABaseCharacter::LoadAtrributesFromDataTable()
{
	//Ignore for the player
	if(GetController() == UGameplayStatics::GetPlayerController(this, 0))
		return;
		
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto AICharactersDataTable = DataTablesManager->AICharactersStats)
			for(const auto RowName : AICharactersDataTable->GetRowNames())
				if(const auto Row = AICharactersDataTable->FindRow<FCharacterStatsSetup>(RowName, TEXT("")))
					if(Row->CharacterClass == GetClass())
					{
						SetAttributesFromSetup(Row->AttributesToValuesSetup);
						return;
					}
}
