// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Characters/TargetDummy.h"

#include "DamageResponseComponent.h"
#include "MyAbilitySystemComponent.h"
#include "MyAttributeSet.h"

ATargetDummy::ATargetDummy()
{
	AbilitySystemComponent = CreateDefaultSubobject<UMyAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	AttributeSet = CreateDefaultSubobject<UMyAttributeSet>(TEXT("AttributeSet"));
}

UAbilitySystemComponent* ATargetDummy::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

float ATargetDummy::InternalTakePointDamage(const float Damage, FPointDamageEvent const& PointDamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if(const auto DamageResponseComponent = Cast<UDamageResponseComponent>(GetComponentByClass(UDamageResponseComponent::StaticClass())))
		return DamageResponseComponent->OnOwnerPointDamageTaken(Damage, PointDamageEvent.HitInfo.Location, PointDamageEvent.ShotDirection, PointDamageEvent.DamageTypeClass, DamageCauser);

	return Super::InternalTakePointDamage(Damage, PointDamageEvent, EventInstigator, DamageCauser);
}
