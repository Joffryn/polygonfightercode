// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/ThrowedProjectile.h"
#include "MyProjectileMovementComponent.h"

void AThrowedProjectile::BeginPlay()
{
	Super::BeginPlay();

	if (bIsFake)
	{
		ProjectileMovementComponent->Deactivate();
		if (UPrimitiveComponent* RootPrimitiveComponent = Cast<UPrimitiveComponent>(GetRootComponent()))
			RootPrimitiveComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	}
	else
	{
		if (InitialVelocity != FVector::ZeroVector)
			ProjectileMovementComponent->Velocity = InitialVelocity;
	}
}
