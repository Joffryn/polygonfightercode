// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Projectile.h"
#include "MyProjectileMovementComponent.h"

AProjectile::AProjectile()
{
	ProjectileMovementComponent = CreateDefaultSubobject<UMyProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
}

void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	
	IgnoreOwnerWhileMoving();
}

void AProjectile::SimulatePhysics_Implementation()
{
	ProjectileMovementComponent->DestroyComponent();
	if (UPrimitiveComponent* RootPrimitiveComponent = Cast<UPrimitiveComponent>(GetRootComponent()))
		RootPrimitiveComponent->SetSimulatePhysics(true);

	ClearTrails();
}

void AProjectile::IgnoreOwnerWhileMoving() const
{
	if (UPrimitiveComponent* RootPrimitiveComponent = Cast<UPrimitiveComponent>(GetRootComponent()))
	{
		if (GetOwner())
		{
			RootPrimitiveComponent->IgnoreActorWhenMoving(GetOwner(), true);
			TArray<UChildActorComponent*> ChildComponents;
			GetOwner()->GetComponents<UChildActorComponent>(ChildComponents);
			for (const auto ChildComponent : ChildComponents)
			{
				RootPrimitiveComponent->IgnoreActorWhenMoving(ChildComponent->GetChildActor(), true);
			}
		}
	}
}

void AProjectile::ClearTrails()
{
	if (const auto Mesh = GetProjectileMesh())
	{
		TArray<USceneComponent*> MeshChildren;
		Mesh->GetChildrenComponents(true, MeshChildren);
		for (const auto MeshChild : MeshChildren)
			MeshChild->DestroyComponent();
	}
}

