// Fill out your copyright notice in the Description page of Project Settings.

#include "MyGameMode.h"




#include "Components/Combat/ReactionSolverComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameConfigComponent.h"
#include "DataTablesManager.h"
#include "MusicManager.h"
#include "CritSolver.h"
#include "AIManager.h"
#include "TravellingManager.h"

AMyGameMode::AMyGameMode()
{
	GameConfigComponent = CreateDefaultSubobject<UGameConfigComponent>(TEXT("GameConfigComponent"));
	TravellingManager = CreateDefaultSubobject<UTravellingManager>(TEXT("TravellingManager"));
	ReactionSolver = CreateDefaultSubobject<UReactionSolverComponent>(TEXT("ReactionSolver"));
	DataTablesManager = CreateDefaultSubobject<UDataTablesManager>(TEXT("DataTablesManager"));
	MusicManager = CreateDefaultSubobject<UMusicManager>(TEXT("MusicManager"));
	CritSolver = CreateDefaultSubobject<UCritSolver>(TEXT("CritSolver"));
	AIManager = CreateDefaultSubobject<UAIManager>(TEXT("AIManager"));
}

AMyGameMode* AMyGameMode::GetMyGameMode(const UObject* WorldContextObject)
{
	return Cast<AMyGameMode>(UGameplayStatics::GetGameMode(WorldContextObject));
}
