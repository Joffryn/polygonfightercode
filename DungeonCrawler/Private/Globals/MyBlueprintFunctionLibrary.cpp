// Fill out your copyright notice in the Description page of Project Settings.


#include "Globals/MyBlueprintFunctionLibrary.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"
#include "BaseCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "GameplayTagContainer.h"
#include "GameplayEffectTypes.h"
#include "GameplayEffect.h"

UActorComponent* UMyBlueprintFunctionLibrary::AddActorComponent(AActor* Owner, const TSubclassOf<UActorComponent> ActorComponentClass)
{
	const UClass* BaseClass = FindObject<UClass>(ANY_PACKAGE, TEXT("ActorComponent"));
	if (ActorComponentClass->IsChildOf(BaseClass))
	{
		UActorComponent* NewComp = NewObject<UActorComponent>(Owner, ActorComponentClass);
		if (!NewComp)
			return nullptr;

		NewComp->RegisterComponent();

		return NewComp;
	}
	return nullptr;
}

class AActor* UMyBlueprintFunctionLibrary::GetClosestActorOfClass(const UObject* WorldContextObject, const FVector Location, const TSubclassOf<AActor> ActorClass)
{
	AActor* ClosestActor = nullptr;
	float DistanceToTheClosestActor = FLT_MAX;

	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(WorldContextObject, ActorClass, Actors);

	for (const auto Actor : Actors)
	{
		const float DistanceToTheActor = (Actor->GetActorLocation() - Location).Size();
		if (DistanceToTheActor < DistanceToTheClosestActor)
		{
			DistanceToTheClosestActor = DistanceToTheActor;
			ClosestActor = Actor;
		}
	}

	return ClosestActor;
}

TArray<TSubclassOf<UGameplayAbility>> UMyBlueprintFunctionLibrary::GetAbilitiesClassesGrantedByEffect(const TSubclassOf<UGameplayEffect> EffectClass)
{
	TArray<TSubclassOf<UGameplayAbility>> AbilitiesClasses;

	const auto DefaultObject = Cast<UGameplayEffect>(EffectClass->GetDefaultObject());

	for(auto GrantedAbility : DefaultObject->GrantedAbilities)
		AbilitiesClasses.Add(GrantedAbility.Ability);
	
	return AbilitiesClasses;
}

UMyAbilitySystemComponent* UMyBlueprintFunctionLibrary::GetPlayerAbilitySystemComponent(const UObject* WorldContextObject)
{
	return ABaseCharacter::GetMyPlayerCharacter(WorldContextObject)->AbilitySystemComponent;
}

void UMyBlueprintFunctionLibrary::ApplyEffectWithMagnitudeByCaller(UAbilitySystemComponent* AbilitySystemComponent, const TSubclassOf<UGameplayEffect> EffectToApply, const FGameplayTag SetByCallerTag, const float Magnitude)
{
	if(!AbilitySystemComponent)
		return;

	auto Spec = AbilitySystemComponent->MakeOutgoingSpec(EffectToApply, 1.0f, FGameplayEffectContextHandle());
	Spec = UAbilitySystemBlueprintLibrary::AssignTagSetByCallerMagnitude(Spec, SetByCallerTag, Magnitude);
	AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*Spec.Data.Get());
}
