// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerController.h"

#include "Kismet/GameplayStatics.h"
#include "ImGuiDebugMenu.h"
#include "ImGuiCommon.h"

AMyPlayerController* AMyPlayerController::GetMyPlayerController(const UObject* WorldContextObject)
{
	return Cast<AMyPlayerController>(UGameplayStatics::GetPlayerController(WorldContextObject, 0));
}

void AMyPlayerController::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);

#if WITH_IMGUI
	if (DebugMenu == nullptr)
	{
		DebugMenu = new ImGuiDebugMenu();
	}
	DebugMenu->Render(this);
#endif
	
}

void AMyPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	OnPossesDelegate.Broadcast(InPawn);
}
