// Fill out your copyright notice in the Description page of Project Settings.


#include "Globals/ImGuiDebugMenu.h"

#include "AICombatComponent.h"
#include "AIManager.h"
#include "BaseCharacter.h"
#include "MusicManager.h"
#include "TargetSystemComponent.h"
#include "Components/AudioComponent.h"

#if WITH_IMGUI

#include "MyPlayerController.h"
#include "ImGuiModule.h"

namespace 
{
	const FString AIActionsStrings[] = {FString("FuckAround"), FString("MeleeAttack"), FString("CloseDistanceAttack"), FString("RangedAttack"),
		FString("Block"), FString("Strafe"), FString("Taunt"), FString("Flex"), FString("Drink")};
}

ImGuiDebugMenu::ImGuiDebugMenu()
{
	FImGuiModule::Get().GetProperties().SetGamepadNavigationEnabled(true);
}

void ImGuiDebugMenu::Render(AMyPlayerController* PlayerController)
{
	if (!PlayerController)
	{
		return;
	}

	const bool bShowMainMenu = FImGuiModule::Get().IsInputMode();

	if (bShowMainMenu)
	{
		if (ImGui::BeginMainMenuBar())
		{
			if (ImGui::MenuItem("AI Combat")) 
			{
				AICombatDebugger.bEnabled = !AICombatDebugger.bEnabled;
			}

			if (ImGui::MenuItem("Music Manager")) 
			{
				MusicManagerDebugger.bEnabled = !MusicManagerDebugger.bEnabled;
			}

			if (ImGui::MenuItem("AI Manager")) 
			{
				AIManagerDebugger.bEnabled = !AIManagerDebugger.bEnabled;
			}
			
			ImGui::EndMainMenuBar();
		}
	}
	
	if (AICombatDebugger.bEnabled)
	{
		AICombatDebugger.Render(PlayerController);
	}

	if (MusicManagerDebugger.bEnabled)
	{
		MusicManagerDebugger.Render(PlayerController);
	}

	if (AIManagerDebugger.bEnabled)
	{
		AIManagerDebugger.Render(PlayerController);
	}
}

void ImGuiDebugMenu::FAICombatDebugger::Render(const AMyPlayerController* PlayerController)
{
	ImGui::SetNextWindowSize(ImVec2(400, 300), ImGuiCond_FirstUseEver);
	if (ImGui::Begin("AI Combat", &bEnabled))
	{
		const auto World = PlayerController->GetWorld();
		if(!World)
			return;
		const auto Player = ABaseCharacter::GetMyPlayerCharacter(World);
		if(!Player)
			return;
		
		if (const auto Target = Cast<ABaseCharacter>(Player->TargetSystemComponent->TargetetActor))
		{
			const auto AICombatComponent = Cast<UAICombatComponent>(Target->GetComponentByClass(UAICombatComponent::StaticClass()));
			if(!AICombatComponent)
				return;

			ImGui::Text("Desired range: %f", AICombatComponent->GetDesiredRangeValue());
			
			if (ImGui::CollapsingHeader("Cooldowns", ImGuiTreeNodeFlags_DefaultOpen))
			{
				ImGui::Text("Global Actions Cooldown: %f", AICombatComponent->GlobalActionsCooldown);
				
				for(const auto ActionCooldown : AICombatComponent->AIActionToCooldownsSetup)
					ImGui::Text("Action: %s, Cooldown: %f", TCHAR_TO_ANSI(*AIActionsStrings[static_cast<int32>(ActionCooldown.Key)]), ActionCooldown.Value);
			}
			
			ImGui::EndChild();
		}
	}
	ImGui::End();
}

void ImGuiDebugMenu::FMusicManagerDebugger::Render(const AMyPlayerController* PlayerController)
{
	ImGui::SetNextWindowSize(ImVec2(250, 200), ImGuiCond_FirstUseEver);
	if (ImGui::Begin("Music Manager", &bEnabled))
	{
		const auto World = PlayerController->GetWorld();
		if(!World)
			return;
		const auto MusicManager = UMusicManager::GetMusicManager(World);
		if(MusicManager)
		{
			if(MusicManager->CurrentlyPlayedMusic)
			{
				ImGui::Text("Currently played music: %s", TCHAR_TO_ANSI(*MusicManager->CurrentlyPlayedMusic->Sound->GetName()));
				//ImGui::Text("Progress : %f / %f", MusicManager->CurrentlyPlayedMusic->Sound->Get(), MusicManager->CurrentlyPlayedMusic->Sound->GetDuration());
			}
			else
				ImGui::Text("No music is played.");

			if (ImGui::CollapsingHeader("Musics", ImGuiTreeNodeFlags_DefaultOpen))
			{
				if(MusicManager->CurrentBossMusic)
					ImGui::Text("Boss music: %s", TCHAR_TO_ANSI(*MusicManager->CurrentBossMusic->Sound->GetName()));
				else
					ImGui::Text("No boss music.");
				
				if(MusicManager->CurrentCombatMusic)
					ImGui::Text("Combat music: %s", TCHAR_TO_ANSI(*MusicManager->CurrentCombatMusic->Sound->GetName()));
				else
					ImGui::Text("No combat music.");
				
				if(MusicManager->CurrentLocationMusic)
					ImGui::Text("Location music: %s", TCHAR_TO_ANSI(*MusicManager->CurrentLocationMusic->Sound->GetName()));
				else
					ImGui::Text("No location music.");
			}
			
			ImGui::EndChild();
		}
	}
	ImGui::End();
}

void ImGuiDebugMenu::FAIManagerDebugger::Render(const AMyPlayerController* PlayerController)
{
	ImGui::SetNextWindowSize(ImVec2(100, 200), ImGuiCond_FirstUseEver);
	if (ImGui::Begin("AI Manager", &bEnabled))
	{
		const auto World = PlayerController->GetWorld();
		if(!World)
			return;
		const auto AIManager = UAIManager::GetAIManager(World);
		if(AIManager)
		{
			ImGui::Text("Alive AIs: %d", AIManager->RegisteredAIs.Num());
			ImGui::Text("Alive bosses: %d", AIManager->RegisteredBosses.Num());
			ImGui::Text("AIs aware of the player: %d", AIManager->AIsAwareOfThePlayer.Num());

			ImGui::EndChild();
		}
	}
	ImGui::End();
}

#endif
