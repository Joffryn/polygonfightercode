// Fill out your copyright notice in the Description page of Project Settings.

#include "MyGameState.h"

#include "SaveComponent.h"
#include "Kismet/GameplayStatics.h"

AMyGameState::AMyGameState()
{
	SaveComponent = CreateDefaultSubobject<USaveComponent>(TEXT("SaveComponent"));
}

AMyGameState* AMyGameState::GetMyGameState(const UObject* WorldContextObject)
{
	return Cast<AMyGameState>(UGameplayStatics::GetGameState(WorldContextObject));
}
