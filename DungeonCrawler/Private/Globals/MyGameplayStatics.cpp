// Fill out your copyright notice in the Description page of Project Settings.

#include "Globals/MyGameplayStatics.h"
#include "AbilitySystemComponent.h"
#include "GameplayTagContainer.h"
#include "MyGameInstance.h"
#include "Kismet/GameplayStatics.h"

void UMyGameplayStatics::ExecuteGameplayCue(UAbilitySystemComponent* AbilitySystem, const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters)
{
	if (AbilitySystem)
		AbilitySystem->ExecuteGameplayCue(GameplayCueTag, GameplayCueParameters);
}

void UMyGameplayStatics::RemoveGameplayCue(UAbilitySystemComponent* AbilitySystem, const FGameplayTag GameplayCueTag)
{
	if (AbilitySystem)
		AbilitySystem->RemoveGameplayCue(GameplayCueTag);
}

void UMyGameplayStatics::AddGameplayCue(UAbilitySystemComponent* AbilitySystem, const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters)
{
	if (AbilitySystem)
		AbilitySystem->AddGameplayCue(GameplayCueTag, GameplayCueParameters);
}

void UMyGameplayStatics::TravelToLevel(const UObject* WorldContextObject, const FName LevelName, const bool bAbsolute, const FString Options)
{
	if(const auto MyGameInstance = UMyGameInstance::GetMyGameInstance(WorldContextObject))
		MyGameInstance->bIsTravellingBetweenLevels = true;

	UGameplayStatics::OpenLevel(WorldContextObject, LevelName, bAbsolute, Options);
}

void UMyGameplayStatics::SetAudioAssetSoundClass(USoundBase* Sound, USoundClass* InSoundClass)
{
	if(Sound)
		if(InSoundClass)
		  Sound->SoundClassObject = InSoundClass;
}

USoundClass* UMyGameplayStatics::GetAudioAssetSoundClass(USoundBase* Sound)
{
	if(Sound)
		return Sound->SoundClassObject;

	return nullptr;
}

FLinearColor UMyGameplayStatics::GetAverageColor(TArray<FLinearColor> Colors)
{
	FLinearColor FinalColor = FLinearColor::Black;
	for(auto Color : Colors)
	{
		FinalColor += Color / Colors.Num();
	}
	return FinalColor;
}

TArray<UMeshComponent*> UMyGameplayStatics::GetActorMeshes(AActor* Actor)
{
	TArray<UMeshComponent*> ActorMeshComponents;
	if(!Actor)
		return ActorMeshComponents;
	
	TArray<UActorComponent*> Components;
	Actor->GetComponents(UMeshComponent::StaticClass(), Components);
	for(const auto Component : Components)
		if(auto MeshComponent = Cast<UMeshComponent>(Component))
			ActorMeshComponents.Add(MeshComponent);

	return ActorMeshComponents;
}

FGameplayEffectContextHandle UMyGameplayStatics::MakeGameplayEffectContextWithParams(UAbilitySystemComponent* AbilitySystemComponent, AActor* Instigator, AActor* Causer)
{
	FGameplayEffectContextHandle GameplayEffectContextHandle = AbilitySystemComponent->MakeEffectContext();
	if (IsValid(Instigator) || IsValid(Causer))
		GameplayEffectContextHandle.AddInstigator(Instigator, Causer);

	return GameplayEffectContextHandle;
}
