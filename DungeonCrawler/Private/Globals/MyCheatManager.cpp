// Fill out your copyright notice in the Description page of Project Settings.

#include "MyCheatManager.h"
#include "Components/Combat/BossComponent.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "CombatComponent.h"
#include "AIController.h"
#include "AIManager.h"
#include "MyAbilitySystemComponent.h"
#include "GameConfigComponent.h"
#include "MyAttributeSet.h"
#include "BaseCharacter.h"
#include "DataTablesManager.h"
#include "MyBlueprintFunctionLibrary.h"
#include "MyGameplayStatics.h"
#include "PlayerCastingComponent.h"
#include "PoiseComponent.h"
#include "SaveComponent.h"
#include "TargetSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "ImGuiCommon.h"
#include "ImGuiDebugMenu.h"
#include "ImGuiModule.h"
#include "MyPlayerController.h"


void UMyCheatManager::FriendlyFire() const
{
	UGameConfigComponent::GetGameConfigComponent(this)->bFriendlyFireEnabled = !UGameConfigComponent::GetGameConfigComponent(this)->bFriendlyFireEnabled;
}

void UMyCheatManager::ChangePoiseLevel(const int32 Level) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
		Player->PoiseComponent->ChangePoiseLevel(Level);
}

void UMyCheatManager::Unstaggerable() const
{
	if(const auto PoiseComponent = ABaseCharacter::GetMyPlayerCharacter(this)->PoiseComponent)
		if(PoiseComponent->ActualPoiseLevel < 1000)
			PoiseComponent->ChangePoiseLevel(1000);
		else
			PoiseComponent->ChangePoiseLevel(-1000);
}

void UMyCheatManager::Suicide() const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
		UGameplayStatics::ApplyDamage(Player, 99999999999.0f, nullptr, nullptr, InevitableDamageType);
}

void UMyCheatManager::DebugMenu() const
{
#if WITH_IMGUI
	const auto Properties = FImGuiModule::Get().GetProperties();
	FImGuiModule::Get().GetProperties().SetInputEnabled(!Properties.IsInputEnabled());
#endif
}

void UMyCheatManager::AICombatDebug() const
{
#if WITH_IMGUI
	if (const auto MyPlayerController = AMyPlayerController::GetMyPlayerController(this))
		MyPlayerController->DebugMenu->AICombatDebugger.bEnabled = !MyPlayerController->DebugMenu->AICombatDebugger.bEnabled;
#endif
}

void UMyCheatManager::AIManagerDebug() const
{
#if WITH_IMGUI
	if (const auto MyPlayerController = AMyPlayerController::GetMyPlayerController(this))
		MyPlayerController->DebugMenu->AIManagerDebugger.bEnabled = !MyPlayerController->DebugMenu->AIManagerDebugger.bEnabled;
#endif
}

void UMyCheatManager::MusicManagerDebug() const
{
#if WITH_IMGUI
	if (const auto MyPlayerController = AMyPlayerController::GetMyPlayerController(this))
		MyPlayerController->DebugMenu->MusicManagerDebugger.bEnabled = !MyPlayerController->DebugMenu->MusicManagerDebugger.bEnabled;
#endif
}

void UMyCheatManager::Kill() const
{
	if(const auto Target = ABaseCharacter::GetMyPlayerCharacter(this)->TargetSystemComponent->TargetetActor)
			UGameplayStatics::ApplyDamage(Target, 99999999.0f, nullptr, nullptr, InevitableDamageType);
}

void UMyCheatManager::EquipWeapon(const FName WeaponName) const
{
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto WeaponsDataTable = DataTablesManager->Weapons)
			if(const auto Row = WeaponsDataTable->FindRow<FWeaponsSetup>(WeaponName, TEXT("")))
				if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
					Player->CombatComponent->EquipWeaponByClass(Row->WeaponClass);
}

void UMyCheatManager::AddPerk(const FName PerkName) const
{
	const FGameplayEffectContextHandle EffectContext;
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto PerksDataTable = DataTablesManager->Perks)
			if(const auto Row = PerksDataTable->FindRow<FUpgradeChoice>(PerkName, TEXT("")))
				if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
					Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(Row->EffectToApply.GetDefaultObject(), 1.0f, EffectContext);
}

void UMyCheatManager::AddAllPerks() const
{
	const FGameplayEffectContextHandle EffectContext;
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto PerksDataTable = DataTablesManager->Perks)
			for(const auto RowName : PerksDataTable->GetRowNames())
				if(const auto Row = PerksDataTable->FindRow<FUpgradeChoice>(RowName, TEXT("")))
					if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
						Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(Row->EffectToApply.GetDefaultObject(), 1.0f, EffectContext);
}

void UMyCheatManager::RemovePerk(FName PerkName) const
{
	const FGameplayEffectContextHandle EffectContext;
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto PerksDataTable = DataTablesManager->Perks)
			if(const auto Row = PerksDataTable->FindRow<FUpgradeChoice>(PerkName, TEXT("")))
				if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
					Player->AbilitySystemComponent->RemoveActiveGameplayEffectBySourceEffect(Row->EffectToApply, nullptr);
}

void UMyCheatManager::RemoveAllPerks() const
{
	const FGameplayEffectContextHandle EffectContext;
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto PerksDataTable = DataTablesManager->Perks)
			for(const auto RowName : PerksDataTable->GetRowNames())
				if(const auto Row = PerksDataTable->FindRow<FUpgradeChoice>(RowName, TEXT("")))
					if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
						Player->AbilitySystemComponent->RemoveActiveGameplayEffectBySourceEffect(Row->EffectToApply, nullptr);
}

void UMyCheatManager::AddSkill(const FName SkillName) const
{
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto SkillsDataTable = DataTablesManager->Skills)
			if(const auto Row = SkillsDataTable->FindRow<FUpgradeChoice>(SkillName, TEXT("")))
				AddSkillByClass(Row->EffectToApply);
}

void UMyCheatManager::AddAllSkills() const
{
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto SkillsDataTable = DataTablesManager->Skills)
			for(const auto RowName : SkillsDataTable->GetRowNames())
				if(const auto Row = SkillsDataTable->FindRow<FUpgradeChoice>(RowName, TEXT("")))
					AddSkillByClass(Row->EffectToApply);
}

void UMyCheatManager::RemoveSkill(FName SkillName) const
{
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto SkillsDataTable = DataTablesManager->Skills)
			if(const auto Row = SkillsDataTable->FindRow<FUpgradeChoice>(SkillName, TEXT("")))
				RemoveSkillByClass(Row->EffectToApply);
}

void UMyCheatManager::RemoveAllSkills() const
{
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto SkillsDataTable = DataTablesManager->Skills)
			for(const auto RowName : SkillsDataTable->GetRowNames())
				if(const auto Row = SkillsDataTable->FindRow<FUpgradeChoice>(RowName, TEXT("")))
					RemoveSkillByClass(Row->EffectToApply);
}

void UMyCheatManager::Damage(const float Amount) const
{
	if(const auto Target = ABaseCharacter::GetMyPlayerCharacter(this)->TargetSystemComponent->TargetetActor)
		UGameplayStatics::ApplyDamage(Target, Amount, nullptr, nullptr, InevitableDamageType);
}

void UMyCheatManager::SetPhase(const int32 Phase) const
{
	if (const auto Target = ABaseCharacter::GetMyPlayerCharacter(this)->TargetSystemComponent->TargetetActor)
		if (const auto BossComponent = Cast<UBossComponent>(Target->GetComponentByClass(UBossComponent::StaticClass())))
			BossComponent->SetPhase(Phase);
}

void UMyCheatManager::KillAll() const
{
	if(const auto AIManager = UAIManager::GetAIManager(this))
		for(int32 i = AIManager->RegisteredAIs.Num() - 1; i >= 0; i-- )
			UGameplayStatics::ApplyDamage(AIManager->RegisteredAIs[i]->GetPawn(), 99999999.0f, nullptr, nullptr, InevitableDamageType);
}

void UMyCheatManager::AddDrinkCharge(const float Amount) const
{
	if(const auto AbilitySystem = UMyBlueprintFunctionLibrary::GetPlayerAbilitySystemComponent(this))
		UMyBlueprintFunctionLibrary::ApplyEffectWithMagnitudeByCaller(AbilitySystem, AddDrinkChargeEffect, SetByCallerDrinkChargeTag, Amount);
}

void UMyCheatManager::SpendDrinkCharge(const float Amount) const
{
	if(const auto AbilitySystem = UMyBlueprintFunctionLibrary::GetPlayerAbilitySystemComponent(this))
		UMyBlueprintFunctionLibrary::ApplyEffectWithMagnitudeByCaller(AbilitySystem, AddDrinkChargeEffect, SetByCallerDrinkChargeTag, -Amount);
}

void UMyCheatManager::AddGold(const float Amount) const
{
	if(const auto AbilitySystem = UMyBlueprintFunctionLibrary::GetPlayerAbilitySystemComponent(this))
		UMyBlueprintFunctionLibrary::ApplyEffectWithMagnitudeByCaller(AbilitySystem, AddGoldEffect, SetByCallerGoldTag, Amount);
}

void UMyCheatManager::SpendGold(const float Amount) const
{
	if(const auto AbilitySystem = UMyBlueprintFunctionLibrary::GetPlayerAbilitySystemComponent(this))
		UMyBlueprintFunctionLibrary::ApplyEffectWithMagnitudeByCaller(AbilitySystem, AddGoldEffect, SetByCallerGoldTag, -Amount);
}

void UMyCheatManager::TakeDamage(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
		UGameplayStatics::ApplyDamage(Player, Amount, nullptr, nullptr, InevitableDamageType);
}

void UMyCheatManager::DamageMultiplierAI(const float Multiplier) const
{
	if(const auto GameConfigComponent = UGameConfigComponent::GetGameConfigComponent(this))
		GameConfigComponent->DamageMultiplierAI = Multiplier;
}

void UMyCheatManager::DamageMultiplierPlayer(const float Multiplier) const
{
	if(const auto GameConfigComponent = UGameConfigComponent::GetGameConfigComponent(this))
		GameConfigComponent->DamageMultiplierPlayer = Multiplier;
}

void UMyCheatManager::RestoreAll() const
{
	RestoreAllHealth();
	RestoreAllMana();
	RestoreAllStamina();
}

void UMyCheatManager::SaveGame() const
{
	if(const auto SaveComponent = USaveComponent::GetSaveComponent(this))
		SaveComponent->SaveGame(FName(*UGameplayStatics::GetCurrentLevelName(this)));
}

void UMyCheatManager::LoadGame() const
{
	if(const auto SaveComponent = USaveComponent::GetSaveComponent(this))
		SaveComponent->LoadGame();
}

void UMyCheatManager::RestoreHealth(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreHealthEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::RestoreAllHealth() const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreHealthEffect.GetDefaultObject(), 999999.0f, EffectContext);
	}
}

void UMyCheatManager::IncreaseMaxHealth(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(ChangeMaxHealthEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::DecreaseMaxHealth(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(ChangeMaxHealthEffect.GetDefaultObject(), -Amount, EffectContext);
	}
}

void UMyCheatManager::InfiniteMana() const
{
	const FGameplayEffectContextHandle EffectContext;
	const auto Player = ABaseCharacter::GetMyPlayerCharacter(this);
	if(!Player)
		return;
	
	bool bFound;
	const float Multiplier = UAbilitySystemBlueprintLibrary::GetFloatAttribute(Player, UMyAttributeSet::GetManaLossMultiplierAttribute(), bFound);
	if (Multiplier == 0.0f)        
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(DefaultManaConsumptionEffect.GetDefaultObject(), 1.0f, EffectContext);
	else
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(InfiniteManaEffect.GetDefaultObject(), 1.0f, EffectContext);
}

void UMyCheatManager::RestoreMana(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreManaEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::ConsumeMana(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(LoseManaEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::ConsumeAllMana() const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(LoseManaEffect.GetDefaultObject(), 999999.0f, EffectContext);
	}
}

void UMyCheatManager::RestoreAllMana() const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreManaEffect.GetDefaultObject(), 999999.0f, EffectContext);
	}
}

void UMyCheatManager::IncreaseMaxMana(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(ChangeMaxManaEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::DecreaseMaxMana(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(ChangeMaxManaEffect.GetDefaultObject(), -Amount, EffectContext);
	}
}

void UMyCheatManager::InfiniteStamina() const
{
	const FGameplayEffectContextHandle EffectContext;
	const auto Player = ABaseCharacter::GetMyPlayerCharacter(this);
	if(!Player)
		return;
	
	bool bFound;
	const float Multiplier = UAbilitySystemBlueprintLibrary::GetFloatAttribute(Player, UMyAttributeSet::GetStaminaLossMultiplierAttribute(), bFound);
	if (Multiplier == 0.0f)
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(DefaultStaminaConsumptionEffect.GetDefaultObject(), 1.0f, EffectContext);
	else
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(InfiniteStaminaEffect.GetDefaultObject(), 1.0f, EffectContext);
}

void UMyCheatManager::RestoreStamina(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreStaminaEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::ConsumeStamina(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(LoseStaminaEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::ConsumeAllStamina() const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(LoseStaminaEffect.GetDefaultObject(), 999999.0f, EffectContext);
	}
}

void UMyCheatManager::RestoreAllStamina() const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreStaminaEffect.GetDefaultObject(), 999999.0f, EffectContext);
	}
}

void UMyCheatManager::IncreaseMaxStamina(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(ChangeMaxStaminaEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::DecreaseMaxStamina(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(ChangeMaxStaminaEffect.GetDefaultObject(), -Amount, EffectContext);
	}
}

void UMyCheatManager::God()
{
	Super::God();

	Unstaggerable();
	InfiniteStamina();
	InfiniteMana();
}

void UMyCheatManager::AddSkillByClass(const TSubclassOf<UGameplayEffect> SkillToAdd) const
{
	auto AbilitiesGrantedByEffect = UMyBlueprintFunctionLibrary::GetAbilitiesClassesGrantedByEffect(SkillToAdd);
	if(AbilitiesGrantedByEffect.Num() == 0)
		return;

	if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
		if(const auto PlayerCastingComponent = Cast<UPlayerCastingComponent>(Player->GetComponentByClass(UPlayerCastingComponent::StaticClass())))
		{
			const FGameplayEffectContextHandle EffectContext;
			Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(SkillToAdd.GetDefaultObject(), 1.0f, EffectContext);
			PlayerCastingComponent->AddAbility(AbilitiesGrantedByEffect[0]);
		}
}

void UMyCheatManager::RemoveSkillByClass(const TSubclassOf<UGameplayEffect> SkillToRemove) const
{
	auto AbilitiesGrantedByEffect = UMyBlueprintFunctionLibrary::GetAbilitiesClassesGrantedByEffect(SkillToRemove);
	if(AbilitiesGrantedByEffect.Num() == 0)
		return;

	if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
		if(const auto PlayerCastingComponent = Cast<UPlayerCastingComponent>(Player->GetComponentByClass(UPlayerCastingComponent::StaticClass())))
		{
			const FGameplayEffectContextHandle EffectContext;
			Player->AbilitySystemComponent->RemoveActiveGameplayEffectBySourceEffect(SkillToRemove, nullptr);
			PlayerCastingComponent->RemoveAbility(AbilitiesGrantedByEffect[0]);
		}
}
