// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Vignettes.h"

#include "MyHUD.h"

UVignettes* UVignettes::GetVignettes(const UObject* WorldContextObject)
{
	if(const auto MyHud = AMyHUD::GetMyHud(WorldContextObject))
		return MyHud->Vignettes;

	return nullptr;
}
