// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/ChoiceWidget.h"
#include "MyBlueprintFunctionLibrary.h"
#include "MyAbilitySystemComponent.h"

void UChoiceWidget::SetupPossibleUpgrades()
{
	for(const auto DataTable : PossibleUpgradesDataTables)
	{
		const auto PlayerAbilitySystem = UMyBlueprintFunctionLibrary::GetPlayerAbilitySystemComponent(this);
		TArray<FUpgradeChoice*> Rows;
		const TCHAR* ContextString = nullptr;
		DataTable->GetAllRows<FUpgradeChoice>(ContextString, Rows);
		for(const auto Row : Rows)
		{
			if(!Row->bCanBeLearnedFromScroll)
				continue;

			//Check if required tags are applied
			if(!PlayerAbilitySystem->HasAllMatchingGameplayTags(Row->RequiredTags))
				continue;
				
			//Don't allow to add the same upgrade twice
			if(!PlayerAbilitySystem->GetGameplayEffectCount(Row->EffectToApply, nullptr, false))
				PossibleUpgrades.Add(*Row);
		}
	}
}
