// Fill out your copyright notice in the Description page of Project Settings.


#include "HudWidget.h"

#include "MyHUD.h"

UHudWidget* UHudWidget::GetHudWidget(const UObject* WorldContextObject)
{
	if(const auto MyHud = AMyHUD::GetMyHud(WorldContextObject))
		return MyHud->HudWidget;

	return nullptr;
}
