// Fill out your copyright notice in the Description page of Project Settings.

#include "MyHUD.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Info3D.h"

AMyHUD* AMyHUD::GetMyHud(const UObject* WorldContextObject)
{
	if (const auto PlayerController = UGameplayStatics::GetPlayerController(WorldContextObject, 0))
		return Cast<AMyHUD>(PlayerController->GetHUD());

	return nullptr;
}

void AMyHUD::ShowDamageInfo(const FVector Location, const FText& Text, const FLinearColor Color, const bool bIsCritical)
{
	if (this)
		if (const auto Info3D = GetWorld()->SpawnActor<AInfo3D>(DamageInfoClass, Location, FRotator()))
			Info3D->ShowInfo(Text, Color, bIsCritical);
}

void AMyHUD::ShowGoldCollectedInfo(const FVector Location, const float Amount)
{
	if (this)
		if (const auto Info3D = GetWorld()->SpawnActor<AInfo3D>(Info3DClass, Location, FRotator()))
			Info3D->ShowInfo(FText::FromString(FString::FromInt(Amount)), GoldColor);
}

void AMyHUD::ShowInfo(const FVector Location, const FText& Text, const FLinearColor Color /*= FLinearColor::White*/)
{
	if (this)
		if (const auto Info3D = GetWorld()->SpawnActor<AInfo3D>(Info3DClass, Location, FRotator()))
			Info3D->ShowInfo(Text, Color);
}

