// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "GameplayTagContainer.h"
#include "MyDamageType.generated.h"

enum class EAttackStaggerStrength : uint8;
UCLASS()
class DUNGEONCRAWLER_API UMyDamageType : public UDamageType
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Responses)
	FLinearColor UIColor = FLinearColor::White;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Responses)
	FGameplayTag HitTag;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Responses)
	EAttackStaggerStrength HitStaggerStrength;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Responses)
	FGameplayTagContainer DamageTags;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Responses)
	bool bCanShowNumbers = true;
	
};
