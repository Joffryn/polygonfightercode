// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameplayTagContainer.h"
#include "GameplayEffectTypes.h"
#include "MyGameplayStatics.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UMyGameplayStatics : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = GameplayCues)
	static void ExecuteGameplayCue(UAbilitySystemComponent* AbilitySystem, const  FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);

	UFUNCTION(BlueprintCallable, Category = GameplayCues)
	static void RemoveGameplayCue(UAbilitySystemComponent* AbilitySystem, const  FGameplayTag GameplayCueTag);

	UFUNCTION(BlueprintCallable, Category = GameplayCues)
	static void AddGameplayCue(UAbilitySystemComponent* AbilitySystem, const  FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);
	
	UFUNCTION(BlueprintCallable, meta=(WorldContext="WorldContextObject", AdvancedDisplay = "2", DisplayName = "Travel To Level (by Name)"), Category="Game")
	static void TravelToLevel(const UObject* WorldContextObject, FName LevelName, bool bAbsolute = true, FString Options = FString(TEXT("")));
	
	UFUNCTION(BlueprintCallable, Category = Audio)
	static void SetAudioAssetSoundClass(USoundBase* Sound, USoundClass* InSoundClass);

	UFUNCTION(BlueprintCallable, Category = Audio)
	static USoundClass* GetAudioAssetSoundClass(USoundBase* Sound);
	
	UFUNCTION(BlueprintPure, Category = Color)
	static FLinearColor GetAverageColor(TArray<FLinearColor> Colors);

	UFUNCTION(BlueprintPure, Category = Mesh)
	static TArray<UMeshComponent*> GetActorMeshes(AActor* Actor);
	
	UFUNCTION(BlueprintCallable, Category = "Ability")
	static FGameplayEffectContextHandle MakeGameplayEffectContextWithParams(UAbilitySystemComponent* AbilitySystemComponent, AActor* Instigator, AActor* Causer);
	
};
