// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class EKatanaStance : uint8
{
	High,
	Medium,
	Low
};

UENUM(BlueprintType)
enum class EUpgradeType : uint8
{
	Active,
	Passive,
	Buff
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	TwoHanded,
	Katana,
	OneHandedAndShield,
	Dual,
	Assassin,
	Shield,
	Spear,
	BodyPart,
	Giant,
	Greatsword,
	OneHanded,
	OrientalSpear
};

UENUM(BlueprintType)
enum class EAttackReply : uint8
{
	Hit,
	Crit,
	Block,
	Dodge,
	Parry
};

UENUM(BlueprintType)
enum class EWeaponSocket : uint8
{
	RightHand,
	LeftHand,
	RightLeg,
	LeftLeg,
	Secondary
};

UENUM(BlueprintType)
enum class EStat : uint8
{
	Health,
	MaxHealth,
	Mana,
	MaxMana,
	Stamina,
	MaxStamina
};

UENUM(BlueprintType)
enum class EAttackStaggerStrength : uint8
{
	None,
	Light,
	Medium,
	Heavy,
	UltraHeavy
};

UENUM(BlueprintType)
enum class EPoiseLevel : uint8
{
	None,
	Light,
	Medium,
	Heavy,
	UltraHeavy,
	Unstaggerable
};

UENUM(BlueprintType)
enum class EStaggerReaction : uint8
{
	None,
	Hit,
	HeavyHit,
	Stun,
	Knockdown
};

UENUM(BlueprintType)
enum class EAttackResponse : uint8
{
	Hit,
	Crit,
	Block,
	Parry,
	InvincibleHit
};

