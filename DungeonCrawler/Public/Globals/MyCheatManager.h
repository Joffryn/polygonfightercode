// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyDamageType.h"
#include "GameFramework/CheatManager.h"
#include "MyCheatManager.generated.h"

class UGameplayEffect;

UCLASS()
class DUNGEONCRAWLER_API UMyCheatManager : public UCheatManager
{
	GENERATED_BODY()
	
public:

	UFUNCTION(exec, Category = Gameplay)
	void FriendlyFire() const;

	UFUNCTION(exec, Category = Poise)
	void ChangePoiseLevel(const int32 Level) const;

	UFUNCTION(exec, Category = Poise)
	void Unstaggerable() const;
	
	UFUNCTION(exec, Category = Damage)
	void Suicide() const;

	UFUNCTION(exec, Category = Damage)
	void DebugMenu() const;

	UFUNCTION(exec, Category = Damage)
	void AICombatDebug() const;

	UFUNCTION(exec, Category = Damage)
	void AIManagerDebug() const;

	UFUNCTION(exec, Category = Damage)
	void MusicManagerDebug() const;
	
	UFUNCTION(exec, Category = Damage)
	void Kill() const;

	UFUNCTION(exec, Category = Weapon)
	void EquipWeapon(FName WeaponName) const;
	
	UFUNCTION(exec, Category = Damage)
	void Damage(const float Amount) const;

	UFUNCTION(exec, Category = Damage)
	void KillAll() const;

	UFUNCTION(exec, Category = Boss)
	void SetPhase(const int32 Phase) const;
	
	UFUNCTION(exec, Category = Drink)
	void AddDrinkCharge(const float Amount) const;

	UFUNCTION(exec, Category = Drink)
	void SpendDrinkCharge(const float Amount) const;
	
	UFUNCTION(exec, Category = Gold)
	void AddGold(const float Amount) const;

	UFUNCTION(exec, Category = Gold)
	void SpendGold(const float Amount) const;
	
	UFUNCTION(exec, Category = Damage)
	void TakeDamage(const float Amount) const;

	UFUNCTION(exec, Category = Damage)
	void DamageMultiplierAI(const float Multiplier) const;

	UFUNCTION(exec, Category = Damage)
	void DamageMultiplierPlayer(const float Multiplier) const;
	

	UFUNCTION(BlueprintImplementableEvent, exec, Category = Hero)
	void SetHeroClass(FName HeroClassName) const;

	UFUNCTION(BlueprintImplementableEvent, exec, Category = Hero)
	void Spawn(FName EnemyName) const;
	
	
	UFUNCTION(exec, Category = Upgrades)
	void AddPerk(FName PerkName) const;
	
	UFUNCTION(exec, Category = Upgrades)
	void AddAllPerks() const;

	UFUNCTION(exec, Category = Upgrades)
	void RemovePerk(FName PerkName) const;
	
	UFUNCTION(exec, Category = Upgrades)
	void RemoveAllPerks() const;

	UFUNCTION(exec, Category = Upgrades)
	void RemoveSkill(FName SkillName) const;
	
	UFUNCTION(exec, Category = Upgrades)
	void RemoveAllSkills() const;
	
	UFUNCTION(exec, Category = Upgrades)
	void AddSkill(FName SkillName) const;
	
	UFUNCTION(exec, Category = Upgrades)
	void AddAllSkills() const;


	UFUNCTION(exec, Category = Stats)
	void RestoreAll() const;


	UFUNCTION(exec, Category = Saving)
	void SaveGame() const;
	
	UFUNCTION(exec, Category = Saving)
	void LoadGame() const;

	
	UFUNCTION(exec, Category = Health)
	void RestoreHealth(const float Amount) const;

	UFUNCTION(exec, Category = Health)
	void RestoreAllHealth() const;

	UFUNCTION(exec, Category = Health)
	void IncreaseMaxHealth(const float Amount) const;

	UFUNCTION(exec, Category = Health)
	void DecreaseMaxHealth(const float Amount) const;


	UFUNCTION(exec, Category = Mana)
	void InfiniteMana() const;

	UFUNCTION(exec, Category = Mana)
	void RestoreMana(const float Amount) const;

	UFUNCTION(exec, Category = Mana)
	void ConsumeMana(const float Amount) const;

	UFUNCTION(exec, Category = Mana)
	void ConsumeAllMana() const;

	UFUNCTION(exec, Category = Mana)
	void RestoreAllMana() const;

	UFUNCTION(exec, Category = Mana)
	void IncreaseMaxMana(const float Amount) const;

	UFUNCTION(exec, Category = Mana)
	void DecreaseMaxMana(const float Amount) const; 


	UFUNCTION(exec, Category = Mana)
	void InfiniteStamina() const;

	UFUNCTION(exec, Category = Stamina)
	void RestoreStamina(const float Amount) const;

	UFUNCTION(exec, Category = Stamina)
	void ConsumeStamina(const float Amount) const;

	UFUNCTION(exec, Category = Stamina)
	void ConsumeAllStamina() const;

	UFUNCTION(exec, Category = Stamina)
	void RestoreAllStamina() const;

	UFUNCTION(exec, Category = Stamina)
	void IncreaseMaxStamina(const float Amount) const;

	UFUNCTION(exec, Category = Stamina)
	void DecreaseMaxStamina(const float Amount) const;


	virtual void God() override;
	

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> AddDrinkChargeEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	FGameplayTag SetByCallerDrinkChargeTag;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> AddGoldEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	FGameplayTag SetByCallerGoldTag;

	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> InfiniteStaminaEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> DefaultStaminaConsumptionEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> InfiniteManaEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> DefaultManaConsumptionEffect;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> RestoreHealthEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UMyDamageType> InevitableDamageType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> ChangeMaxHealthEffect;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> RestoreManaEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> LoseManaEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> ChangeMaxManaEffect;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> RestoreStaminaEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> LoseStaminaEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<UGameplayEffect> ChangeMaxStaminaEffect;

private:

	void AddSkillByClass(TSubclassOf<class UGameplayEffect> SkillToAdd) const;

	void RemoveSkillByClass(TSubclassOf<class UGameplayEffect> SkillToAdd) const;
};
