// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "AttributeSet.h"
#include "GameplayEffect.h"
#include "MySaveGame.generated.h"

USTRUCT()
struct FGameplayEffectSaveData
{
	GENERATED_BODY()

	UPROPERTY()
	FGameplayEffectSpec EffectSpec;

	UPROPERTY()
	float EffectElapsedTime;

};

UCLASS()
class DUNGEONCRAWLER_API UMySaveGame : public USaveGame
{
	GENERATED_BODY()

public:

	UPROPERTY()
	FName LevelName;
	
	UPROPERTY()
	USkeletalMesh* Mesh;
	
	UPROPERTY()
	UMaterial* Material;
	
	UPROPERTY()
	TSubclassOf<class AWeapon> PrimaryWeaponClass;
	
	UPROPERTY()
	TSubclassOf<AWeapon> SecondaryWeaponClass;

	UPROPERTY()
	TSubclassOf<AWeapon> SecondaryLeftWeaponClass;
	
	UPROPERTY()
	TMap<FGameplayAttribute, float> Attributes;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	TArray<TSubclassOf<UGameplayAbility>> CastingAbilitiesClasses;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	TArray<TSubclassOf<UGameplayEffect>> CastingEffectsToApply;
	
	UPROPERTY()
	TArray< FGameplayEffectSaveData > UpgradeEffects;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Character)
	TMap<FGameplayTag, USoundBase*> SoundsToTagsMap;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	TArray<FName> HiddenBones;
	
};
