// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "CoreMinimal.h"
#include "BaseCharacter.h"
#include "Enums.h"
#include "GameplayEffect.h"
#include "MyAttributeSet.h"

#include "Structs.generated.h"

class UGameplayAbility;
class UAnimMontage;
class UTexture2D;

USTRUCT(BlueprintType)
struct FCharacterStatsSetup : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class ABaseCharacter> CharacterClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FGameplayAttribute, float> AttributesToValuesSetup;

	FCharacterStatsSetup()
	{
		//General stats important for everyone
		AttributesToValuesSetup.Add(UMyAttributeSet::GetMaxHealthAttribute(), 100.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetHealthAttribute(), 100.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetGoldAttribute(), 50.0f);

		//Right now only really important to the player
		AttributesToValuesSetup.Add(UMyAttributeSet::GetMaxStaminaAttribute(), 100.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetStaminaAttribute(), 100.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetMaxManaAttribute(), 100.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetManaAttribute(), 100.0f);
		
		//Mostly important just to the player
		AttributesToValuesSetup.Add(UMyAttributeSet::GetStaminaSprintCostAttribute(), 5.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetStaminaRegenAttribute(), 10.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetDrinkChargesAttribute(), 0.0f);

		//MSeveral miscellaneous multipliers
		AttributesToValuesSetup.Add(UMyAttributeSet::GetCritDamageMultiplierAttribute(), 1.5f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetBlockingStaminaCostMultiplierAttribute(), 1.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetDodgeStrengthMultiplierAttribute(), 1.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetDamageMultiplierAttribute(), 1.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetSpeedAttribute(), 1.0f);

		//Base stats multipliers
		AttributesToValuesSetup.Add(UMyAttributeSet::GetStaminaGainMultiplierAttribute(), 1.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetStaminaLossMultiplierAttribute(), 0.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetHealthGainMultiplierAttribute(), 1.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetHealthLossMultiplierAttribute(), 1.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetManaGainMultiplierAttribute(), 1.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetManaLossMultiplierAttribute(), 0.0f);
	};
};

USTRUCT(BlueprintType)
struct FStatusParams : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag StatusTag;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* Icon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor Color = FLinearColor::White;
	
};

USTRUCT(BlueprintType)
struct FDamageParams
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UDamageType> DamageClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Amount;
	
};

USTRUCT(BlueprintType)
struct FWeaponsSetup : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EWeaponType WeaponType;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class AWeapon> WeaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EAttackStaggerStrength BaseAttackStaggerStrength = EAttackStaggerStrength::Light;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FDamageParams> BaseDamageParams;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BaseStaminaCost = 10.0f;
	
};

USTRUCT(BlueprintType)
struct FAttackParams : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UAnimMontage* MontageToPlay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float StaminaCostMultiplier = 1.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 ExtraPoise = 1;

};

USTRUCT(BlueprintType)
struct FUpgradeChoice : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UGameplayEffect> EffectToApply;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* Icon;
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTagContainer RequiredTags;
        	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor Color = FLinearColor::White;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Title;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EUpgradeType Type = EUpgradeType::Passive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanBeLearnedFromScroll = true;
	
};

USTRUCT(BlueprintType)
struct FAIAttackParams : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DamageMultiplier = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PlayRate = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Range = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Weight = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UAnimMontage* MontageToPlay;

};

USTRUCT(BlueprintType)
struct FAttackStrengthToReactionSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<EAttackStaggerStrength, EStaggerReaction> AttackToReactionMap;

	FAttackStrengthToReactionSetup()
	{
		AttackToReactionMap.Add(EAttackStaggerStrength::None, EStaggerReaction::None);
		AttackToReactionMap.Add(EAttackStaggerStrength::Light, EStaggerReaction::Hit);
		AttackToReactionMap.Add(EAttackStaggerStrength::Medium, EStaggerReaction::HeavyHit);
		AttackToReactionMap.Add(EAttackStaggerStrength::Heavy, EStaggerReaction::Stun);
		AttackToReactionMap.Add(EAttackStaggerStrength::UltraHeavy, EStaggerReaction::Knockdown);
	}
};

USTRUCT(BlueprintType)
struct FPoiseToStaggerSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<EPoiseLevel, FAttackStrengthToReactionSetup> PoiseLevelToStaggerSetupMap;

	FPoiseToStaggerSetup()
	{
		PoiseLevelToStaggerSetupMap.Add(EPoiseLevel::None, FAttackStrengthToReactionSetup());
		PoiseLevelToStaggerSetupMap.Add(EPoiseLevel::Light, FAttackStrengthToReactionSetup());
		PoiseLevelToStaggerSetupMap.Add(EPoiseLevel::Medium, FAttackStrengthToReactionSetup());
		PoiseLevelToStaggerSetupMap.Add(EPoiseLevel::Heavy, FAttackStrengthToReactionSetup());
		PoiseLevelToStaggerSetupMap.Add(EPoiseLevel::UltraHeavy, FAttackStrengthToReactionSetup());
		PoiseLevelToStaggerSetupMap.Add(EPoiseLevel::Unstaggerable, FAttackStrengthToReactionSetup());
	}
};
