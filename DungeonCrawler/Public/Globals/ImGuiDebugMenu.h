// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ImGuiCommon.h"
#include "CoreMinimal.h"

class AMyPlayerController;

class DUNGEONCRAWLER_API ImGuiDebugMenu
{
#if WITH_IMGUI
public:
	ImGuiDebugMenu();
	
	void Render(AMyPlayerController* PlayerController);

	struct FAICombatDebugger
	{
		bool bEnabled = false;
		void Render(const AMyPlayerController* PlayerController);
	};

	struct FMusicManagerDebugger
	{
		bool bEnabled = false;
		void Render(const AMyPlayerController* PlayerController);
	};

	struct FAIManagerDebugger
	{
		bool bEnabled = false;
		void Render(const AMyPlayerController* PlayerController);
	};
	
	FAICombatDebugger AICombatDebugger;
	FMusicManagerDebugger MusicManagerDebugger;
	FAIManagerDebugger AIManagerDebugger;
	
#endif
};

