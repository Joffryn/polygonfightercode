// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ImGuiCommon.h"
#include "MyPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPossess, APawn*, Pawn);

UCLASS()
class DUNGEONCRAWLER_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static AMyPlayerController* GetMyPlayerController(const UObject* WorldContextObject);
	
	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FOnPossess OnPossesDelegate;
	
	virtual void Tick(float DeltaTime) override;
	
	virtual void OnPossess(APawn* InPawn) override;
	
	#if WITH_IMGUI
	class ImGuiDebugMenu* DebugMenu; 
	#endif
	
};
