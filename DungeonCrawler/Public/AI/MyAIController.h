// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "CoreMinimal.h"
#include "MyAIController.generated.h"

UCLASS()
class DUNGEONCRAWLER_API AMyAIController : public AAIController
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintCallable)
	void SetNewTarget(ACharacter* Target);
	
	AMyAIController();

	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;
	
private:

	UFUNCTION()
	void OnPossessedPawnDeath();
	
	UPROPERTY()
	ACharacter* LastAwareTarget;
	
};
