// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTaskNode_ExecuteAbilityByTag.generated.h"

class UGameplayAbility;
UCLASS()
class DUNGEONCRAWLER_API UBTTaskNode_ExecuteAbilityByTag : public UBTTaskNode
{
	GENERATED_BODY()
	UBTTaskNode_ExecuteAbilityByTag();

	UPROPERTY(EditAnywhere, Category = Abilities)
	 FGameplayTag GameplayTag;

	UPROPERTY(EditAnywhere, Category = Abilities)
	bool bCancelAbilityOnAbort;

	UPROPERTY(EditAnywhere, Category = Abilities)
	bool bIgnoreTargetCheck;

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	virtual FString GetStaticDescription() const override;

	UPROPERTY()
	class UMyAbilitySystemComponent* MyAbilitySystemComponent;
	
	FDelegateHandle OnAbilityEndedHandle;
	FDelegateHandle OnAbilityFailedHandle;

protected:
	virtual EBTNodeResult::Type AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

public:
	UFUNCTION()
	void OnAbilityEnded(UGameplayAbility* GameplayAbility, UBehaviorTreeComponent* OwnerComp);

	UFUNCTION()
	void OnAbilityFailed(const UGameplayAbility* GameplayAbility, const FGameplayTagContainer& GameplayTags, UBehaviorTreeComponent* OwnerComp);
};
