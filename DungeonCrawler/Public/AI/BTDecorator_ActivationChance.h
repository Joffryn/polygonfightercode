// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTDecorator_ActivationChance.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UBTDecorator_ActivationChance : public UBTDecorator
{
	GENERATED_BODY()

public:

	UBTDecorator_ActivationChance();

	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

	UPROPERTY(EditAnywhere, Category = "Settings", meta = (ClampMax = "1.0", ClampMin = "0.0"))//, UIMax = "0.0", UIMin = "1.0"))
	float ChanceToActivate = 1.0;

};
