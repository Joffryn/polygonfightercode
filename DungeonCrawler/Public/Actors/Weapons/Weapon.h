// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "Enums.h"
#include "Weapon.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAttackStarted);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAttackFinished);

UCLASS()
class DUNGEONCRAWLER_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:
	
	AWeapon();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USoundsComponent* SoundsComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attachment")
	FName EquipSocket;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attachment")
	FName SheathSocket;

	//Optional variable for weapons like dual wielding or one handed and shield
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	TSubclassOf<AWeapon> SecondWeaponClass;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attachment")
	FGameplayTag WeaponHitTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attachment")
	FVector LocationCorrection;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attachment")
	FRotator RotationCorrection;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat")
	UAnimMontage* BlockedMontage;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat")
	FAttackParams ParryParams;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat")
	UAnimMontage* EquipMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat")
	UAnimMontage* UnequipMontage;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Movement")
	UBlendSpaceBase* MovementBlendspace;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Movement")
	class UBlendSpace1D* RollingBlendspace;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Movement")
	UBlendSpaceBase* GuardBlendspace;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Movement")
	UAnimSequence* GuardAnimation;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Movement")
	UAnimSequence* IdleAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Reactions")
	UAnimMontage* KnockBackMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Reactions")
	TArray<UAnimMontage*> KnockDownMontages;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat | AI")
	TArray<UAnimMontage*> TauntMontages;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat | AI")
	TArray<UAnimMontage*> FlexMontages;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat | AI")
	TArray<FAttackParams> MeleeAttacksParams; 

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat | AI")
	TArray<FAttackParams> DistanceCloseAttackParams;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat")
	TArray<FAIAttackParams> CastsParams;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat")
	TArray<FAttackParams> RangedAttacksParams;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat")
	TArray<FAttackParams> LightAttacksParams;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat")
	TArray<FAttackParams> HeavyAttacksParams;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat")
	FAttackParams SpecialAttackParams;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat")
	FAttackParams DodgeAttackParams;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat")
	FAttackParams SprintAttackParams;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Combat")
	FAttackParams CastAttackParams;
	

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Reactions")
	TArray<UAnimMontage*> BeingHitMontages;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Animations | Reactions")
	TArray<UAnimMontage*> DyingMontages;
	
	
	 UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	 FOnAttackStarted OnAttackStarted;
	
	 UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	 FOnAttackStarted OnAttackFinished;


	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EWeaponType WeaponType;

	
	UPROPERTY(Transient, BlueprintReadOnly)
	FAttackParams CurrentAttackParams;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly = "Effects")
	TSubclassOf<UGameplayEffect> EquippedGameplayEffectClass;

	UPROPERTY()
	TSubclassOf<UDamageType> DamageType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	UTexture2D* Icon;
	
	UPROPERTY(BlueprintReadWrite, Category = "Stamina")
	float BaseStaminaCost = 10.0f;

	
	UFUNCTION(BlueprintCallable)
	void SetCurrentAttackParams(FAttackParams NewAttackParams);
	
	UFUNCTION(BlueprintCallable)
	virtual void AttackStarted();

	UFUNCTION(BlueprintCallable)
	virtual void HideWeapon();

	UFUNCTION(BlueprintCallable)
	virtual void ShowWeapon();
	
	UFUNCTION(BlueprintCallable)
	virtual void AttackFinished();
	
	ECollisionEnabled::Type CollisionEnabledCache;
	
};
