// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "TrailInterface.h"
#include "Weapon.h"
#include "MeleeWeapon.generated.h"
class AMeleeWeapon;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponDamageDealingStarted, AMeleeWeapon*, Weapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponDamageDealingFinished, AMeleeWeapon*, Weapon);


UCLASS(Abstract)
class DUNGEONCRAWLER_API AMeleeWeapon : public AWeapon, public ITrailInterface
{
	GENERATED_BODY()
	
public:

	AMeleeWeapon();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UWeaponCollision* WeaponCollision;

	//Some of the weapons have secondary collision(for example hilt of the Axe), which should perform Traces but shouldn't make trails
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UBoxComponent* SecondaryWeaponCollision;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UTrailComponent* TrailComponent;

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FOnWeaponDamageDealingStarted OnWeaponDamageDealingStarted;

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FOnWeaponDamageDealingFinished OnWeaponDamageDealingFinished;


	UPROPERTY(EditDefaultsOnly, Category = "Config | Debug")
	bool bShowDebug;

	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	bool bIsDealingDamage;

	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	bool bNextAttackWillAlwaysCrit;

	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	TArray<FDamageParams> BaseDamageParams;
	
	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	TArray<FDamageParams> CurrentDamageParams;

	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	TSubclassOf<UDamageType> UnblockableDamageType;
	
	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	bool bIsUnblockable;
	
	UPROPERTY(BlueprintReadWrite, Category = "Stagger")
	EAttackStaggerStrength BaseAttackStaggerStrength = EAttackStaggerStrength::Light;

	UPROPERTY(BlueprintReadWrite, Category = "Stagger")
	EAttackStaggerStrength CurrentAttackStaggerStrength;

	UFUNCTION(BlueprintCallable)
	virtual void OnActorOverlapped(AActor* Actor, const FHitResult& HitResult);

	UFUNCTION(BlueprintCallable)
	void ClearHitActors();

	UFUNCTION(BlueprintCallable)
	void PlayHitSound() const;

	// Called every frame
	virtual void Tick(const float DeltaTime) override;

	virtual void BeginPlay() override;

	virtual UStaticMeshComponent*  GetStaticMesh_Implementation() override;

	UFUNCTION(BlueprintCallable)	
    void CalculateWeaponDamage(float DamageMultiplier, int32 ExtraAttackStaggerStrength);
	
	UFUNCTION(BlueprintImplementableEvent ,BlueprintCallable)
	void SetUnblockableWeaponMode(bool bNewState);
	
protected:

	UPROPERTY(EditDefaultsOnly, Category = Sockets)
	FName CollisionStartSocketName;

	UPROPERTY(EditDefaultsOnly, Category = Sockets)
	FName CollisionEndSocketName;

	UPROPERTY(EditDefaultsOnly, Category = Sockets)
	FName SecondaryCollisionStartSocketName;

	UPROPERTY(EditDefaultsOnly, Category = Sockets)
	FName SecondaryCollisionEndSocketName;

	virtual void HideWeapon() override;
	
	virtual void ShowWeapon() override;

private:

	DECLARE_DYNAMIC_MULTICAST_SPARSE_DELEGATE_SixParams(FComponentBeginOverlapSignature, UPrimitiveComponent, OnComponentBeginOverlap, UPrimitiveComponent*, OverlappedComponent, AActor*, OtherActor, UPrimitiveComponent*, OtherComp, int32, OtherBodyIndex, bool, bFromSweep, const FHitResult&, SweepResult);

	UFUNCTION()
	void OnCollisionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	void TraceWeaponCollision();
	
	UPROPERTY(Transient)
	TArray<AActor*> ActorsHitInCurrentAttack;

	void LoadConfigFromDataTable();
	
};
