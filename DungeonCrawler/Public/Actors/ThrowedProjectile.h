// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/Projectile.h"
#include "ThrowedProjectile.generated.h"

UCLASS()
class DUNGEONCRAWLER_API AThrowedProjectile : public AProjectile
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn))
	bool bIsFake;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn))
	FVector InitialVelocity;

	virtual void BeginPlay() override;

};
