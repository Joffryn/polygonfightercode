// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnProjectileHit, FHitResult, HitResult, float, Damage);

UCLASS()
class DUNGEONCRAWLER_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	AProjectile();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UMyProjectileMovementComponent* ProjectileMovementComponent;

	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable)
	FOnProjectileHit OnProjectileHit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	float Damage;

protected:
	virtual void BeginPlay() override;

public:	

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SimulatePhysics();

	UFUNCTION(BlueprintCallable)
	void IgnoreOwnerWhileMoving() const;

	UFUNCTION(BlueprintCallable)
	void ClearTrails();

	UFUNCTION(BlueprintPure, BlueprintImplementableEvent)
	UStaticMeshComponent* GetProjectileMesh();

};
