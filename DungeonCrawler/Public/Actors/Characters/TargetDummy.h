// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AbilitySystemInterface.h"
#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "TargetDummy.generated.h"

UCLASS(Blueprintable, BlueprintType)
class DUNGEONCRAWLER_API ATargetDummy : public AActor, public IAbilitySystemInterface
{
	GENERATED_BODY()
	
public:	
	ATargetDummy();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UMyAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UMyAttributeSet* AttributeSet;

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	virtual float InternalTakePointDamage(const float Damage, FPointDamageEvent const& PointDamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
};
