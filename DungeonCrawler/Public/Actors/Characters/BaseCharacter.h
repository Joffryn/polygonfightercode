// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Perception/AIPerceptionListenerInterface.h"
#include "GenericTeamAgentInterface.h"
#include "GameplayTagAssetInterface.h"
#include "AbilitySystemInterface.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "InteractableInterface.h"

#include "BaseCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTargetSet, AActor*, Target);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);

UCLASS()
class DUNGEONCRAWLER_API ABaseCharacter : public ACharacter, public IAbilitySystemInterface, public IGameplayTagAssetInterface, public IGenericTeamAgentInterface, public IAIPerceptionListenerInterface, public IInteractableInterface
{
	GENERATED_BODY()

public:

	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnTargetSet OnTargetSet;
	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnDeath OnDeath;

	// Sets default values for this character's properties
	explicit ABaseCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static ABaseCharacter* GetMyPlayerCharacter(const UObject* WorldContextObject);

	virtual void PostInitializeComponents() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UMyAbilitySystemComponent* AbilitySystemComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UAIPerceptionComponent* PerceptionComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UTargetSystemComponent* TargetSystemComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class URotationComponent* RotationComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UCombatComponent* CombatComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USoundsComponent* SoundsComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UPoiseComponent* PoiseComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStatsComponent* StatsComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UDamageResponseComponent* DamageResponseComponent;
	
	UFUNCTION(BlueprintPure, Category = Getters)
	class UMyCharacterMovementComponent* GetMyCharacterMovementComponent() const ;

	UPROPERTY(EditAnywhere, Category = "System Effects")
	TArray<TSubclassOf<class UGameplayEffect>> DefaultInfiniteEffects;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Team)
	FGenericTeamId TeamID; 

	
	//Shouldn't really be here
	UPROPERTY(Transient, BlueprintReadWrite, Category = MotionSkills)
	bool bRollRequested;
	
	UPROPERTY(Transient, BlueprintReadWrite, Category = MotionSkills)
	float MotionSkillDirection;

	UPROPERTY(Transient, BlueprintReadWrite, Category = Anims)
	float Base;

	UPROPERTY(Transient, BlueprintReadWrite, Category = Anims)
	TArray<FName> HiddenBones;
	
	UFUNCTION(BlueprintCallable)
	void HideBones(TArray<FName> BonesToHide);
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Damage)
	void ForceDeath();

	UFUNCTION(BlueprintPure)
	bool IsAlive() const;

	UFUNCTION(BlueprintCallable)
	void SendTeamEvent(AActor* Target, float EventRange = 1000.0f);

	//IGenericTeamAgentInterface
	virtual void SetGenericTeamId(const FGenericTeamId& NewTeamID) override;
	
	virtual FGenericTeamId GetGenericTeamId() const override; 

	UFUNCTION(BlueprintPure)
	ETeamAttitude::Type GetTeamAttitudeTowardsActor(const AActor* Target) const;
	//End of IGenericTeamAgentInterface
	
	UFUNCTION(BlueprintCallable)
	void SendTouchEvent(AActor* TouchReceiver);
	
protected:
	virtual void BeginPlay() override;

	virtual float InternalTakePointDamage(const float Damage, FPointDamageEvent const& PointDamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	virtual float InternalTakeRadialDamage(float Damage, FRadialDamageEvent const& RadialDamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
public:	

	//IGameplayTagAssetInterface
	virtual void GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const override;
	
	//IAIPerceptionListenerInterface
	virtual UAIPerceptionComponent* GetPerceptionComponent() override;

	//IGenericTeamAgentInterface
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UFUNCTION(BlueprintCallable)
	void SetAttributesFromSetup(TMap<FGameplayAttribute, float> AttributesToValuesSetup);
	
private:

	void LoadAtrributesFromDataTable();
	
};
