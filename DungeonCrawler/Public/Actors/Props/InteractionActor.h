// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Interfaces/InteractableInterface.h"
#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "InteractionActor.generated.h"

UCLASS()
class DUNGEONCRAWLER_API AInteractionActor : public AActor, public IInteractableInterface
{
	GENERATED_BODY()
	
};
