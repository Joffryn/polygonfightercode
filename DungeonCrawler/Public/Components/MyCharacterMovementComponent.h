// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "MyCharacterMovementComponent.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UMyCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite, Category = Input)
	float ForwardAxisValue;
	
	UPROPERTY(BlueprintReadWrite, Category = Input)
	float RightAxisValue;

	UFUNCTION(BlueprintPure, Category = "Getters")
	FVector GetFeetLocation() const;
	
};
