// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "PlayerFeedbackComponent.generated.h"

USTRUCT(BlueprintType)
struct FActionFeedbackSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shake)
	TSubclassOf< UCameraShakeBase> Shake;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ForceFeedback)
	UForceFeedbackEffect* ForceFeedback;
	
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundBase* Sound;

     //Could be replaced by a curve
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Slomo)
	float CustomTimeDilation = 1.0f;
	
    //Could be replaced by a curve
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Slomo)
	float SlomoTime = 0.0f;
	
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class DUNGEONCRAWLER_API UPlayerFeedbackComponent final : public UActorComponent
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnBeingHitFeedback;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnBeingCritFeedback;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnBeingBlockedFeedback;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnBeingParriedFeedback;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnDeathFeedback;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnHitFeedback;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnCritFeedback;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnBlockedFeedback;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnHitInvincibleFeedback;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnParriedFeedback;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnKillFeedback;
	
	
	UPROPERTY()
	class ABaseCharacter* CharacterOwner;
	
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void BindDelegates();

	UFUNCTION()
	void OnDeath();
	
	UFUNCTION()
	void OnKill(AActor* Target);
	
	UFUNCTION()
	void OnBeingHit();

	UFUNCTION()
	void OnBeingCrit();
	
	UFUNCTION()
	void OnBeingBlocked();

	UFUNCTION()
	void OnBeingParried();
	
	UFUNCTION()
	void OnAttackHit(); 

	UFUNCTION()
	void OnCastFailed();

	UFUNCTION()
	void OnAttackCrit(TSubclassOf<UDamageType> DamageType, float Amount);

	UFUNCTION()
	void OnAttackBlocked();

	UFUNCTION()
	void OnAttackParried(AActor* Actor, TSubclassOf<UDamageType> DamageType, float Amount);

	UFUNCTION()
	void OnAttackHitInvincible();

private:

	float CurrentCustomTimeDilation = 1.0f;
	FTimerHandle SlomoTimer;
	
	void PlayActionFeedback(const FActionFeedbackSetup& FeedbackSetup);
	
	void TryToApplySlomo(float Time, float Value);
	void OnSlomoEnded();

};
