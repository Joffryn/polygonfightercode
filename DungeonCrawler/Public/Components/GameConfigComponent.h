// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameConfigComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UGameConfigComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UGameConfigComponent* GetGameConfigComponent(const UObject* WorldContextObject);
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	bool bFriendlyFireEnabled;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	bool bFallingDamageEnabled;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = UI)
	bool bShowDamageNumbers;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = UI)
	bool bShowBaseActionsWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = UI)
	bool bShowGoldCount;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	bool bShowInvincibleHitCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animations)
	bool bBlendPerBoneWhenGuarding;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animations)
	bool bBlendPerBoneWhenUsingMotionSkill;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category =  Combat)
	float DamageMultiplierAI = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category =  Combat)
	float DamageMultiplierPlayer = 1.0f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Trail)
	UParticleSystem* BaseTrail;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Sound)
	USoundAttenuation* BaseAttenuation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Sound)
	USoundAttenuation* BaseLongAttenuation;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Trail)
	TSubclassOf<class AMeleeWeapon> RightLegWeaponClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Trail)
	TSubclassOf<AMeleeWeapon> LeftLegWeaponClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Trail)
	TSubclassOf<AMeleeWeapon> RightHandWeaponClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Trail)
	TSubclassOf<AMeleeWeapon> LeftHandWeaponClass;

};
