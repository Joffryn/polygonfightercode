// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Components/ActorComponent.h"
#include "LootComponent.generated.h"

UCLASS( ClassGroup=(Custom), Blueprintable)
class DUNGEONCRAWLER_API ULootComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	float MinGoldAmount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	float MaxGoldAmount;

	UPROPERTY(BlueprintReadWrite)
	float ActualGoldAmount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	USoundBase* LootingSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	TSubclassOf<class UGameplayEffect> AddGoldEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	FGameplayTag SetByCallerGoldTag;

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void Loot(AActor* Looter);

};
