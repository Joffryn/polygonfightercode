// Copyright 2018-2019 Mickael Daniel. All Rights Reserved.

#pragma once

#include "Components/ActorComponent.h"
#include "Components/WidgetComponent.h"
#include "CoreMinimal.h"
#include "TargetSystemComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FComponentOnTargetLockedOnOff, AActor*, TargetActor);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UTargetSystemComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UTargetSystemComponent();

	// The minimum distance to enable target locked on.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target System")
	float MinimumDistanceToEnable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target System")
	FVector LookAtPointLocationAdjustment;
	
	// The Widget Class to use when locked on Target. If not defined, will fallback to a Text-rendered widget with a single O character.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target System")
	TSubclassOf<UUserWidget> TargetLockedOnWidgetClass;

	// The Widget Draw Size for the Widget class to use when locked on Target.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target System")
	float TargetLockedOnWidgetDrawSize;

	// Whether or not the Target Lock On Widget indicator should be drawn and attached automatically.
	// When set to false, this allows you to manually draw the widget for further control on where you'd like it to appear.
	// OnTargetLockedOn and OnTargetLockedOff events can be used for this.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target System")
	bool ShouldDrawTargetLockedOnWidget;

	// The amount of time to break line of sight when actor gets behind an Object.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target System")
	float BreakLineOfSightDelay;

	// Lower this value is, easier it will be to switch new target on right or left.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target System")
	float StartRotatingThreshold;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target System")
	float MaxCameraLookAtPitch;
	
	// Whether or not the character rotation should be controlled when Target is locked on.
	// If true, it'll set the value of bUseControllerRotationYaw and bOrientationToMovement variables on Target locked on / off.
	// Set it to true if you want the character to rotate around the locked on target to enable you to setup strafe animations.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target System")
	bool ShouldControlRotationWhenLockedOn;

	// The Relative Location to apply on Target LockedOn Widget when attached to a target.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target System")
	FVector TargetLockedOnWidgetRelativeLocation;

	// The AActor Subclass to search for targetable Actors.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target System")
	TSubclassOf<AActor> TargetableActors;

	// Function to call to target a new actor.
	UFUNCTION(BlueprintCallable, Category = "Target System")
	void TargetActor();

	// Function to call to manually untarget.
	UFUNCTION(BlueprintCallable, Category = "Target System")
	void TargetLockOff();

	// Function to call to target a new actor.
	UFUNCTION(BlueprintCallable, Category = "Target System")
	void TargetActorWithAxisInput(const float AxisValue);

	// Called when a target is locked off, either if it is out of reach (based on MinimumDistanceToEnable) or behind an Object.
	UPROPERTY(Transient, BlueprintAssignable, Category = "Target System")
	FComponentOnTargetLockedOnOff OnTargetLockedOff;

	// Called when a target is locked on
	UPROPERTY(Transient, BlueprintAssignable, Category = "Target System")
	FComponentOnTargetLockedOnOff OnTargetLockedOn;

	UFUNCTION(BLueprintPure)
	FORCEINLINE  AActor* GetNearestTarget() const { return NearestTarget; }

	UFUNCTION(BLueprintPure)
	FORCEINLINE bool HasTarget() const { return NearestTarget != nullptr; }

	UFUNCTION(BlueprintCallable, Category = "Target System")
	void TargetLockOn(AActor* TargetToLockOn);

	UPROPERTY(BlueprintReadOnly)
	AActor* TargetetActor;

	UFUNCTION(BlueprintPure)
	FRotator GetControlRotationOnTarget() const;

private:

	float ClosestTargetDistance;
	bool TargetLocked;
	UPROPERTY(Transient)
	AActor* CharacterReference;
	UPROPERTY(Transient)
	APlayerController* PlayerController;
	UPROPERTY(Transient)
	AActor* NearestTarget;
	UPROPERTY(Transient)
	UWidgetComponent* TargetLockedOnWidgetComponent;
	FTimerHandle LineOfSightBreakTimerHandle;
	bool bIsBreakingLineOfSight;
	FTimerHandle SwitchingTargetTimerHandle;
	bool bIsSwitchingTarget;

	TArray<AActor*> GetAllActorsOfClass(const TSubclassOf<AActor> ActorClass) const;
	AActor* FindNearestTarget(TArray<AActor*> Actors) const;
	bool LineTrace(FHitResult& HitResult, const AActor* OtherActor, const TArray<AActor*> ActorsToIgnore = TArray<AActor*>()) const;
	bool LineTraceForActor(AActor* OtherActor, const TArray<AActor*> ActorsToIgnore = TArray<AActor*>()) const;
	void SetControlRotationOnTarget() const;
	void CreateAndAttachTargetLockedOnWidgetComponent(AActor* TargetActor);
	bool ShouldBreakLineOfSight() const;
	void BreakLineOfSight();
	bool IsInViewport(const AActor* TargetActor) const;


	float GetDistanceFromCharacter(const AActor* OtherActor) const;
	TArray<AActor*> FindTargetsInRange(TArray<AActor*> ActorsToLook, const float RangeMin, const float RangeMax) const;
	float GetAngleUsingCameraRotation(const AActor* ActorToLook) const;
	static FRotator FindLookAtRotation(const FVector Start, const FVector Target);
	void ResetIsSwitchingTarget();

	bool TargetIsTargetable(const AActor* Actor) const;

	void UpdateCameraPitchToLookAtTarget() const;

protected:
	virtual void BeginPlay() override;

	virtual void TickComponent(float const DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
};
