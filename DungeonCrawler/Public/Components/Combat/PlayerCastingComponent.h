// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "PlayerCastingComponent.generated.h"

class UGameplayAbility;
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSkillAtIndexChanged, int32, Index);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAbilityCastFailed);

struct FGameplayAbilitySpecHandle;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class DUNGEONCRAWLER_API UPlayerCastingComponent final : public UActorComponent
{
	GENERATED_BODY()
	
public:

	UPlayerCastingComponent();
	
	virtual void BeginPlay() override;

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FOnSkillAtIndexChanged OnSkillAtIndexChanged;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	int32 NumberOfSkills = 4;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	TArray<TSubclassOf<UGameplayAbility>> AbilitiesClasses;
	
	UPROPERTY(BlueprintReadOnly, Category = Handles)
	TArray<FGameplayAbilitySpecHandle> AbilitiesHandles;

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FOnAbilityCastFailed OnAbilityCastFailed;
	
	UFUNCTION(BlueprintPure, Category = Getters)
	UGameplayAbility* GetAbilityAtIndex(int32 Index = 0);

	UFUNCTION(BlueprintCallable)
	void AddAbility(TSubclassOf<UGameplayAbility> AbilityClass);

	UFUNCTION(BlueprintCallable)
	void RemoveAbility(TSubclassOf<UGameplayAbility> AbilityClass);
	
	UFUNCTION(BlueprintCallable, Category = Getters)
	void SetAbilityAtIndex(TSubclassOf<UGameplayAbility> AbilityClass, int32 Index = 0);

	void SetupAbilities();
	
private:

	UPROPERTY(Transient)
	class UMyAbilitySystemComponent* OwnerAbilitySystemComponent;

	void AddHandleForAbility(TSubclassOf<UGameplayAbility> AbilityClass, int32 Index = 0);
	
};
