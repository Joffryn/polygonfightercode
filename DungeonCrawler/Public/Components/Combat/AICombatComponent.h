// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
		
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AICombatComponent.generated.h"

UENUM(BlueprintType)
enum class EAIDesiredRange : uint8
{
	Melee,
	CloseDistanceAttack,
	Ranged
};

UENUM(BlueprintType)
enum class EAIActionType : uint8
{
	FuckAround,
	MeleeAttack,
	CloseDistanceAttack,
	RangedAttack,
	Block,
	Strafe,
	Taunt,
	Flex,
	Drink

};

USTRUCT(BlueprintType)
struct FMeaningfulActionSetup
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EAIActionType Action;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Priority;
	
};

USTRUCT(BlueprintType)
struct FAiActionSetup
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MinRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxRange;

	//Cooldown for this action
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Cooldown;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CooldownRandomInterval = 0.0f;

	//Cooldown for all actions after this executed
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float GlobalActionCooldown;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float GlobalActionCooldownRandomInterval = 0.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ActivationChance = 1.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanExecuteAction;
	
	FAiActionSetup(const float InMinRange, const float InMaxRange, const float InCooldown, const bool CanExecuteAction = true, const float InActivationChance = 1.0f)
	{
		MinRange = InMinRange;
		MaxRange = InMaxRange;
		Cooldown = InCooldown;
		bCanExecuteAction = CanExecuteAction;
		ActivationChance = InActivationChance;
	}

	FAiActionSetup(): MinRange(0), MaxRange(0), Cooldown(0), bCanExecuteAction(false)
	{
	}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UAICombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UAICombatComponent();
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	EAIDesiredRange DesiredRange;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	TArray<FMeaningfulActionSetup> MeaningfulActionsSetup;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	TMap<EAIActionType, FAiActionSetup> AiActionsSetup;

	UPROPERTY(Transient, BlueprintReadOnly)
	TMap<EAIActionType, float> AIActionToCooldownsSetup;

	UPROPERTY(Transient, BlueprintReadWrite)
	int32 LastMeleeAttackIndex;

	UPROPERTY(Transient, BlueprintReadWrite)
	float GlobalActionsCooldown;
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void OnActionExecuted(EAIActionType Action);

	UFUNCTION(BlueprintCallable)
	bool PerformActivationChanceCheck(EAIActionType Action);

	UFUNCTION(BlueprintCallable, Category = Setters)
	void ApplyActionCooldown(EAIActionType Action);
	
	UFUNCTION(BlueprintCallable, Category = Setters)
	void SetActionCooldown(EAIActionType Action, float Cooldown);

	UFUNCTION(BlueprintPure, Category = Getters)
	bool CanExecuteAnyMeaningfulAction(float DistanceToTarget, int32 Priority);
	
	UFUNCTION(BlueprintPure, Category = Getters)
	float GetDesiredRangeValue();
	
	UFUNCTION(BlueprintPure, Category = Getters)
	bool IsActionOnCooldown(EAIActionType Action);

	UFUNCTION(BlueprintPure, Category = Getters)
	bool IsOnGlobalActionCooldown() const;
	
	UFUNCTION(BlueprintPure, Category = Getters)
	float GetActionCooldown(EAIActionType Action);
	
};
