// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "Enums.h"
#include "ReactionSolverComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UReactionSolverComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UReactionSolverComponent* GetReactionSolver(const UObject* WorldContextObject);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TMap<EAttackStaggerStrength, FGameplayTag> AttackStrengthToTagsMap;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TMap<EStaggerReaction, FGameplayTag> StaggerReactionToTagsMap;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FPoiseToStaggerSetup ReactionsSetup;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag NoReaction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag HitReaction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag StrongHitReaction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag StunReaction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag KnockDownReaction;

	UFUNCTION(BlueprintPure, Category = "Ability System")
	FGameplayTag GetGameplayTagForAttackStaggerStrength(EAttackStaggerStrength AttackStaggerStrength);

	UFUNCTION(BlueprintCallable, Category = "Ability System")
    void SendStaggerGameplayEvent(AActor* Target, EAttackStaggerStrength AttackStaggerStrength);

	UFUNCTION(BlueprintPure, Category = "Ability System")
	EAttackStaggerStrength GetAttackStaggerStrengthForGameplayTag(FGameplayTag Tag) const;
	
	UFUNCTION(BlueprintPure)
	FGameplayTag GetReactionTag(EAttackStaggerStrength AttackStrength, EPoiseLevel PoiseLevel);
		
private:

	EStaggerReaction DecideStaggerReaction(EAttackStaggerStrength AttackStrength, EPoiseLevel PoiseLevel);

};
