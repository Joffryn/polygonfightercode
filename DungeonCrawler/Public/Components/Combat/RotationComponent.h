// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "RotationComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStrafingEnded);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API URotationComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	URotationComponent();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	FGameplayTagContainer BlockingGameplayTags;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	float AIRotationSpeed = 180.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	float RotationSpeedMultiplier = 1.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	bool bShouldRotate = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	FName IsInRangeBlackboardKeyName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	bool bWithDebug;
	
	UPROPERTY(BlueprintReadOnly, Category = Rotation)
	 AActor* Target;

	UPROPERTY(BlueprintReadWrite, Category = Rotation)
	FRotator RequestedRotation;

	UPROPERTY(BlueprintReadWrite, Category = Rotation)
	FRotator CharacterRotation;

	UPROPERTY(BlueprintReadOnly)
	bool bStrafeRight;
	
	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable)
	FOnStrafingEnded OnStrafingEnded;
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = Rotation)
	void SetCharacterRotation(FRotator NewRequestedRotation, bool bInterp = false, float InterpSpeed = 0.0f, float AcceptableYawDiff = 5.0f);

	UFUNCTION(BlueprintCallable)
	void SetTarget(AActor* NewTarget);

	UFUNCTION(BlueprintPure, Category = Rotation)
	bool WantsToStrafe() const { return bWantsToStrafe;};
	
	UFUNCTION(BlueprintPure, Category = Rotation)
	bool CanTurn() const;

	UFUNCTION(BlueprintCallable, Category = Strafing)
	void StartStrafing();

	UFUNCTION(BlueprintCallable, Category = Strafing)
	void StopStrafing();

	UFUNCTION(BlueprintCallable, Category = Strafing)
	void Reset();

private:

	bool bWantsToStrafe;

	void RotateTowardTarget(float DeltaTime);
	void Strafe() const;

	FRotator GetLookAtRotation() const;
	
};
