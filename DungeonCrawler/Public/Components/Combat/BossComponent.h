// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Components/ActorComponent.h"
#include "BossComponent.generated.h"

struct FGameplayAttribute;
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBossPhaseEnter, int32, Phase);

USTRUCT(BlueprintType)
struct FBossPhaseInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float HealthPercentage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 PhaseID;

};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UBossComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UBossComponent();

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable)
	FOnBossPhaseEnter OnBossPhaseEnter;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TArray<FBossPhaseInfo> PhasesSetup;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	FGameplayTag NextBossPhaseTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Music")
	USoundBase* Music;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Music")
	float MusicFadeInTime = 1.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Music")
	float MusicFadeOutTime = 2.5f;

	UPROPERTY(BlueprintReadWrite, Category = Phase)
	int32 Phase;
	
	UFUNCTION(BlueprintCallable, Category = Phase)
	void SetPhase(int32 NewPhase);

protected:
	
	virtual void BeginPlay() override;

private:
	
	bool bIsSetuped;
	
	UFUNCTION()
	void OnTargetSet(AActor* Target);

	UFUNCTION()
	void OnOwnerAttributeChange(const FGameplayAttribute Attribute, float NewValue, float OldValue);

	UFUNCTION()
	void OnOwnerDeath();

	UFUNCTION()
	void OnPlayerDeath();

	void StopMusic() const;
	
};
