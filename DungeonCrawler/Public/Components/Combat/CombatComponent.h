// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "GameplayEffectTypes.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "Enums.h"
#include "CombatComponent.generated.h"

class AWeapon;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWeaponDelegate, AWeapon*, Weapon);

UCLASS()
class DUNGEONCRAWLER_API UCombatComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:

	UCombatComponent();

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FWeaponDelegate OnWeaponEquipped;

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FWeaponDelegate OnWeaponUnequipped;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	TSubclassOf<AWeapon> WeaponClass;
	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	TSubclassOf<AWeapon> SecondaryWeaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	TSubclassOf<AWeapon> SecondaryLeftWeaponClass;
	
	UPROPERTY(EditAnywhere, Category = Config)
	FName RightHandAttachmentSocket;

	UPROPERTY(EditAnywhere, Category = Config)
	FName LeftHandAttachmentSocket;

	UPROPERTY(EditAnywhere, Category = Config)
	FName RightLegAttachmentSocket;

	UPROPERTY(EditAnywhere, Category = Config)
	FName LeftLegAttachmentSocket;

	UPROPERTY(BlueprintReadWrite)
	bool bIsWeaponEquipped = true;

	UPROPERTY(BlueprintReadWrite)
    FGameplayTagContainer CurrentAttackExtraTags;
	
	UPROPERTY(BlueprintReadOnly)
	FAttackParams CurrentAttackParams;

	UPROPERTY(BlueprintReadOnly)
	FActiveGameplayEffectHandle EquippedRightWeaponEffectHandle;

	UPROPERTY(BlueprintReadOnly)
	FActiveGameplayEffectHandle EquippedLeftWeaponEffectHandle;
	
	void Init();

	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintCallable)
	void EquipWeaponByClass(const TSubclassOf<AWeapon> WeaponClassToEquip);

	UFUNCTION(BlueprintCallable)
	void SwitchWeapon();

	UFUNCTION(BlueprintCallable)
	void InstantlySwitchWeapon();

	UFUNCTION(BlueprintPure)
	bool HasWeapon() const;

	UFUNCTION(BlueprintPure)
	AWeapon* GetWeaponInSocket(const EWeaponSocket Socket) const;

	UFUNCTION(BlueprintPure)
	AWeapon* GetSecondaryWeaponInSocket(const EWeaponSocket Socket) const;
		
	UFUNCTION(BlueprintPure)
	FORCEINLINE AWeapon* GetRightHandWeapon() const { return RightHandWeapon; }

	UFUNCTION(BlueprintPure)
	FORCEINLINE AWeapon* GetLeftHandWeapon() const { return LeftHandWeapon; }

	UFUNCTION(BlueprintPure)
	FORCEINLINE AWeapon* GetRightLegWeapon() const { return RightLeg; }

	UFUNCTION(BlueprintPure)
	FORCEINLINE AWeapon* GetLeftLegWeapon() const { return LeftLeg; }

	UFUNCTION(BlueprintCallable)
	void CalculateWeaponAttack(const EWeaponSocket WeaponSocket, float DamageMultiplier, int32 ExtraAttackStaggerStrength) const ;

	UFUNCTION(BlueprintPure)
	TArray<AWeapon*> GetWeapons() const;

	UFUNCTION(BlueprintPure)
	TArray<AWeapon*> GetSecondaryWeapons() const;

	UFUNCTION(BlueprintCallable)
	void SheathWeapon(AWeapon* Weapon) const ;

	UFUNCTION(BlueprintCallable)
	void DrawWeapon(AWeapon* Weapon) const ;
	
protected:

	UPROPERTY(Transient)
	AWeapon* RightHandWeapon;

	UPROPERTY(Transient)
	AWeapon* LeftHandWeapon;
	
	UPROPERTY(Transient)
	AWeapon* SecondaryRightHandWeapon;

	UPROPERTY(Transient)
	AWeapon* SecondaryLeftHandWeapon;
	
	UPROPERTY(Transient)
	AWeapon* RightLeg;

	UPROPERTY(Transient)
	AWeapon* LeftLeg;

private:

	AWeapon* SpawnAndAttachWeapon(const TSubclassOf<AWeapon> WeaponClassToSpawn, const EWeaponSocket WeaponSocket) const;

	void SpawnAndAttachLegWeapons();
	void SpawnAndAttachSecondaryWeapon();
	void PostEquipActions() ;

};
