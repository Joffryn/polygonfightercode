// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Components/ActorComponent.h"
#include "CritSolver.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UCritSolver : public UActorComponent
{
	GENERATED_BODY()

public:	
	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UCritSolver* GetCritSolver(const UObject* WorldContextObject);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag AlwaysCrittedTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag SprintAttackTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag SprintAttackCritUpgradeTag;

	UFUNCTION(BlueprintPure)
	bool IsAttackCrit(AActor* Attacker, AActor* Attacked) const;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	float CritDamageMultiplier = 1.5f;
	
};
