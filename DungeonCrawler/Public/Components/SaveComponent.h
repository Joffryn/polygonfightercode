// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AttributeSet.h"
#include "GameplayEffect.h"

#include "SaveComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API USaveComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	USaveComponent();

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static USaveComponent* GetSaveComponent(const UObject* WorldContextObject);

	
	UPROPERTY(EditDefaultsOnly, Category = "Save Game" )
	FGameplayEffectQuery SaveGameplayEffectsQuery;
	
	UPROPERTY(EditAnywhere)
	TArray<FGameplayAttribute> AttributesToSave;
	
	UFUNCTION(BlueprintCallable, Category = Save)
	void SaveGame(FName NextLevelName);

	UFUNCTION(BlueprintCallable, Category = Load)
	void LoadGame();

	UFUNCTION(BlueprintPure, Category = Load)
	bool DoesSaveExist();
	
	UFUNCTION(BlueprintPure, Category = Load)
	FName GetSavedLevelName();
	
	private:

	UPROPERTY()
	class UMySaveGame* Save;

	UPROPERTY()
	class ABaseCharacter* PlayerCharacter;

	void SaveLook() const;
	void SaveWeapons() const;
	void SaveAttributes();
	void SaveCasting() const;
	void SaveUpgradeEffects() const;
	void SaveSounds() const;
	
	void LoadLook() const;
	void LoadWeapons() const;
	void LoadAttributes();
	void LoadCasting() const;
	void LoadUpgradeEffects() const;
	void LoadSounds() const;
	
};
