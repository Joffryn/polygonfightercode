// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enums.h"
#include "GameplayTagContainer.h"
#include "Components/ActorComponent.h"
#include "UpgradeHandlingComponent.generated.h"


class UGameplayEffect;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UUpgradeHandlingComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	//Consider using an attribute
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	float ManaPerMeleeDamageGainMultiplier = 0.1f;
	
	//Consider using an attribute
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	float DamageTakenManaGainMultiplier = 0.2f;

	//Consider using an attribute
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	float CritDamageStaminaGainMultiplier = 0.1f;

	//Consider using an attribute
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	float HealthGainPerKill = 5.0f;

	//Consider using an attribute
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	float ManaRestorePerKill = 5.0f;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag HealOnKillTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag ManaRestoreOnKillTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag DamageTakeManaGainTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag CritStaminaGainTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag DamageMeleeTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag ManaGenerationBlockedTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag ParryStaminaRestorationTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag ParryDamageDealTag;
	
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> RestoreHealthEffect;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> RestoreStaminaEffect;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> RestoreManaEffect;

	
protected:
	virtual void BeginPlay() override;

	UPROPERTY(Transient)
	class ABaseCharacter* BaseCharacterOwner;

	UPROPERTY(Transient)
	class UAbilitySystemComponent* OwnerAbilitySystemComponent;

	void BindDelegates();

	UFUNCTION()
	void OnStatChanged( EStat Stat, float Amount);

	UFUNCTION()
	void OnAttackCrit(TSubclassOf<UDamageType> DamageType, float Amount);

	UFUNCTION()
	void OnAttackParried(AActor* Actor, TSubclassOf<UDamageType> DamageType, float Amount);
	
	UFUNCTION()
	void OnAttackDamageDealed(TSubclassOf<UDamageType> DamageType, float Amount);
	
	UFUNCTION()
	void OnKill(AActor* Target);
	
};
