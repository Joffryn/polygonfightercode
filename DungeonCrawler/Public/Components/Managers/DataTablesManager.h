// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DataTablesManager.generated.h"

class UDataTable;
//Manager for keeping references to Data Tables
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UDataTablesManager : public UActorComponent
{
	GENERATED_BODY()

public:	

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UDataTablesManager* GetDataTablesManager(const UObject* WorldContextObject);
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	UDataTable* Weapons;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	UDataTable* AICharactersStats;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	UDataTable* PlayerStats;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	UDataTable* Perks;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	UDataTable* Skills;
	
};
