// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/PlayerCameraManager.h"
#include "MyPlayerCameraManager.generated.h"

UCLASS()
class DUNGEONCRAWLER_API AMyPlayerCameraManager : public APlayerCameraManager
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static AMyPlayerCameraManager* GetMyPlayerCameraManager(const UObject* WorldContextObject);
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintPure)
	USkeletalMeshComponent* GetCameraComponent();
	
};
