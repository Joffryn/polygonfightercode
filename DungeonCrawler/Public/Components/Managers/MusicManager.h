// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MusicManager.generated.h"

//Music priority: Boss > Combat > Location
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UMusicManager : public UActorComponent
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UMusicManager* GetMusicManager(const UObject* WorldContextObject);

	
	UFUNCTION(BlueprintCallable)
	void PlayLocationMusic(USoundBase* Music, float FadeInTime);

	UFUNCTION(BlueprintCallable)
	void StopLocationMusic(float FadeOutTime);

	
	UFUNCTION(BlueprintCallable)
	void PlayBossMusic(USoundBase* Music, float FadeInTime);

	UFUNCTION(BlueprintCallable)
	void StopBossMusic(float FadeOutTime);
	

	UFUNCTION(BlueprintCallable)
	void PlayCombatMusic(float FadeInTime);

	UFUNCTION(BlueprintCallable)
	void StopCombatMusic(float FadeOutTime);
	
	UPROPERTY(EditAnywhere, Category = Config)
	TArray<USoundBase*> DefaultCombatMusic;
	
	UPROPERTY(BlueprintReadWrite)
	UAudioComponent* CurrentCombatMusic;
		
	UPROPERTY(BlueprintReadWrite)
	UAudioComponent* CurrentBossMusic;
	
	UPROPERTY(BlueprintReadWrite)
	UAudioComponent* CurrentLocationMusic;
	
	UPROPERTY(BlueprintReadWrite)
	UAudioComponent* CurrentlyPlayedMusic;
	
};
