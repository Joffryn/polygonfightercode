// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DamageResponseManager.generated.h"

USTRUCT(BlueprintType)
struct FDamageInfo 
{
	GENERATED_BODY()

	UPROPERTY (BlueprintReadOnly)
	AActor* DamagedActor;

	UPROPERTY(BlueprintReadOnly)
	float BaseDamage;

	UPROPERTY(BlueprintReadOnly)
	FHitResult HitInfo;

	UPROPERTY(BlueprintReadOnly)
	AController* EventInstigator;

	UPROPERTY(BlueprintReadOnly)
	AActor* DamageCauser;

	UPROPERTY(BlueprintReadOnly)
	TSubclassOf<UDamageType> DamageTypeClass;

	UPROPERTY(BlueprintReadOnly)
	bool bIsUnblockable;

};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UDamageResponseManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	UDamageResponseManager();

	UFUNCTION(BlueprintCallable)
	float SendDamageInfo(FDamageInfo DamageInfo);

protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
};
