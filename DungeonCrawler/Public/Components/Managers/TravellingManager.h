// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TravellingManager.generated.h"


USTRUCT(BlueprintType)
struct FCameraFadeSetup
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	float FromAlpha = 0.0f;

	UPROPERTY(EditDefaultsOnly)
	float ToAlpha = 1.0f;

	UPROPERTY(EditDefaultsOnly)
	float Duration = 1.0f;

	UPROPERTY(EditDefaultsOnly)
	bool bShouldFadeAudio = true;

	UPROPERTY(EditDefaultsOnly)
	bool bHoldWhenFinished = true;
	
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UTravellingManager : public UActorComponent
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UTravellingManager* GetTravellingManager(const UObject* WorldContextObject);
	
	UPROPERTY(EditDefaultsOnly)
	FCameraFadeSetup StartTravelFadeSetup;

	UPROPERTY(EditDefaultsOnly)
	FCameraFadeSetup EndTravelFadeSetup;
	
	UFUNCTION(BlueprintCallable)
	void TravelToLevel(FName LevelName);
	
protected:
	
	virtual void BeginPlay() override;
	
	private:
	
	UFUNCTION()
	void Travel();

	UFUNCTION()
	void BeginPlayFadeFinished();
	
	void HandleLevelLoaded();

	FName RequestedLevelName;
	FTimerHandle Handle;

};
