// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AIManager.generated.h"

class AAIController;
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAIManagerDelegate);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UAIManager : public UActorComponent
{
	GENERATED_BODY()

public:	

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UAIManager* GetAIManager(const UObject* WorldContextObject);

	UFUNCTION(BlueprintPure)
	bool AreThereAnyEnemies() const;
	
	UFUNCTION(BlueprintPure)
	bool IsThereAIAwareOfThePlayer() const;

	UFUNCTION(BlueprintCallable)
	void AIStartBeingAwareOfThePlayer(AAIController* AIController);
	
	UFUNCTION(BlueprintCallable)
	void AIStopBeingAwareOfThePlayer(AAIController* AIController);
	
	UFUNCTION(BlueprintCallable)
	void RegisterAI(AAIController* AIController);

	UFUNCTION(BlueprintCallable)
	void UnregisterAI(AAIController* AIController);

	UPROPERTY(BlueprintReadWrite)
	TArray<AAIController*> RegisteredAIs;

	UPROPERTY(BlueprintReadWrite)
	TArray<AAIController*> RegisteredBosses;
	
	UPROPERTY(Transient, BlueprintAssignable)
	FAIManagerDelegate OnLastAIDeath;

	UPROPERTY(Transient, BlueprintAssignable)
	FAIManagerDelegate OnLastBossDeath;

	UPROPERTY(Transient, BlueprintAssignable)
	FAIManagerDelegate OnFirstEnemyStartBeingAwareOfThePlayer;

	UPROPERTY(Transient, BlueprintAssignable)
	FAIManagerDelegate OnLastEnemyStopBeingAwareOfThePlayer;
	
	UPROPERTY(BlueprintReadWrite)
	TArray<AAIController*> AIsAwareOfThePlayer;
	
};
