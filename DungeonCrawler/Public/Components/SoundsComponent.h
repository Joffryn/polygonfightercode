// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "SoundsComponent.generated.h"

UCLASS()
class DUNGEONCRAWLER_API USoundsComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Character)
	TMap<FGameplayTag, USoundBase*> SoundsToTagsMap;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Footsteps)
	bool bPlayFootsteps = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Footsteps)
	bool bFootstepsRattle = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Footsteps)
	bool bIsMale = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Footsteps)
	float FootstepsVolume = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Footsteps)
	float FootstepsRattleVolume = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Efforts)
	float CharacterVolumeMultiplier = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Efforts)
	float CharacterPitchMultiplier = 1.0f;

	UFUNCTION(BlueprintCallable, Category = Sounds)
	void SpawnSound(const FGameplayTag SoundTag, const float VolumeMultiplier = 1.0f, const float PitchMultiplier = 1.0f) const;

	UFUNCTION(BlueprintCallable, Category = Sounds)
	UAudioComponent* SpawnSoundAttached(const FGameplayTag SoundTag, const FName AttachPointName, const float VolumeMultiplier = 1.0f,const float PitchMultiplier = 1.0) const;
	
};
