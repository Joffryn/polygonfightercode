// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Vignettes.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UVignettes : public UUserWidget
{
	GENERATED_BODY()
	
public:
	
	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UVignettes* GetVignettes(const UObject* WorldContextObject);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void Show();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void Hide();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void PushColorChange(FLinearColor Color);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void PopColorChange(FLinearColor Color);
	
};
