// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HudWidget.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UHudWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UHudWidget* GetHudWidget(const UObject* WorldContextObject);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void HideHud();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowHud();
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowMessage(const FText& Message);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowErrorMessage(const FText& Message);

};
