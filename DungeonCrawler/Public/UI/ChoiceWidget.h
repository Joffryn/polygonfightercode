// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Structs.h"
#include "Blueprint/UserWidget.h"
#include "ChoiceWidget.generated.h"
UCLASS()
class DUNGEONCRAWLER_API UChoiceWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = Config, meta = (ExposeOnSpawn=true))
	TArray<UDataTable*> PossibleUpgradesDataTables;
	
	UPROPERTY(Transient, BlueprintReadWrite, Category = Upgrades)
	TArray<FUpgradeChoice> PossibleUpgrades;

	UFUNCTION(BlueprintCallable, Category = Upgrades)
	void SetupPossibleUpgrades();
	
};
