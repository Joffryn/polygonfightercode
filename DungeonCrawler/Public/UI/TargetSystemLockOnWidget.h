// Copyright 2018-2019 Mickael Daniel. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TargetSystemLockOnWidget.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UTargetSystemLockOnWidget : public UUserWidget
{
	GENERATED_BODY()

};
