// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MyHUD.generated.h"

UCLASS()
class DUNGEONCRAWLER_API AMyHUD : public AHUD
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static AMyHUD* GetMyHud(const UObject* WorldContextObject);

	UPROPERTY(Transient, BlueprintReadWrite, Category = Widgets)
	class UHudWidget* HudWidget;

	UPROPERTY(Transient, BlueprintReadWrite, Category = Widgets)
	class UVignettes* Vignettes;
	
	UFUNCTION(BlueprintCallable)
	void ShowDamageInfo(const FVector Location, const FText& Text, const FLinearColor Color = FLinearColor::White, bool bIsCritical = false);

	UFUNCTION(BlueprintCallable)
	void ShowGoldCollectedInfo(const FVector Location, float Amount);
	
	UFUNCTION(BlueprintCallable)
	void ShowInfo(const FVector Location, const FText& Text, const FLinearColor Color = FLinearColor::White);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Widgets)
	void ShowBossHealthBar(class ABaseCharacter* BossCharacter);
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void HideHudWidget();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowHudWidget();
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowDeathMessage();
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    void ShowVictoryMessage();
    	
private:

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FLinearColor GoldColor = FLinearColor::Yellow;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	TSubclassOf<class AInfo3D> Info3DClass;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	TSubclassOf<AInfo3D> DamageInfoClass;

};
