// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "GradualStatChangeCalculation.generated.h"

/**
 * 
 */
UCLASS()
class DUNGEONCRAWLER_API UGradualStatChangeCalculation : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()
	
};
