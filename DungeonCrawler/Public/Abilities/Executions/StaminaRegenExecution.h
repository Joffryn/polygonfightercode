// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "StaminaRegenExecution.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UStaminaRegenExecution : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()

	UStaminaRegenExecution();

	UPROPERTY(EditDefaultsOnly)
	FGameplayTagContainer StaminaRegenBlockingTags;

	UPROPERTY(EditDefaultsOnly)
	FGameplayTag GuardTag;

	UPROPERTY(EditDefaultsOnly)
	FGameplayTag GuardRegenTag;
	
	UPROPERTY(EditDefaultsOnly)
	FGameplayTag SprintTag;

	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;

};
