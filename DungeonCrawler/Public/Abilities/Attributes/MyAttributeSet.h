// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "CoreMinimal.h"
#include "MyAttributeSet.generated.h"

// Uses macros from AttributeSet.h
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

/** This holds all of the attributes used by abilities, it instantiates a copy of this on every character */

UCLASS()
class DUNGEONCRAWLER_API UMyAttributeSet : public UAttributeSet
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintReadOnly, Category = "Gold")
	FGameplayAttributeData Gold;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, Gold)
	
	UPROPERTY(BlueprintReadOnly, Category = "Health")
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, Health)

	UPROPERTY(BlueprintReadOnly, Category = "Health")
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, MaxHealth)

	UPROPERTY(BlueprintReadOnly, Category = "Health")
	FGameplayAttributeData HealthGainMultiplier;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, HealthGainMultiplier)

	UPROPERTY(BlueprintReadOnly, Category = "Health")
	FGameplayAttributeData HealthLossMultiplier;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, HealthLossMultiplier)


	UPROPERTY(BlueprintReadOnly, Category = "Mana")
	FGameplayAttributeData Mana;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, Mana)

	UPROPERTY(BlueprintReadOnly, Category = "Mana")
	FGameplayAttributeData MaxMana;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, MaxMana)

	UPROPERTY(BlueprintReadOnly, Category = "Mana")
	FGameplayAttributeData ManaGainMultiplier;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, ManaGainMultiplier)

	UPROPERTY(BlueprintReadOnly, Category = "Mana")
	FGameplayAttributeData ManaLossMultiplier;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, ManaLossMultiplier)

	UPROPERTY(BlueprintReadOnly, Category = "Mana")
	FGameplayAttributeData CurrentActionManaCost;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, CurrentActionManaCost)


	UPROPERTY(BlueprintReadOnly, Category = "Stamina")
	FGameplayAttributeData Stamina;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, Stamina)

	UPROPERTY(BlueprintReadOnly, Category = "Stamina")
	FGameplayAttributeData MaxStamina;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, MaxStamina)

	UPROPERTY(BlueprintReadOnly, Category = "Stamina")
	FGameplayAttributeData StaminaGainMultiplier;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, StaminaGainMultiplier)

	UPROPERTY(BlueprintReadOnly, Category = "Stamina")
	FGameplayAttributeData StaminaLossMultiplier;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, StaminaLossMultiplier)

	UPROPERTY(BlueprintReadOnly, Category = "Stamina")
	FGameplayAttributeData StaminaRegen;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, StaminaRegen)

	UPROPERTY(BlueprintReadOnly, Category = "Stamina")
	FGameplayAttributeData StaminaSprintCost;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, StaminaSprintCost)

	UPROPERTY(BlueprintReadOnly, Category = "Stamina")
	FGameplayAttributeData CurrentActionStaminaCost;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, CurrentActionStaminaCost)
	

	UPROPERTY(BlueprintReadOnly, Category = "Movement")
	FGameplayAttributeData DodgeStrengthMultiplier;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, DodgeStrengthMultiplier)

	UPROPERTY(BlueprintReadOnly, Category = "Skills")
	FGameplayAttributeData DrinkCharges;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, DrinkCharges)
	
	UPROPERTY(BlueprintReadOnly, Category = "Stamina")
	FGameplayAttributeData BlockingStaminaCostMultiplier;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, BlockingStaminaCostMultiplier)


	UPROPERTY(BlueprintReadOnly, Category = "Speed")
	FGameplayAttributeData Speed;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, Speed)
	

	UPROPERTY(BlueprintReadOnly, Category = "Damage")
	FGameplayAttributeData DamageMultiplier;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, DamageMultiplier)

	UPROPERTY(BlueprintReadOnly, Category = "Damage")
	FGameplayAttributeData CritDamageMultiplier;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, CritDamageMultiplier)
	

	UPROPERTY(BlueprintReadOnly, Category = "Combo")
	FGameplayAttributeData LightAttackComboCounter;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, LightAttackComboCounter)

	UPROPERTY(BlueprintReadOnly, Category = "Combo")
	FGameplayAttributeData HeavyAttackComboCounter;
	ATTRIBUTE_ACCESSORS(UMyAttributeSet, HeavyAttackComboCounter)
	

};
