// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "AbilityTask_InterpRotation.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UAbilityTask_InterpRotation : public UAbilityTask
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "true", AutoCreateRefTerm = "LocationOffset"), Category = "Ability|Tasks")
	static UAbilityTask_InterpRotation* InterpolateControlRotationToActor(UGameplayAbility* OwningAbility, AActor* TargetActor, const FVector& LocationOffset, const float Speed = 1.0f, const bool OnlyYaw = true, const float Tolerance = 0.01f);
	
	UAbilityTask_InterpRotation();

	virtual void TickTask(const float DeltaTime) override;

protected:
	FVector GetTargetLocation() const;
	FVector GetDirectionToTarget(AActor* AvatarActor) const;

public:
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInterpControlRotationDelegate);

	UPROPERTY(BlueprintAssignable)
	FInterpControlRotationDelegate	OnFinish;

	UPROPERTY(BlueprintAssignable)
	FInterpControlRotationDelegate	OnError;

protected:
	UPROPERTY()
	AActor* TargetActor;

	UPROPERTY()
	USceneComponent* TargetComponent;

	UPROPERTY()
	FVector TargetLocation;

	UPROPERTY()
	bool RotateOnlyYaw;

	UPROPERTY()
	float InterpolationSpeed;

	UPROPERTY()
	float Tolerance;

};