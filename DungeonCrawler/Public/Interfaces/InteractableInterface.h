// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/Interface.h"
#include "CoreMinimal.h"
#include "InteractableInterface.generated.h"

UINTERFACE(MinimalAPI, BlueprintType, Blueprintable)
class UInteractableInterface : public UInterface
{
	GENERATED_BODY()
};

class DUNGEONCRAWLER_API IInteractableInterface
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	bool CanInteract();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	FText GetInteractionText();
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void Interact();

};
