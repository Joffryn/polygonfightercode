// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TrailInterface.generated.h"

UINTERFACE(MinimalAPI)
class UTrailInterface : public UInterface
{
	GENERATED_BODY()
};

class DUNGEONCRAWLER_API ITrailInterface
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	UStaticMeshComponent* GetStaticMesh();
	
};
