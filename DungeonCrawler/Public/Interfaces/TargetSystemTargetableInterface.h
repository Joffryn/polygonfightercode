// Copyright 2018-2019 Mickael Daniel. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TargetSystemTargetableInterface.generated.h"

UINTERFACE(Blueprintable)
class UTargetSystemTargetableInterface : public UInterface
{
	GENERATED_BODY()
};

class DUNGEONCRAWLER_API ITargetSystemTargetableInterface
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target System")
	bool IsTargetable() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target System")
	void OnBeingLockedOn() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target System")
	void OnBeingLockedOff() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target System")
	USceneComponent* GetAttachComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Target System")
	FVector GetLookAtPositionAdjustment() const;
	
};
