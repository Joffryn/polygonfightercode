// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CastableAbilityInterface.generated.h"

UINTERFACE(MinimalAPI)
class UCastableAbilityInterface : public UInterface
{
	GENERATED_BODY()
};

class DUNGEONCRAWLER_API ICastableAbilityInterface
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	float GetManaCost();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	UTexture2D* GetIcon();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	FLinearColor GetIconColor();
	
};
