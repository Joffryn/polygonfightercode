// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class DungeonCrawlerEditorTarget : TargetRules
{
	public DungeonCrawlerEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.Add("DungeonCrawler");
	}
}
